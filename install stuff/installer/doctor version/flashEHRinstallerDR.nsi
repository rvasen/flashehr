;======================================================
; Include
 
  !include "MUI2.nsh"
 
;======================================================
; Installer Information
 
  Name "FlashEHR for Doctors version 0.1"
  OutFile "FlashEHR_DR_0.1.exe"
  InstallDir \FlashEHR
  RequestExecutionLevel user
 
;======================================================
; Modern Interface Configuration
 
  !define MUI_HEADERIMAGE
  !define MUI_ABORTWARNING
  !define MUI_COMPONENTSPAGE_SMALLDESC
  !define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
  !define MUI_FINISHPAGE
  !define MUI_FINISHPAGE_TEXT "Thank you for installing FlashEHR. You are a nice person and I love you."
  !define MUI_DIRECTORYPAGE
  !define MUI_DIRECTORYPAGE_TEXT_TOP "Please select the USB flash drive to install FlashEHR onto."
  ;!define MUI_WELCOMEFINISHPAGE_BITMAP "..\somepic.bmp"
  ;!define MUI_ICON "..\someicon.ico"
 
;======================================================
; Pages
 
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  ;!insertmacro MUI_UNPAGE_CONFIRM
  ;!insertmacro MUI_UNPAGE_COMPONENTS
  ;!insertmacro MUI_UNPAGE_INSTFILES
 
;======================================================
; Languages
 
  !insertmacro MUI_LANGUAGE "English"
 
;======================================================
; Variables
;Var SHORTCUT

;======================================================
; Installer sections

Section "-app"
  SetOutPath C:
  File /r "FlashEHR"
  CreateDirectory C:\FlashEHR\patients
  ;WriteUninstaller $PROGRAMFILES32\FlashEHR
  ; Add the app here
  ;WriteRegStr HKLM "Software\FlashEHR" "" ""
SectionEnd

Section "Shortcut"
  ; Add the app in the shortcut link and the icon
  CreateShortCut "$DESKTOP\FlashEHR.lnk" "C:\FlashEHR\doctorVersion.exe" "" "C:\FlashEHR\health.ico"
  ;WriteRegStr HKLM "Software\FlashEHR" "Shortcut" "yeah boi"
SectionEnd

;Section "Uninstall"
  ;RMDir \r $PROGRAMFILES32\FlashEHR
  ;RMDir \r $INSTDIR\FlashEHR
  ;ReadRegStr $SHORTCUT HKLM Software\FlashEHR "Shortcut"
  ;StrCmp $SHORTCUT "yeah boi" +2
  ;Goto +2
  ;Delete "$DESKTOP\FlashEHR.lnk"
  ;DeleteRegKey HKCU Software\FlashEHR
;SectionEnd
