import sys
from cx_Freeze import setup, Executable

exe = Executable(script = "patientVersion.py",
				 base = "Win32GUI",
				 icon = "C:\FlashEHR_patient\health.ico",
				)
build_exe_options = {
	'includes' : ['atexit', 'PySide.QtNetwork']
}

setup(name = "FlashEHR",
	  version = "0.1.0",
	  description = 'A flash drive based electronic health records system.',
	  executables = [exe],
	  options = {'build_exe': build_exe_options})