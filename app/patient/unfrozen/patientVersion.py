import sys
import pymysql, os
from Crypto.Hash import SHA512
from Crypto.Cipher import AES
from hashlib import md5
from tempfile import mkstemp
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtHelp import *
from PySide.QtNetwork import *
from PySide import QtXml
from xml.etree import ElementTree as ET
from encryptDB.patient.logInPatient import login
from encryptDB.patient.closeOutOfClient import closeOut
from encryptDB.patient.newUserPatient import createUser
from shutil import copy

myXmlDictionary = {}
myDuplicateXmlDictionary = {}
tripleVal = [False, "", 0]
globalEnd = False

def writeOut(myXmlDictionary):
    if (xmlFile.open(QIODevice.WriteOnly) == False):
        print("Error opening file")    
    else:
        xmlWriter.writeStartDocument()
        xmlWriter.writeCharacters(makeTab(0))
        xmlWriter.writeStartElement("patient")

        #START MAIN INFO RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("mainInfo")

        #WHO
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("who")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("lastname")
        xmlWriter.writeCharacters(myXmlDictionary["lastname"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("firstname")
        xmlWriter.writeCharacters(myXmlDictionary["firstname"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("ssn")
        xmlWriter.writeCharacters(myXmlDictionary["ssn"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("marital")
        xmlWriter.writeCharacters(myXmlDictionary["marital"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("DOB")
        xmlWriter.writeCharacters(myXmlDictionary["DOB"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("sex")
        xmlWriter.writeCharacters(myXmlDictionary["sex"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("pic")
        xmlWriter.writeCharacters(myXmlDictionary["pic"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #CONTACT
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("contact")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("address")
        xmlWriter.writeCharacters(myXmlDictionary["address"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("state")
        xmlWriter.writeCharacters(myXmlDictionary["state"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("country")
        xmlWriter.writeCharacters(myXmlDictionary["country"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("email")
        xmlWriter.writeCharacters(myXmlDictionary["email"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("city")
        xmlWriter.writeCharacters(myXmlDictionary["city"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("postalCode")
        xmlWriter.writeCharacters(myXmlDictionary["postalCode"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("emergencyPhone")
        xmlWriter.writeCharacters(myXmlDictionary["emergencyPhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("emergencyContact")
        xmlWriter.writeCharacters(myXmlDictionary["emergencyContact"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("workPhone")
        xmlWriter.writeCharacters(myXmlDictionary["workPhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("homePhone")
        xmlWriter.writeCharacters(myXmlDictionary["homePhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("mobilePhone")
        xmlWriter.writeCharacters(myXmlDictionary["mobilePhone"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #EMPLOYER
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("employer")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("occupation")
        xmlWriter.writeCharacters(myXmlDictionary["occupation"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerName")
        xmlWriter.writeCharacters(myXmlDictionary["employerName"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerAddress")
        xmlWriter.writeCharacters(myXmlDictionary["employerAddress"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerState")
        xmlWriter.writeCharacters(myXmlDictionary["employerState"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerCountry")
        xmlWriter.writeCharacters(myXmlDictionary["employerCountry"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerCity")
        xmlWriter.writeCharacters(myXmlDictionary["employerCity"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerPost")
        xmlWriter.writeCharacters(myXmlDictionary["employerPost"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #STATS
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("stats")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("language")
        xmlWriter.writeCharacters(myXmlDictionary["language"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("race")
        xmlWriter.writeCharacters(myXmlDictionary["race"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("familySize")
        xmlWriter.writeCharacters(myXmlDictionary["familySize"])
        xmlWriter.writeEndElement()
         
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("homeless")
        xmlWriter.writeCharacters(myXmlDictionary["homeless"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("income")
        xmlWriter.writeCharacters(myXmlDictionary["income"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("interpreter")
        xmlWriter.writeCharacters(myXmlDictionary["interpreter"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #INSURANCE
        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("insurance")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("provider")
        xmlWriter.writeCharacters(myXmlDictionary["provider"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("plan")
        xmlWriter.writeCharacters(myXmlDictionary["plan"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("date")
        xmlWriter.writeCharacters(myXmlDictionary["date"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("policyNumber")
        xmlWriter.writeCharacters(myXmlDictionary["policyNumber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("groupNumber")
        xmlWriter.writeCharacters(myXmlDictionary["groupNumber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subEmployer")
        xmlWriter.writeCharacters(myXmlDictionary["subEmployer"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subscriber")
        xmlWriter.writeCharacters(myXmlDictionary["subscriber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("relationship")
        xmlWriter.writeCharacters(myXmlDictionary["relationship"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subDOB")
        xmlWriter.writeCharacters(myXmlDictionary["subDOB"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subSsn")
        xmlWriter.writeCharacters(myXmlDictionary["subSsn"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subSex")
        xmlWriter.writeCharacters(myXmlDictionary["subSex"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subAddress")
        xmlWriter.writeCharacters(myXmlDictionary["subAddress"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subCity")
        xmlWriter.writeCharacters(myXmlDictionary["subCity"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subState")
        xmlWriter.writeCharacters(myXmlDictionary["subState"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subZip")
        xmlWriter.writeCharacters(myXmlDictionary["subZip"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subCountry")
        xmlWriter.writeCharacters(myXmlDictionary["subCountry"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subPhone")
        xmlWriter.writeCharacters(myXmlDictionary["subPhone"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START MEDICAL RECORD RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("medRecords")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("diagnosis")
        tempVar = 1
        while "diagID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("diagID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["diagID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "doctor" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("doctor" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["doctor" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "details" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("details" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["details" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "diagDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("diagDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["diagDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("medication")
        tempVar = 1
        while "mediID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mediID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mediID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "dosage" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("dosage" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["dosage" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "prescriber" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("prescriber" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["prescriber" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "mediDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mediDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mediDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("immunization")
        tempVar = 1
        while "immID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("immID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["immID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "immDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("immDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["immDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "location" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("location" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["location" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("allergy")
        tempVar = 1
        while "allID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("allID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["allID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "severity" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("severity" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["severity" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "mitigation" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mitigation" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mitigation" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START TEST RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("testResults")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("radiology")
        tempVar = 1
        while "radImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("labtest")
        tempVar = 1
        while "labImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START CURRENT STATUS RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("currentStatus")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("upcomingAppt")
        tempVar = 1
        while "futLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futPurpose" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futPurpose" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futPurpose" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("pastAppt")
        tempVar = 1
        while "locDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("locDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["locDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "date" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("date" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["date" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "height" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("height" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["height" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "weight" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("weight" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["weight" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "bloodPressure" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("bloodPressure" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["bloodPressure" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "pulse" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("pulse" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["pulse" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "notes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("notes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["notes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("graphicalDisplay")
        tempVar = 1
        while "heightImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("heightImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["heightImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "weightImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("weightImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["weightImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "pressureImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("pressureImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["pressureImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()  

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #END DOCUMENT
        xmlWriter.writeCharacters(makeTab(0))
        xmlWriter.writeEndElement()
        xmlWriter.writeEndDocument()
        xmlFile.close()

def makeTab(indCount):
    space = ""
    while indCount > 0:
        indCount -= 1
        space = space + "    "
    newLine = "\n" + space
    return newLine

def parseXML():
    global content
    global mainInfo
    global medRecords
    global testResults
    global currentStatus

    #Set static values as global
    global firstNameVal
    global lastNameVal
    global ssnVal
    global marVal
    global dobVal
    global sexVal
    global picVal

    global addVal
    global statVal
    global countVal
    global emailVal
    global cityVal
    global postVal
    global emPhoneVal
    global emContVal
    global workPhoneVal
    global homePhoneVal
    global mobPhoneVal

    global occVal
    global empNameVal
    global empAddVal
    global empStatVal
    global empCountVal
    global empCityVal
    global empPostVal

    global langVal
    global raceVal
    global famSizeVal
    global homeVal
    global incVal
    global interVal

    global primVal
    global planVal
    global effDateVal
    global policyVal
    global groupVal
    global subsEmpVal
    global subsVal
    global relateVal
    global insDobVal
    global insSsnVal
    global insSexVal
    global insAddVal
    global insCityVal
    global insStateVal
    global insZipVal
    global insCountVal
    global insPhoneVal

    global diagCounterSet
    global mediCounterSet
    global immCounterSet 
    global allCounterSet 
    global radCounterSet
    global labCounterSet
    global futCounterSet
    global pastCounterSet
    global graphCounterSet

    content = ET.parse(tripleVal[1])
    mainInfo=content.find("mainInfo")
    medRecords=content.find("medRecords")
    testResults=content.find("testResults")
    currentStatus=content.find("currentStatus")

    #mainInfo
    lastNameVal = mainInfo.find('who/lastname').text
    firstNameVal = mainInfo.find('who/firstname').text
    ssnVal = mainInfo.find('who/ssn').text
    marVal = mainInfo.find('who/marital').text
    dobVal = mainInfo.find('who/DOB').text
    sexVal = mainInfo.find('who/sex').text
    picVal = mainInfo.find('who/pic').text

    #contact
    addVal = mainInfo.find('contact/address').text
    statVal = mainInfo.find('contact/state').text
    countVal = mainInfo.find('contact/country').text
    emailVal = mainInfo.find('contact/email').text
    cityVal = mainInfo.find('contact/city').text
    postVal = mainInfo.find('contact/postalCode').text
    emPhoneVal = mainInfo.find('contact/emergencyPhone').text
    emContVal = mainInfo.find('contact/emergencyContact').text
    workPhoneVal = mainInfo.find('contact/workPhone').text
    homePhoneVal = mainInfo.find('contact/homePhone').text
    mobPhoneVal = mainInfo.find('contact/mobilePhone').text

    #employer
    occVal = mainInfo.find('employer/occupation').text
    empNameVal = mainInfo.find('employer/employerName').text
    empAddVal = mainInfo.find('employer/employerAddress').text
    empStatVal = mainInfo.find('employer/employerState').text
    empCountVal = mainInfo.find('employer/employerCountry').text
    empCityVal = mainInfo.find('employer/employerCity').text
    empPostVal = mainInfo.find('employer/employerPost').text

    #stats
    langVal = mainInfo.find('stats/language').text
    raceVal = mainInfo.find('stats/race').text
    famSizeVal = mainInfo.find('stats/familySize').text
    homeVal = mainInfo.find('stats/homeless').text
    incVal = mainInfo.find('stats/income').text
    interVal = mainInfo.find('stats/interpreter').text

    #insurance
    primVal = mainInfo.find('insurance/provider').text
    planVal = mainInfo.find('insurance/plan').text
    effDateVal = mainInfo.find('insurance/date').text
    policyVal = mainInfo.find('insurance/policyNumber').text
    groupVal = mainInfo.find('insurance/groupNumber').text
    subsEmpVal = mainInfo.find('insurance/subEmployer').text
    subsVal = mainInfo.find('insurance/subscriber').text
    relateVal = mainInfo.find('insurance/relationship').text
    insDobVal = mainInfo.find('insurance/subDOB').text
    insSsnVal = mainInfo.find('insurance/subSsn').text
    insSexVal = mainInfo.find('insurance/subSex').text
    insAddVal = mainInfo.find('insurance/subAddress').text
    insCityVal = mainInfo.find('insurance/subCity').text
    insStateVal = mainInfo.find('insurance/subState').text
    insZipVal = mainInfo.find('insurance/subZip').text
    insCountVal = mainInfo.find('insurance/subCountry').text
    insPhoneVal = mainInfo.find('insurance/subPhone').text

    #medRecords
    diagCounterSet = 1
    mediCounterSet = 1
    immCounterSet = 1
    allCounterSet = 1
    radCounterSet = 1
    labCounterSet = 1
    futCounterSet = 1
    pastCounterSet = 1
    graphCounterSet = 1

    while str(type(medRecords.find("diagnosis/diagID" + str(diagCounterSet)))) != "<class 'NoneType'>": 
        global diagName
        global diagDocName
        global diagDetName
        global diagDateName

        diagName = "diagnosis/diagID" + str(diagCounterSet)
        diagDocName = "diagnosis/doctor" + str(diagCounterSet)
        diagDetName = "diagnosis/details" + str(diagCounterSet)
        diagDateName = "diagnosis/diagDate" + str(diagCounterSet)

        myXmlDictionary["diagID" + str(diagCounterSet)] = medRecords.find(diagName).text
        myXmlDictionary["doctor" + str(diagCounterSet)] = medRecords.find(diagDocName).text
        myXmlDictionary["details" + str(diagCounterSet)] = medRecords.find(diagDetName).text
        myXmlDictionary["diagDate" + str(diagCounterSet)] = medRecords.find(diagDateName).text

        diagCounterSet += 1

    while str(type(medRecords.find("medication/mediID" + str(mediCounterSet)))) != "<class 'NoneType'>": 
        global mediIDValName
        global mediDosValName
        global mediPresValName
        global mediDateValName

        mediIDValName = "medication/mediID" + str(mediCounterSet)
        mediDosValName = "medication/dosage" + str(mediCounterSet)
        mediPresValName = "medication/prescriber" + str(mediCounterSet)
        mediDateValName = "medication/mediDate" + str(mediCounterSet)        

        myXmlDictionary["mediID" + str(mediCounterSet)] = medRecords.find(mediIDValName).text
        myXmlDictionary["dosage" + str(mediCounterSet)] = medRecords.find(mediDosValName).text
        myXmlDictionary["prescriber" + str(mediCounterSet)] = medRecords.find(mediPresValName).text
        myXmlDictionary["mediDate" + str(mediCounterSet)] = medRecords.find(mediDateValName).text

        mediCounterSet += 1
    
    while str(type(medRecords.find("immunization/immID" + str(immCounterSet)))) != "<class 'NoneType'>": 
        global immIDValName
        global immDateValName
        global immLocValName

        immIDValName = "immunization/immID" + str(immCounterSet)
        immDateValName = "immunization/immDate" + str(immCounterSet)
        immLocValName = "immunization/location" + str(immCounterSet)    

        myXmlDictionary["immID" + str(immCounterSet)] = medRecords.find(immIDValName).text
        myXmlDictionary["immDate" + str(immCounterSet)] = medRecords.find(immDateValName).text
        myXmlDictionary["location" + str(immCounterSet)] = medRecords.find(immLocValName).text

        immCounterSet += 1

    while str(type(medRecords.find("allergy/allID" + str(allCounterSet)))) != "<class 'NoneType'>": 
        global allIDValName
        global allSevValName
        global allMitiValName

        allIDValName = "allergy/allID" + str(allCounterSet)
        allSevValName = "allergy/severity" + str(allCounterSet)
        allMitiValName = "allergy/mitigation" + str(allCounterSet)
        
        myXmlDictionary["allID" + str(allCounterSet)] = medRecords.find(allIDValName).text
        myXmlDictionary["severity" + str(allCounterSet)] = medRecords.find(allSevValName).text
        myXmlDictionary["mitigation" + str(allCounterSet)] = medRecords.find(allMitiValName).text

        allCounterSet += 1

    #testResults
    radiology = testResults.find('radiology')
    
    while str(type(testResults.find("radiology/radLocDr" + str(radCounterSet)))) != "<class 'NoneType'>": 
        global radImgValName
        global radLocValName
        global radDateValName
        global radNoteValName

        radImgValName = "radiology/radImg" + str(radCounterSet)
        radLocValName = "radiology/radLocDr" + str(radCounterSet)
        radDateValName = "radiology/radDate" + str(radCounterSet)
        radNoteValName = "radiology/radNotes" + str(radCounterSet)

        myXmlDictionary["radImg" + str(radCounterSet)] = testResults.find(radImgValName).text
        myXmlDictionary["radLocDr" + str(radCounterSet)] = testResults.find(radLocValName).text
        myXmlDictionary["radDate" + str(radCounterSet)] = testResults.find(radDateValName).text
        myXmlDictionary["radNotes" + str(radCounterSet)] = testResults.find(radNoteValName).text

        radCounterSet += 1
    labtest = testResults.find('labtest')

    while str(type(testResults.find("labtest/labLocDr" + str(labCounterSet)))) != "<class 'NoneType'>": 
        global labImgValName
        global labLocValName
        global labDateValName
        global labNoteValName

        labImgValName = "labtest/labImg" + str(labCounterSet)
        labLocValName = "labtest/labLocDr" + str(labCounterSet)
        labDateValName = "labtest/labDate" + str(labCounterSet)
        labNoteValName = "labtest/labNotes" + str(labCounterSet)

        myXmlDictionary["labImg" + str(labCounterSet)] = testResults.find(labImgValName).text
        myXmlDictionary["labLocDr" + str(labCounterSet)] = testResults.find(labLocValName).text
        myXmlDictionary["labDate" + str(labCounterSet)] = testResults.find(labDateValName).text
        myXmlDictionary["labNotes" + str(labCounterSet)] = testResults.find(labNoteValName).text

        labCounterSet += 1

    #currentStatus
    while str(type(currentStatus.find("upcomingAppt/futLocDr" + str(futCounterSet)))) != "<class 'NoneType'>": 
        global futLocValName
        global futDateValName
        global futPurValName
        global futNoteValName

        futLocValName = "upcomingAppt/futLocDr" + str(futCounterSet)
        futDateValName = "upcomingAppt/futDate" + str(futCounterSet)
        futPurValName = "upcomingAppt/futPurpose" + str(futCounterSet)
        futNoteValName = "upcomingAppt/futNotes" + str(futCounterSet)

        myXmlDictionary["futLocDr" + str(futCounterSet)] = currentStatus.find(futLocValName).text
        myXmlDictionary["futDate" + str(futCounterSet)] = currentStatus.find(futDateValName).text
        myXmlDictionary["futPurpose" + str(futCounterSet)] = currentStatus.find(futPurValName).text
        myXmlDictionary["futNotes" + str(futCounterSet)] = currentStatus.find(futNoteValName).text

        futCounterSet += 1

    while str(type(currentStatus.find("pastAppt/locDr" + str(pastCounterSet)))) != "<class 'NoneType'>": 
        global pastLocValName
        global pastDateValName
        global pastHeightValName
        global pastWeightValName
        global pastPressValName
        global pastPulseValName
        global pastNoteValName

        pastLocValName = "pastAppt/locDr" + str(pastCounterSet)
        pastDateValName = "pastAppt/date" + str(pastCounterSet)
        pastHeightValName = "pastAppt/height" + str(pastCounterSet)
        pastWeightValName = "pastAppt/weight" + str(pastCounterSet)
        pastPressValName = "pastAppt/bloodPressure" + str(pastCounterSet)
        pastPulseValName = "pastAppt/pulse" + str(pastCounterSet)
        pastNoteValName = "pastAppt/notes" + str(pastCounterSet)

        myXmlDictionary["locDr" + str(pastCounterSet)] = currentStatus.find(pastLocValName).text
        myXmlDictionary["date" + str(pastCounterSet)] = currentStatus.find(pastDateValName).text
        myXmlDictionary["height" + str(pastCounterSet)] = currentStatus.find(pastHeightValName).text
        myXmlDictionary["weight" + str(pastCounterSet)] = currentStatus.find(pastWeightValName).text
        myXmlDictionary["bloodPressure" + str(pastCounterSet)] = currentStatus.find(pastPressValName).text
        myXmlDictionary["pulse" + str(pastCounterSet)] = currentStatus.find(pastPulseValName).text
        myXmlDictionary["notes" + str(pastCounterSet)] = currentStatus.find(pastNoteValName).text

        pastCounterSet += 1

    while str(type(currentStatus.find("graphicalDisplay/heightImg" + str(graphCounterSet)))) != "<class 'NoneType'>": 
        global heightImgValName
        global weightImgValName
        global pressureImgValName

        heightImgValName = "graphicalDisplay/heightImg" + str(graphCounterSet)
        weightImgValName = "graphicalDisplay/weightImg" + str(graphCounterSet)
        pressureImgValName = "graphicalDisplay/pressureImg" + str(graphCounterSet)
        
        myXmlDictionary["heightImg" + str(graphCounterSet)] = currentStatus.find(heightImgValName).text
        myXmlDictionary["weightImg" + str(graphCounterSet)] = currentStatus.find(weightImgValName).text
        myXmlDictionary["pressureImg" + str(graphCounterSet)] = currentStatus.find(pressureImgValName).text

        graphCounterSet += 1

def writeXML(number, funcType, tagVal, parameters):
    global xmlFile
    global xmlWriter
    global myXmlDictionary

    xmlFile = QFile(tripleVal[1])
    xmlWriter = QXmlStreamWriter()
    xmlWriter.setDevice(xmlFile)

    myDuplicateXmlDictionary = {} #used to transpose values to the actual dictionary later

    writeMain(myDuplicateXmlDictionary)
    writeMed(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)
    writeTest(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)
    writeCurrent(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)

    myXmlDictionary = myDuplicateXmlDictionary
    writeOut(myXmlDictionary)

def writeMain(myDuplicateXmlDictionary):
    #maininfo
    #Who
    myDuplicateXmlDictionary["lastname"] = lastNameEdit.text()
    myDuplicateXmlDictionary["firstname"] = firstNameEdit.text()
    myDuplicateXmlDictionary["ssn"] = ssnValueLabel.text()
    myDuplicateXmlDictionary["marital"] = marValueLabel.text()
    myDuplicateXmlDictionary["DOB"] = dobValueLabel.text()
    myDuplicateXmlDictionary["sex"] = sexValueLabel.text()
    try:
        myDuplicateXmlDictionary["pic"] = subString
    except NameError:
        myDuplicateXmlDictionary["pic"] = ""
    #contact 
    myDuplicateXmlDictionary["address"] = addValueLabel.text()
    myDuplicateXmlDictionary["state"] = statValueLabel.text()
    myDuplicateXmlDictionary["country"] = counValueLabel.text()
    myDuplicateXmlDictionary["email"] = contValueLabel.text()
    myDuplicateXmlDictionary["city"] = cityValueLabel.text()
    myDuplicateXmlDictionary["postalCode"] = postValueLabel.text()
    myDuplicateXmlDictionary["emergencyPhone"] = emeValueLabel.text()
    myDuplicateXmlDictionary["emergencyContact"] = emconValueLabel.text()
    myDuplicateXmlDictionary["workPhone"] = workValueLabel.text()
    myDuplicateXmlDictionary["homePhone"] = homeValueLabel.text()
    myDuplicateXmlDictionary["mobilePhone"] = mobValueLabel.text()
    
    #employer
    myDuplicateXmlDictionary["occupation"] = occValueLabel.text()
    myDuplicateXmlDictionary["employerName"] = emNameValueLabel.text()
    myDuplicateXmlDictionary["employerAddress"] = emAddValueLabel.text()
    myDuplicateXmlDictionary["employerState"] = staValueLabel.text()
    myDuplicateXmlDictionary["employerCountry"] = countValueLabel.text()
    myDuplicateXmlDictionary["employerCity"] = citValueLabel.text()
    myDuplicateXmlDictionary["employerPost"] = postCodValueLabel.text()
    
    #stats
    myDuplicateXmlDictionary["language"] = langValueLabel.text()
    myDuplicateXmlDictionary["race"] = raceValueLabel.text()
    myDuplicateXmlDictionary["familySize"] = famiValueLabel.text()
    myDuplicateXmlDictionary["homeless"] = homlValueLabel.text()
    myDuplicateXmlDictionary["income"] = incoValueLabel.text()
    myDuplicateXmlDictionary["interpreter"] = inteValueLabel.text()

    #insurance
    myDuplicateXmlDictionary["provider"] = primValueLabel.text()
    myDuplicateXmlDictionary["plan"] = planValueLabel.text()
    myDuplicateXmlDictionary["date"] = effeValueLabel.text()
    myDuplicateXmlDictionary["policyNumber"] = plcyValueLabel.text()
    myDuplicateXmlDictionary["groupNumber"] = gropValueLabel.text()
    myDuplicateXmlDictionary["subEmployer"] = subsValueLabel.text()
    myDuplicateXmlDictionary["subscriber"] = scrbValueLabel.text()
    myDuplicateXmlDictionary["relationship"] = relaValueLabel.text()
    myDuplicateXmlDictionary["subDOB"] = dob2ValueLabel.text()
    myDuplicateXmlDictionary["subSsn"] = ssn2ValueLabel.text()
    myDuplicateXmlDictionary["subSex"] = sex2ValueLabel.text()
    myDuplicateXmlDictionary["subAddress"] = sub2ValueLabel.text()
    myDuplicateXmlDictionary["subCity"] = cit2ValueLabel.text()
    myDuplicateXmlDictionary["subState"] = sta2ValueLabel.text()
    myDuplicateXmlDictionary["subZip"] = zip2ValueLabel.text()
    myDuplicateXmlDictionary["subCountry"] = con2ValueLabel.text()
    myDuplicateXmlDictionary["subPhone"] = phonValueLabel.text()

def writeMed(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    #medRecords
    #PUT DIAGNOSES LOOP HERE
    tempDiagCounter = 1
    while tempDiagCounter < diagCounterSet:
        #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["diagID" + str(tempDiagCounter)]
        tempDoctorVar = myXmlDictionary["doctor" + str(tempDiagCounter)] 
        tempDetVar = myXmlDictionary["details" + str(tempDiagCounter)]
        tempDateVar = myXmlDictionary["diagDate" + str(tempDiagCounter)]

        myDuplicateXmlDictionary["diagID" + str(tempDiagCounter)] = tempIDVar
        myDuplicateXmlDictionary["doctor" + str(tempDiagCounter)] = tempDoctorVar
        myDuplicateXmlDictionary["details" + str(tempDiagCounter)] = tempDetVar
        myDuplicateXmlDictionary["diagDate" + str(tempDiagCounter)] = tempDateVar

        tempDiagCounter += 1

    #PUT MEDICATION LOOP HERE
    tempMediCounter = 1
    while tempMediCounter < mediCounterSet:
        #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["mediID" + str(tempMediCounter)]
        tempdosageVar = myXmlDictionary["dosage" + str(tempMediCounter)] 
        tempDetVar = myXmlDictionary["prescriber" + str(tempMediCounter)]
        tempDateVar = myXmlDictionary["mediDate" + str(tempMediCounter)]

        myDuplicateXmlDictionary["mediID" + str(tempMediCounter)] = tempIDVar
        myDuplicateXmlDictionary["dosage" + str(tempMediCounter)] = tempdosageVar
        myDuplicateXmlDictionary["prescriber" + str(tempMediCounter)] = tempDetVar
        myDuplicateXmlDictionary["mediDate" + str(tempMediCounter)] = tempDateVar
        
        tempMediCounter += 1

    #PUT IMMUNIZATION LOOP HERE
    tempImmCounter = 1
    while tempImmCounter < immCounterSet:
        #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["immID" + str(tempImmCounter)]
        tempimmDateVar = myXmlDictionary["immDate" + str(tempImmCounter)] 
        tempDetVar = myXmlDictionary["location" + str(tempImmCounter)]

        myDuplicateXmlDictionary["immID" + str(tempImmCounter)] = tempIDVar
        myDuplicateXmlDictionary["immDate" + str(tempImmCounter)] = tempimmDateVar
        myDuplicateXmlDictionary["location" + str(tempImmCounter)] = tempDetVar
        
        tempImmCounter += 1

    #PUT ALLERGY LOOP HERE
    tempAllCounter = 1
    while tempAllCounter < allCounterSet:
        #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["allID" + str(tempAllCounter)]
        tempseverityVar = myXmlDictionary["severity" + str(tempAllCounter)] 
        tempDetVar = myXmlDictionary["mitigation" + str(tempAllCounter)]

        myDuplicateXmlDictionary["allID" + str(tempAllCounter)] = tempIDVar
        myDuplicateXmlDictionary["severity" + str(tempAllCounter)] = tempseverityVar
        myDuplicateXmlDictionary["mitigation" + str(tempAllCounter)] = tempDetVar
        
        tempAllCounter += 1

def writeTest(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    #testResults
    tempRadCounter = 1
    while tempRadCounter < radCounterSet:
    #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["radImg" + str(tempRadCounter)]
        tempradLocDrVar = myXmlDictionary["radLocDr" + str(tempRadCounter)] 
        tempDetVar = myXmlDictionary["radDate" + str(tempRadCounter)]
        tempDateVar = myXmlDictionary["radNotes" + str(tempRadCounter)]

        myDuplicateXmlDictionary["radImg" + str(tempRadCounter)] = tempIDVar
        myDuplicateXmlDictionary["radLocDr" + str(tempRadCounter)] = tempradLocDrVar
        myDuplicateXmlDictionary["radDate" + str(tempRadCounter)] = tempDetVar
        myDuplicateXmlDictionary["radNotes" + str(tempRadCounter)] = tempDateVar
        
        tempRadCounter += 1

    tempLabCounter = 1
    while tempLabCounter < labCounterSet:
    #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["labImg" + str(tempLabCounter)]
        templabLocDrVar = myXmlDictionary["labLocDr" + str(tempLabCounter)] 
        tempDetVar = myXmlDictionary["labDate" + str(tempLabCounter)]
        tempDateVar = myXmlDictionary["labNotes" + str(tempLabCounter)]

        myDuplicateXmlDictionary["labImg" + str(tempLabCounter)] = tempIDVar
        myDuplicateXmlDictionary["labLocDr" + str(tempLabCounter)] = templabLocDrVar
        myDuplicateXmlDictionary["labDate" + str(tempLabCounter)] = tempDetVar
        myDuplicateXmlDictionary["labNotes" + str(tempLabCounter)] = tempDateVar
        
        tempLabCounter += 1

def writeCurrent(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    #currentstatus
    tempFutCounter = 1
    while tempFutCounter < futCounterSet:
    #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["futLocDr" + str(tempFutCounter)]
        tempdateVar = myXmlDictionary["futDate" + str(tempFutCounter)] 
        tempDetVar = myXmlDictionary["futPurpose" + str(tempFutCounter)]
        tempDateVar = myXmlDictionary["futNotes" + str(tempFutCounter)]

        myDuplicateXmlDictionary["futLocDr" + str(tempFutCounter)] = tempIDVar
        myDuplicateXmlDictionary["futDate" + str(tempFutCounter)] = tempdateVar
        myDuplicateXmlDictionary["futPurpose" + str(tempFutCounter)] = tempDetVar
        myDuplicateXmlDictionary["futNotes" + str(tempFutCounter)] = tempDateVar
        
        tempFutCounter += 1

    tempPastCounter = 1
    while tempPastCounter < pastCounterSet:
    #sets variables to temporaries to be outputted
        tempIDVar = myXmlDictionary["locDr" + str(tempPastCounter)]
        tempDoctorVar = myXmlDictionary["date" + str(tempPastCounter)]
        tempDetVar = myXmlDictionary["height" + str(tempPastCounter)]
        tempDateVar = myXmlDictionary["weight" + str(tempPastCounter)]
        tempIDVar2 = myXmlDictionary["bloodPressure" + str(tempPastCounter)]
        tempDoctorVar2 = myXmlDictionary["pulse" + str(tempPastCounter)]
        tempDetVar2 = myXmlDictionary["notes" + str(tempPastCounter)]
        tempHimg = myXmlDictionary["heightImg" + str(tempPastCounter)]
        tempWimg = myXmlDictionary["weightImg" + str(tempPastCounter)]
        tempPimg = myXmlDictionary["pressureImg" + str(tempPastCounter)]

        myDuplicateXmlDictionary["locDr" + str(tempPastCounter)] = tempIDVar
        myDuplicateXmlDictionary["date" + str(tempPastCounter)] = tempDoctorVar
        myDuplicateXmlDictionary["height" + str(tempPastCounter)] = tempDetVar
        myDuplicateXmlDictionary["weight" + str(tempPastCounter)] = tempDateVar
        myDuplicateXmlDictionary["bloodPressure" + str(tempPastCounter)] = tempIDVar2
        myDuplicateXmlDictionary["pulse" + str(tempPastCounter)] = tempDoctorVar2
        myDuplicateXmlDictionary["notes" + str(tempPastCounter)] = tempDetVar2
        myDuplicateXmlDictionary["heightImg" + str(tempPastCounter)] = tempHimg
        myDuplicateXmlDictionary["weightImg" + str(tempPastCounter)] = tempWimg
        myDuplicateXmlDictionary["pressureImg" + str(tempPastCounter)] = tempPimg
            
        tempPastCounter += 1

class loginTab(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        temp = QWidget()

        mainLayout = QVBoxLayout()
        temp.setLayout(mainLayout)
        self.setGeometry(1,1,0,0)

        self.login1()

    def setExistingDirectory(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal1.setText(str(directory))

    def set2ExistingDirectory(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal2.setText(str(directory))

    def set3ExistingDirectory(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal3.setText(str(directory))

    def checkLogin1(self):
        global tripleVal
        tripleVal = login(userLabelVal1.text(), passBox1.text())
        tempVar = tripleVal[1]
        directory = tempVar.replace("\\", "\\")
        
        listVal = list(tripleVal)
        listVal[1] = directory
        tripleVal = tuple(listVal)
        
        if tripleVal[0] == True:
            self.TabDialog()
            self.close()
        else:
            self.popUpLog1()

    def checkLogin2(self):
        global tripleVal
        tripleVal = login(userLabelVal3.text(), passBox2.text())
        tempVar = tripleVal[1]
        directory = tempVar.replace("\\", "\\")
        
        listVal = list(tripleVal)
        listVal[1] = directory
        tripleVal = tuple(listVal)
        
        if tripleVal[0] == True:
            self.TabDialog()
            self.close()
        else:
            self.popUpLog2()
            #self.close()

    def createUserFinal(self):
        global boolCreate
        boolCreate = createUser(userLabelVal2.text(), pass2Box.text())

        if boolCreate == True:
            self.login2()
            self.window.close
        else:
            self.popUpNew()

    def popUpLog1(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.login1)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)
        
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def popUpLog2(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.login2)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)
        
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def popUpNew(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.newUser)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def login1(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(450)
        self.window.setFixedHeight(300)          
        self.window.show()

        #Sets up boxes of information
        global passBox1
        userLabel = QLabel(self.tr("User Name:"))
        self.setExistingDirectory1 = QPushButton(self.tr("Select USB Directory"))
        self.connect(self.setExistingDirectory1, SIGNAL("clicked()"), self.setExistingDirectory)

        passLabel = QLabel(self.tr("Password:"))
        passBox1 = QLineEdit(self)

        submitButton = QPushButton(self.tr("Login"))
        userButton = QPushButton(self.tr("Create New User"))

        global userLabelVal1
        directoryVal = QLabel(self.tr("Selected Directory:"))
        userLabelVal1 = QLabel("")
        userLabelVal1.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        
        #during submit click login(directory, pass) --> returns boolean, string, integer
        #boolean - determines whether or not login occurs: true goes to next page, false stays on same (login failed popup)
        #string - path to XML: NEEDS TO BE GLOBAL
        #integer - SOMETHING NEEDED FOR ANOTHER METHOD: used for closing

        submitButton.clicked.connect(self.checkLogin1)

        userButton.clicked.connect(self.newUser)
        userButton.clicked.connect(self.close)

        #Sets up two boxes of information
        loginGroup = QGroupBox(self.tr("Login:"))
        submitGroup = QGroupBox(self.tr("Submit:"))

        loginGroup.setFixedWidth(425)
        submitGroup.setFixedWidth(425)

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QHBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        loginGroupLayout.addWidget(self.setExistingDirectory1)
        loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal1)
        loginGroupLayout.addWidget(passLabel)
        loginGroupLayout.addWidget(passBox1)

        submitGroupLayout.addWidget(submitButton)
        submitGroupLayout.addWidget(userButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Overall display
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def newUser(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(475)
        self.window.setFixedHeight(325)       
        self.window.show()

        #boolean - if true: advances again, if fail: Dialogue (failed: try again)
        
        #Information setup
        userLabel = QLabel(self.tr("Username:"))
        self.setExistingDirectory2 = QPushButton(self.tr("Select USB Directory"))
        self.connect(self.setExistingDirectory2, SIGNAL("clicked()"), self.set2ExistingDirectory)

        pass1Label = QLabel(self.tr("Enter Password:"))
        pass1Box = QLineEdit(self)

        global pass2Box
        pass2Label = QLabel(self.tr("Please Re-enter Password:"))
        pass2Box = QLineEdit(self)

        global userLabelVal2
        directoryVal = QLabel(self.tr("Selected Directory:"))
        userLabelVal2 = QLabel("")
        userLabelVal2.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        submitButton = QPushButton(self.tr("Submit"))
        submitButton.clicked.connect(self.createUserFinal)

        #Sets up each of the two boxes
        loginGroup = QGroupBox(self.tr("Create Account Information"))
        loginGroup.setFixedWidth(450)

        submitGroup = QGroupBox()
        submitGroup.setFixedWidth(450)

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QVBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        loginGroupLayout.addWidget(self.setExistingDirectory2)
        loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal2)
        loginGroupLayout.addWidget(pass1Label)
        loginGroupLayout.addWidget(pass1Box)
        loginGroupLayout.addWidget(pass2Label)
        loginGroupLayout.addWidget(pass2Box)

        submitGroupLayout.addWidget(submitButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Final setup of page
        mainLayout = QVBoxLayout(self.window)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def login2(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(450)
        self.window.setFixedHeight(300)          
        self.window.show()

        #Sets up boxes of information
        userLabel = QLabel(self.tr("User Name:"))
        self.setExistingDirectory3 = QPushButton(self.tr("Select USB Directory"))
        self.connect(self.setExistingDirectory3, SIGNAL("clicked()"), self.set3ExistingDirectory)

        global userLabelVal3
        directoryVal = QLabel(self.tr("Selected Directory:"))
        userLabelVal3 = QLabel("")
        userLabelVal3.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global passBox2
        passLabel = QLabel(self.tr("Password:"))
        passBox2 = QLineEdit(self)

        submitButton = QPushButton(self.tr("Login"))
        userButton = QPushButton(self.tr("Create New User"))

        submitButton.clicked.connect(self.checkLogin2)

        userButton.clicked.connect(self.newUser)
        userButton.clicked.connect(self.close)

        #Sets up two boxes of information
        loginGroup = QGroupBox(self.tr("Login:"))
        submitGroup = QGroupBox(self.tr("Submit:"))

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QHBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        loginGroupLayout.addWidget(self.setExistingDirectory3)
        loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal3)
        loginGroupLayout.addWidget(passLabel)
        loginGroupLayout.addWidget(passBox2)

        submitGroupLayout.addWidget(submitButton)
        submitGroupLayout.addWidget(userButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Overall display
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def TabDialog(self):
        self.window = QDialog()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedHeight(600)
        self.window.setFixedWidth(1200)
        self.window.show()

        #Sets up the file bar
        exitAction = QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)   

        #Displays tab interface of the program
        tabWidget = QTabWidget()
        tabWidget.addTab(HomeTab(), self.tr("Home"))
        tabWidget.addTab(MedicalRecordsTab(), self.tr("Medical Records"))
        tabWidget.addTab(TestResultsTab(), self.tr("Test Results"))
        tabWidget.addTab(CurrentStatusTab(), self.tr("Current Status"))
        
        okButton = QPushButton(self.tr("Import"))
        cancelButton = QPushButton(self.tr("Export"))
 
        buttonLayout = QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(okButton)
        buttonLayout.addWidget(cancelButton)
 
        #Sets up main layout of the program: file bar, tabs, button
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonLayout)
        self.window.setLayout(mainLayout)

class HomeTab(QWidget):
    def userSelectFile(self):
        global userDirectory
        global myXmlDictionary
        global subString


        userDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(userDirectory).rfind('/')
        endPos = str(userDirectory).rfind("',")
        subString = str(userDirectory)[startPos+1:endPos]
        editUserImgVal.setText(subString)
        newLocation = theDirectory + "\\imgs\\" + subString
        copy(userDirectory[0], newLocation)
        
        patientmap = QPixmap(newLocation)
        patientmap = patientmap.scaled(200, 200, Qt.KeepAspectRatio)

        patientPhotoID2.setPixmap(patientmap)
        myXmlDictionary["pic"] = subString

    def __init__(self, parent=None): 
        globalEnd = True

        QWidget.__init__(self, parent)
        widget = QWidget()

        global theDirectory
        endPos2 = tripleVal[1].rfind('\\temporary.xml')               
        theDirectory = tripleVal[1][0:endPos2]

        parseXML()

                #Sets all editing variables as global
        global firstNameEdit
        global lastNameEdit
        global dobValueLabel
        global ssnValueLabel
        global sexValueLabel
        global marValueLabel
        global addValueLabel
        global statValueLabel
        global counValueLabel
        global emeValueLabel
        global workValueLabel
        global contValueLabel
        global cityValueLabel
        global postValueLabel
        global emconValueLabel
        global homeValueLabel
        global mobValueLabel
        global occValueLabel
        global emAddValueLabel
        global staValueLabel
        global countValueLabel
        global emNameValueLabel
        global citValueLabel
        global postCodValueLabel
        global langValueLabel
        global raceValueLabel
        global famiValueLabel
        global homlValueLabel
        global incoValueLabel
        global inteValueLabel
        global primValueLabel
        global planValueLabel
        global effeValueLabel
        global plcyValueLabel
        global gropValueLabel
        global subsValueLabel
        global scrbValueLabel
        global relaValueLabel
        global dob2ValueLabel
        global ssn2ValueLabel
        global sex2ValueLabel
        global sub2ValueLabel
        global cit2ValueLabel
        global sta2ValueLabel
        global zip2ValueLabel
        global con2ValueLabel
        global phonValueLabel
        
        #Begins setup for the GUI interface
        '''WHO'''
        firstNameLabel = QLabel(self.tr("First Name:"))
        firstNameLabel.setFixedWidth(175)
        firstNameEdit = QLineEdit(firstNameVal)
        
        firstName = QGroupBox()
        firstNameLayout = QVBoxLayout()

        firstNameLayout.addWidget(firstNameLabel)
        firstNameLayout.addWidget(firstNameEdit)
        firstName.setLayout(firstNameLayout)

        lastNameLabel = QLabel(self.tr("Last Name:"))
        lastNameLabel.setFixedWidth(200)
        lastNameEdit = QLineEdit(lastNameVal)
        
        lastName = QGroupBox()
        lastNameLayout = QVBoxLayout()

        lastNameLayout.addWidget(lastNameLabel)
        lastNameLayout.addWidget(lastNameEdit)
        lastName.setLayout(lastNameLayout)

        patientName = QGroupBox()
        patientNameLayout = QHBoxLayout()

        patientNameLayout.addWidget(firstName)
        patientNameLayout.addWidget(lastName)

        patientName.setLayout(patientNameLayout)

        dobLabel = QLabel(self.tr("Date of Birth:"))
        dobValueLabel = QLineEdit(dobVal)
        
        ssnLabel = QLabel(self.tr("Social Security Number:"))
        ssnValueLabel = QLineEdit(ssnVal)
        
        sexLabel = QLabel(self.tr("Sex:"))
        sexValueLabel = QLineEdit(sexVal)
        
        marLabel = QLabel(self.tr("Marital Status:"))
        marValueLabel = QLineEdit(marVal)
        
        imgButton = QPushButton("Select Picture")
        imgButton.clicked.connect(self.userSelectFile)

        updateButton = QPushButton("Update Information")
        updateButton.clicked.connect(self.XMLWrite)

        global editUserImgVal
        editUserImgVal = QLabel("")
        editUserImgVal.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        picDir = theDirectory + "\\imgs\\" + str(picVal)
        
        global patientPhotoID2
        patientmap = QPixmap(picDir)
        patientPhotoID2 = QLabel()
        patientmap = patientmap.scaled(200, 200, Qt.KeepAspectRatio)
        patientPhotoID2.setPixmap(patientmap)
        patientPhotoID2.setFixedWidth(200)
        patientPhotoID2.setFixedHeight(150)

        patientPhotoID = QGroupBox()
        patientPhotoIDLayout = QVBoxLayout()
        patientPhotoIDLayout.addWidget(patientPhotoID2)
        patientPhotoIDLayout.addWidget(imgButton)
        patientPhotoIDLayout.addWidget(editUserImgVal)

        patientPhotoID.setLayout(patientPhotoIDLayout)

        '''CONTACT'''
        addLabel = QLabel(self.tr("Address:"))
        addValueLabel = QLineEdit(addVal)

        statLabel = QLabel(self.tr("State:"))
        statValueLabel = QLineEdit(statVal)
        
        counLabel = QLabel(self.tr("Country:"))
        counValueLabel = QLineEdit(countVal)
        
        emeLabel = QLabel(self.tr("Emergency Phone:"))
        emeValueLabel = QLineEdit(emPhoneVal)
        
        workLabel = QLabel(self.tr("Work Phone:"))
        workValueLabel = QLineEdit(workPhoneVal)
        
        contLabel = QLabel(self.tr("Contact Email:"))
        contValueLabel = QLineEdit(emailVal)
        
        cityLabel = QLabel(self.tr("City:"))
        cityValueLabel = QLineEdit(cityVal)
        
        postLabel = QLabel(self.tr("Postal Code:"))
        postValueLabel = QLineEdit(postVal)
        
        emconLabel = QLabel(self.tr("Emergency Contact:"))
        emconValueLabel = QLineEdit(emContVal)
        
        homeLabel = QLabel(self.tr("Home Phone:"))
        homeValueLabel = QLineEdit(homePhoneVal)
        
        mobLabel = QLabel(self.tr("Mobile Phone:"))
        mobValueLabel = QLineEdit(mobPhoneVal)
        
        '''EMPLOYER'''
        occLabel = QLabel(self.tr("Occupation:"))
        occValueLabel = QLineEdit(occVal)
        
        emAddLabel = QLabel(self.tr("Employer Address:"))
        emAddValueLabel = QLineEdit(empAddVal)
        
        staLabel = QLabel(self.tr("State:"))
        staValueLabel = QLineEdit(empStatVal)
        
        countLabel = QLabel(self.tr("Country:"))
        countValueLabel = QLineEdit(empCountVal)
        
        emNameLabel = QLabel(self.tr("Employer Name:"))
        emNameValueLabel = QLineEdit(empNameVal)
        
        citLabel = QLabel(self.tr("City:"))
        citValueLabel = QLineEdit(empCityVal)
        
        postCodLabel = QLabel(self.tr("Postal Code:"))
        postCodValueLabel = QLineEdit(empPostVal)
        
        '''STATS'''
        langLabel = QLabel(self.tr("Language:"))
        langValueLabel = QLineEdit(langVal)
        
        raceLabel = QLabel(self.tr("Race/Ethnicity:"))
        raceValueLabel = QLineEdit(raceVal)
        
        famiLabel = QLabel(self.tr("Family Size:"))
        famiValueLabel = QLineEdit(famSizeVal)
        
        homlLabel = QLabel(self.tr("Homeless, etc.:"))
        homlValueLabel = QLineEdit(homeVal)
        
        incoLabel = QLabel(self.tr("Monthly Income:"))
        incoValueLabel = QLineEdit(incVal)

        inteLabel = QLabel(self.tr("Interpreter:"))
        inteValueLabel = QLineEdit(interVal)

        '''INSURANCE'''
        primLabel = QLabel(self.tr("Primary Insurance Provider:"))
        primValueLabel = QLineEdit(primVal)

        planLabel = QLabel(self.tr("Plan Name:"))
        planValueLabel = QLineEdit(planVal)

        effeLabel = QLabel(self.tr("Effective Date:"))
        effeValueLabel = QLineEdit(effDateVal)

        plcyLabel = QLabel(self.tr("Policy Number:"))
        plcyValueLabel = QLineEdit(policyVal)

        gropLabel = QLabel(self.tr("Group Number:"))
        gropValueLabel = QLineEdit(groupVal)

        subsLabel = QLabel(self.tr("Subscriber Employer (SE):"))
        subsValueLabel = QLineEdit(subsEmpVal)

        scrbLabel = QLabel(self.tr("Subscriber:"))
        scrbValueLabel = QLineEdit(subsVal)

        relaLabel = QLabel(self.tr("Relationship:"))
        relaValueLabel = QLineEdit(relateVal)

        dob2Label = QLabel(self.tr("Date of Birth:"))
        dob2ValueLabel = QLineEdit(insDobVal)

        ssn2Label = QLabel(self.tr("Social Security Number:"))
        ssn2ValueLabel = QLineEdit(insSsnVal)

        sex2Label = QLabel(self.tr("Sex:"))
        sex2ValueLabel = QLineEdit(insSexVal)

        sub2Label = QLabel(self.tr("Subscriber Address:"))
        sub2ValueLabel = QLineEdit(insAddVal)

        cit2Label = QLabel(self.tr("City:"))
        cit2ValueLabel = QLineEdit(insCityVal)

        sta2Label = QLabel(self.tr("State:"))
        sta2ValueLabel = QLineEdit(insStateVal)

        zip2Label = QLabel(self.tr("Zip Code:"))
        zip2ValueLabel = QLineEdit(insZipVal)

        con2Label = QLabel(self.tr("Country:"))
        con2ValueLabel = QLineEdit(insCountVal)

        phonLabel = QLabel(self.tr("Subscriber Phone:"))
        phonValueLabel = QLineEdit(insPhoneVal)

        #Box off each section of information displayed
        WhoRecordsGroup = QGroupBox(self.tr("Who"))
        ContactRecordsGroup = QGroupBox(self.tr("Contact"))
        EmployerRecordsGroup = QGroupBox(self.tr("Employer"))
        StatsRecordsGroup = QGroupBox(self.tr("Stats"))
        InsuranceRecordsGroup = QGroupBox(self.tr("Insurance"))
       
        #Assigns information to each box
        #WHO GROUP
        whoMiniGroup1 = QGroupBox()
        whoMiniGroup2 = QGroupBox()
        whoMiniGroup3 = QGroupBox()

        whoMiniGroup1Layout = QVBoxLayout()
        whoMiniGroup2Layout = QVBoxLayout()
        whoMiniGroup3Layout = QVBoxLayout()

        whoMiniGroup1Layout.addWidget(patientName)
        whoMiniGroup1Layout.addWidget(ssnLabel)
        whoMiniGroup1Layout.addWidget(ssnValueLabel)
        whoMiniGroup1Layout.addWidget(marLabel)
        whoMiniGroup1Layout.addWidget(marValueLabel)

        whoMiniGroup2Layout.addWidget(dobLabel)
        whoMiniGroup2Layout.addWidget(dobValueLabel)
        whoMiniGroup2Layout.addWidget(sexLabel)
        whoMiniGroup2Layout.addWidget(sexValueLabel)

        whoMiniGroup3Layout.addWidget(patientPhotoID)

        whoMiniGroup1.setLayout(whoMiniGroup1Layout)
        whoMiniGroup2.setLayout(whoMiniGroup2Layout)        
        whoMiniGroup3.setLayout(whoMiniGroup3Layout)

        WhoRecordsLayout = QHBoxLayout()
        WhoRecordsLayout.addWidget(whoMiniGroup1)
        WhoRecordsLayout.addWidget(whoMiniGroup2)
        WhoRecordsLayout.addWidget(whoMiniGroup3)
        WhoRecordsGroup.setLayout(WhoRecordsLayout)

        #CONTACT GROUP
        contactMiniGroup1 = QGroupBox()
        contactMiniGroup2 = QGroupBox()

        contactMiniGroup1Layout = QVBoxLayout()
        contactMiniGroup2Layout = QVBoxLayout()

        contactMiniGroup1Layout.addWidget(addLabel)
        contactMiniGroup1Layout.addWidget(addValueLabel)
        contactMiniGroup1Layout.addWidget(statLabel)
        contactMiniGroup1Layout.addWidget(statValueLabel)
        contactMiniGroup1Layout.addWidget(counLabel)
        contactMiniGroup1Layout.addWidget(counValueLabel)
        contactMiniGroup1Layout.addWidget(contLabel)
        contactMiniGroup1Layout.addWidget(contValueLabel)
        contactMiniGroup1Layout.addWidget(cityLabel)
        contactMiniGroup1Layout.addWidget(cityValueLabel)
        contactMiniGroup1Layout.addWidget(postLabel)
        contactMiniGroup1Layout.addWidget(postValueLabel)
        
        contactMiniGroup2Layout.addWidget(emeLabel)
        contactMiniGroup2Layout.addWidget(emeValueLabel)
        contactMiniGroup2Layout.addWidget(emconLabel)
        contactMiniGroup2Layout.addWidget(emconValueLabel)
        contactMiniGroup2Layout.addWidget(workLabel)
        contactMiniGroup2Layout.addWidget(workValueLabel)
        contactMiniGroup2Layout.addWidget(homeLabel)
        contactMiniGroup2Layout.addWidget(homeValueLabel)
        contactMiniGroup2Layout.addWidget(mobLabel)
        contactMiniGroup2Layout.addWidget(mobValueLabel)

        contactMiniGroup1.setLayout(contactMiniGroup1Layout)
        contactMiniGroup2.setLayout(contactMiniGroup2Layout)

        ContactRecordsLayout = QHBoxLayout()
        ContactRecordsLayout.addWidget(contactMiniGroup1)
        ContactRecordsLayout.addWidget(contactMiniGroup2)
        ContactRecordsGroup.setLayout(ContactRecordsLayout)

        #EMPLOYER GROUP
        employerMiniGroup1 = QGroupBox()
        employerMiniGroup2 = QGroupBox()

        employerMiniGroup1Layout = QVBoxLayout()
        employerMiniGroup2Layout = QVBoxLayout()

        employerMiniGroup1Layout.addWidget(occLabel)
        employerMiniGroup1Layout.addWidget(occValueLabel)
        employerMiniGroup1Layout.addWidget(emNameLabel)
        employerMiniGroup1Layout.addWidget(emNameValueLabel)
        employerMiniGroup1Layout.addWidget(emAddLabel)
        employerMiniGroup1Layout.addWidget(emAddValueLabel)

        employerMiniGroup2Layout.addWidget(staLabel)
        employerMiniGroup2Layout.addWidget(staValueLabel)
        employerMiniGroup2Layout.addWidget(countLabel)
        employerMiniGroup2Layout.addWidget(countValueLabel)
        employerMiniGroup2Layout.addWidget(citLabel)
        employerMiniGroup2Layout.addWidget(citValueLabel)
        employerMiniGroup2Layout.addWidget(postCodLabel)
        employerMiniGroup2Layout.addWidget(postCodValueLabel)

        employerMiniGroup1.setLayout(employerMiniGroup1Layout)
        employerMiniGroup2.setLayout(employerMiniGroup2Layout)

        EmployerRecordsLayout = QHBoxLayout()
        EmployerRecordsLayout.addWidget(employerMiniGroup1)
        EmployerRecordsLayout.addWidget(employerMiniGroup2)
        EmployerRecordsGroup.setLayout(EmployerRecordsLayout)

        #STATS GROUP
        statsMiniGroup1 = QGroupBox()
        statsMiniGroup2 = QGroupBox()

        statsMiniGroup1Layout = QVBoxLayout()
        statsMiniGroup2Layout = QVBoxLayout()

        statsMiniGroup1Layout.addWidget(langLabel)
        statsMiniGroup1Layout.addWidget(langValueLabel)
        statsMiniGroup1Layout.addWidget(raceLabel)
        statsMiniGroup1Layout.addWidget(raceValueLabel)
        statsMiniGroup1Layout.addWidget(famiLabel)
        statsMiniGroup1Layout.addWidget(famiValueLabel)

        statsMiniGroup2Layout.addWidget(homlLabel)
        statsMiniGroup2Layout.addWidget(homlValueLabel)
        statsMiniGroup2Layout.addWidget(incoLabel)
        statsMiniGroup2Layout.addWidget(incoValueLabel)
        statsMiniGroup2Layout.addWidget(inteLabel)
        statsMiniGroup2Layout.addWidget(inteValueLabel)

        statsMiniGroup1.setLayout(statsMiniGroup1Layout)
        statsMiniGroup2.setLayout(statsMiniGroup2Layout)

        StatsRecordsLayout = QHBoxLayout()
        StatsRecordsLayout.addWidget(statsMiniGroup1)
        StatsRecordsLayout.addWidget(statsMiniGroup2)
        StatsRecordsGroup.setLayout(StatsRecordsLayout)

        #INSURANCE GROUP
        insuranceMiniGroup1 = QGroupBox()
        insuranceMiniGroup2 = QGroupBox()

        insuranceMiniGroup1Layout = QVBoxLayout()
        insuranceMiniGroup2Layout = QVBoxLayout()

        insuranceMiniGroup1Layout.addWidget(primLabel)
        insuranceMiniGroup1Layout.addWidget(primValueLabel)
        insuranceMiniGroup1Layout.addWidget(planLabel)
        insuranceMiniGroup1Layout.addWidget(planValueLabel)
        insuranceMiniGroup1Layout.addWidget(effeLabel)
        insuranceMiniGroup1Layout.addWidget(effeValueLabel)
        insuranceMiniGroup1Layout.addWidget(plcyLabel)
        insuranceMiniGroup1Layout.addWidget(plcyValueLabel)
        insuranceMiniGroup1Layout.addWidget(gropLabel)
        insuranceMiniGroup1Layout.addWidget(gropValueLabel)
        insuranceMiniGroup1Layout.addWidget(subsLabel)
        insuranceMiniGroup1Layout.addWidget(subsValueLabel)
        insuranceMiniGroup1Layout.addWidget(scrbLabel)
        insuranceMiniGroup1Layout.addWidget(scrbValueLabel)
        insuranceMiniGroup1Layout.addWidget(relaLabel)
        insuranceMiniGroup1Layout.addWidget(relaValueLabel)

        insuranceMiniGroup2Layout.addWidget(dob2Label)
        insuranceMiniGroup2Layout.addWidget(dob2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(ssn2Label)
        insuranceMiniGroup2Layout.addWidget(ssn2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sex2Label)
        insuranceMiniGroup2Layout.addWidget(sex2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sub2Label)
        insuranceMiniGroup2Layout.addWidget(sub2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(cit2Label)
        insuranceMiniGroup2Layout.addWidget(cit2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sta2Label)
        insuranceMiniGroup2Layout.addWidget(sta2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(zip2Label)
        insuranceMiniGroup2Layout.addWidget(zip2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(con2Label)
        insuranceMiniGroup2Layout.addWidget(con2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(phonLabel)
        insuranceMiniGroup2Layout.addWidget(phonValueLabel)

        insuranceMiniGroup1.setLayout(insuranceMiniGroup1Layout)
        insuranceMiniGroup2.setLayout(insuranceMiniGroup2Layout)

        InsuranceRecordsLayout = QHBoxLayout()
        InsuranceRecordsLayout.addWidget(insuranceMiniGroup1)
        InsuranceRecordsLayout.addWidget(insuranceMiniGroup2)
        InsuranceRecordsGroup.setLayout(InsuranceRecordsLayout)

        #Puts together each of the boxes
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(WhoRecordsGroup)
        mainLayout.addWidget(ContactRecordsGroup)
        mainLayout.addWidget(EmployerRecordsGroup)
        mainLayout.addWidget(StatsRecordsGroup)
        mainLayout.addWidget(InsuranceRecordsGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        vLayout.addWidget(updateButton)
        self.setLayout(vLayout)

    def XMLWrite(self, parent=None):
        myXmlDictionary["firstname"] = firstNameEdit.text()
        myXmlDictionary["lastname"] = lastNameEdit.text()
        myXmlDictionary["DOB"] = dobValueLabel.text()
        myXmlDictionary["ssn"] = ssnValueLabel.text()
        myXmlDictionary["sex"] = sexValueLabel.text()
        myXmlDictionary["marital"] = marValueLabel.text()
        try:
            myXmlDictionary["pic"] = userDirectory[0]
        except NameError:
            myXmlDictionary["pic"] = ""
        myXmlDictionary["address"] = addValueLabel.text()
        myXmlDictionary["state"] = statValueLabel.text()
        myXmlDictionary["country"] = counValueLabel.text()
        myXmlDictionary["emergencyPhone"] = emeValueLabel.text()
        myXmlDictionary["workPhone"] = workValueLabel.text()
        myXmlDictionary["email"] = contValueLabel.text()
        myXmlDictionary["city"] = cityValueLabel.text()
        myXmlDictionary["postalCode"] = postValueLabel.text()
        myXmlDictionary["emergencyContact"] = emconValueLabel.text()
        myXmlDictionary["homePhone"] = homeValueLabel.text()
        myXmlDictionary["mobilePhone"] = mobValueLabel.text()

        myXmlDictionary["occupation"] = occValueLabel.text()
        myXmlDictionary["employerName"] = emAddValueLabel.text()
        myXmlDictionary["employerAddress"] = staValueLabel.text()
        myXmlDictionary["employerState"] = countValueLabel.text()
        myXmlDictionary["employerCountry"] = emNameValueLabel.text()
        myXmlDictionary["employerCity"] = citValueLabel.text()
        myXmlDictionary["employerPost"] = postCodValueLabel.text()

        myXmlDictionary["language"] = langValueLabel.text()
        myXmlDictionary["race"] = raceValueLabel.text()
        myXmlDictionary["familySize"] = famiValueLabel.text()
        myXmlDictionary["homeless"] = homlValueLabel.text()
        myXmlDictionary["income"] = incoValueLabel.text()
        myXmlDictionary["interpreter"] = inteValueLabel.text()

        myXmlDictionary["provider"] = primValueLabel.text()
        myXmlDictionary["plan"] = planValueLabel.text()
        myXmlDictionary["date"] = effeValueLabel.text()
        myXmlDictionary["policyNumber"] = plcyValueLabel.text()
        myXmlDictionary["groupNumber"] = gropValueLabel.text()
        myXmlDictionary["subEmployer"] = subsValueLabel.text()
        myXmlDictionary["subscriber"] = scrbValueLabel.text()
        myXmlDictionary["relationship"] = relaValueLabel.text()
        myXmlDictionary["subDOB"] = dob2ValueLabel.text()
        myXmlDictionary["subSsn"] = ssn2ValueLabel.text()
        myXmlDictionary["subSex"] = sex2ValueLabel.text()
        myXmlDictionary["subAddress"] = sub2ValueLabel.text()
        myXmlDictionary["subCity"] = cit2ValueLabel.text()
        myXmlDictionary["subState"] = sta2ValueLabel.text()
        myXmlDictionary["subZip"] = zip2ValueLabel.text()
        myXmlDictionary["subCountry"] = con2ValueLabel.text()
        myXmlDictionary["subPhone"] = phonValueLabel.text()
        writeXML(0,"","","")

class MedicalRecordsTab(QWidget):
    def __init__(self, parent=None):
        content = ET.parse(tripleVal[1])
        medRecords=content.find("medRecords")
        diagCounter = 1
        mediCounter = 1
        immCounter = 1
        allCounter = 1

        global diagValueLabel
        global diagDoctValueLabel
        global detaValueLabel
        global diagDateValueLabel
        global mediValueLabel
        global doseValueLabel
        global preValueLabel
        global datepValueLabel
        global immValueLabel
        global dateiValueLabel
        global lociValueLabel
        global allValueLabel
        global seveValueLabel
        global mitiValueLabel

        QWidget.__init__(self, parent)
        widget = QWidget()
       
        #DIAGNOSES LIST
        DiagnosesStatusListBox = diagnosisWidget()
        DiagnosesStatus = []
        
        while "diagID" + str(diagCounter) in myXmlDictionary: 
            tempVar = "Diagnosis #" + str(diagCounter)
            DiagnosesStatus.append(tempVar)
            diagCounter += 1
        DiagnosesStatusListBox.insertItems(0, DiagnosesStatus)

        DiagnosesStatusListBox.connect(DiagnosesStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               DiagnosesStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))
        
        #MEDICATION LIST
        MedicationStatusListBox = medicationWidget()
        MedicationStatus = []
 
        while "mediID" + str(mediCounter) in myXmlDictionary:
            tempVar = "Medication #" + str(mediCounter)
            MedicationStatus.append(tempVar)
            mediCounter += 1
        MedicationStatusListBox.insertItems(0, MedicationStatus)
        MedicationStatusListBox.connect(MedicationStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               MedicationStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #IMMUNIZATION LIST
        ImmunizationStatusListBox = immunizationWidget()
        ImmunizationStatus = []
 
        while "immID" + str(immCounter) in myXmlDictionary: 
            tempVar = "Immunization #" + str(immCounter)
            ImmunizationStatus.append(tempVar)
            immCounter += 1
        ImmunizationStatusListBox.insertItems(0, ImmunizationStatus)
        ImmunizationStatusListBox.connect(ImmunizationStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               ImmunizationStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #ALLERGIES LIST
        AllergiesStatusListBox = allergyWidget()
        AllergiesStatus = []

        while "allID" + str(allCounter) in myXmlDictionary:
            tempVar = "Allergy #" + str(allCounter)
            AllergiesStatus.append(tempVar)
            allCounter += 1
        AllergiesStatusListBox.insertItems(0, AllergiesStatus)
        AllergiesStatusListBox.connect(AllergiesStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               AllergiesStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Sets up side boxes information
        #Diagnoses
        diagLabel = QLabel(self.tr("Diagnosis:"))
        diagValueLabel = QLabel("")
        diagValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        doctLabel = QLabel(self.tr("Doctor:"))
        diagDoctValueLabel = QLabel("")
        diagDoctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        detaLabel = QLabel(self.tr("Detailed Diagnosis:"))
        detaValueLabel = QLabel("")
        detaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        dateLabel = QLabel(self.tr("Date:"))
        diagDateValueLabel = QLabel("")
        diagDateValueLabel.setFixedWidth(400)
        diagDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Medication
        mediLabel = QLabel(self.tr("Medication:"))
        mediValueLabel = QLabel("")
        mediValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        doseLabel = QLabel(self.tr("Daily Dosage:"))
        doseValueLabel = QLabel("")
        doseValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        preLabel = QLabel(self.tr("Prescriber:"))
        preValueLabel = QLabel("")
        preValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        datepLabel = QLabel(self.tr("Date Prescribed:"))
        datepValueLabel = QLabel("")
        datepValueLabel.setFixedWidth(400)
        datepValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Immunize
        immLabel = QLabel(self.tr("Date Immunized:"))
        immValueLabel = QLabel("")
        immValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        dateiLabel = QLabel(self.tr("Date Immunized:"))
        dateiValueLabel = QLabel("")
        dateiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        lociLabel = QLabel(self.tr("Location:"))
        lociValueLabel = QLabel("")
        lociValueLabel.setFixedWidth(400)
        lociValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Allergies
        allLabel = QLabel(self.tr("Severity:"))
        allValueLabel = QLabel("")
        allValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        seveLabel = QLabel(self.tr("Severity:"))
        seveValueLabel = QLabel("")
        seveValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        mitiLabel = QLabel(self.tr("Mitigation Technique:"))
        mitiLabel.setFixedWidth(400)
        mitiValueLabel = QLabel("")
        mitiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Sets up final boxes
        DiagnosisVBox = QGroupBox()
        MedicationVBox = QGroupBox()
        ImmunizationVBox = QGroupBox()
        AllergiesVBox = QGroupBox()

        DiagnosisVBoxLayout = QVBoxLayout()
        MedicationVBoxLayout = QVBoxLayout()
        ImmunizationVBoxLayout = QVBoxLayout()
        AllergiesVBoxLayout = QVBoxLayout()

        DiagnosisVBoxLayout.addWidget(diagLabel)
        DiagnosisVBoxLayout.addWidget(diagValueLabel)
        DiagnosisVBoxLayout.addWidget(doctLabel)
        DiagnosisVBoxLayout.addWidget(diagDoctValueLabel)
        DiagnosisVBoxLayout.addWidget(detaLabel)
        DiagnosisVBoxLayout.addWidget(detaValueLabel)
        DiagnosisVBoxLayout.addWidget(dateLabel)
        DiagnosisVBoxLayout.addWidget(diagDateValueLabel)

        MedicationVBoxLayout.addWidget(mediLabel)
        MedicationVBoxLayout.addWidget(mediValueLabel)
        MedicationVBoxLayout.addWidget(doseLabel)
        MedicationVBoxLayout.addWidget(doseValueLabel)
        MedicationVBoxLayout.addWidget(preLabel)
        MedicationVBoxLayout.addWidget(preValueLabel)
        MedicationVBoxLayout.addWidget(datepLabel)
        MedicationVBoxLayout.addWidget(datepValueLabel)

        ImmunizationVBoxLayout.addWidget(immLabel)
        ImmunizationVBoxLayout.addWidget(immValueLabel)
        ImmunizationVBoxLayout.addWidget(dateiLabel)
        ImmunizationVBoxLayout.addWidget(dateiValueLabel)
        ImmunizationVBoxLayout.addWidget(lociLabel)
        ImmunizationVBoxLayout.addWidget(lociValueLabel)

        AllergiesVBoxLayout.addWidget(allLabel)
        AllergiesVBoxLayout.addWidget(allValueLabel)
        AllergiesVBoxLayout.addWidget(seveLabel)
        AllergiesVBoxLayout.addWidget(seveValueLabel)
        AllergiesVBoxLayout.addWidget(mitiLabel)
        AllergiesVBoxLayout.addWidget(mitiValueLabel)

        DiagnosisVBox.setLayout(DiagnosisVBoxLayout)
        MedicationVBox.setLayout(MedicationVBoxLayout)
        ImmunizationVBox.setLayout(ImmunizationVBoxLayout)
        AllergiesVBox.setLayout(AllergiesVBoxLayout)

        #Displays each of list on page
        DiagnosesGroup = QGroupBox(self.tr("Diagnoses"))
        MedicationGroup = QGroupBox(self.tr("Medication"))
        ImmunizationGroup = QGroupBox(self.tr("Immunization"))
        AllergiesGroup = QGroupBox(self.tr("Allergies"))

        DiagnosesGroupLayout = QHBoxLayout()
        DiagnosesGroupLayout.addWidget(DiagnosesStatusListBox)
        DiagnosesGroupLayout.addWidget(DiagnosisVBox)

        MedicationGroupLayout = QHBoxLayout()
        MedicationGroupLayout.addWidget(MedicationStatusListBox)
        MedicationGroupLayout.addWidget(MedicationVBox)

        ImmunizationGroupLayout = QHBoxLayout()
        ImmunizationGroupLayout.addWidget(ImmunizationStatusListBox)
        ImmunizationGroupLayout.addWidget(ImmunizationVBox)

        AllergiesGroupLayout = QHBoxLayout()
        AllergiesGroupLayout.addWidget(AllergiesStatusListBox)
        AllergiesGroupLayout.addWidget(AllergiesVBox)

        DiagnosesGroup.setLayout(DiagnosesGroupLayout)
        MedicationGroup.setLayout(MedicationGroupLayout)
        ImmunizationGroup.setLayout(ImmunizationGroupLayout)
        AllergiesGroup.setLayout(AllergiesGroupLayout)

        #Overall layout
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(DiagnosesGroup)
        mainLayout.addWidget(MedicationGroup)
        mainLayout.addWidget(ImmunizationGroup)
        mainLayout.addWidget(AllergiesGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)
 
class diagnosisWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]

        tempVar1 = myXmlDictionary["diagID" + str(numberVal)]
        tempVar2 = myXmlDictionary["doctor" + str(numberVal)]
        tempVar3 = myXmlDictionary["details" + str(numberVal)]
        tempVar4 = myXmlDictionary["diagDate" + str(numberVal)]

        diagValueLabel.setText(tempVar1)
        diagDoctValueLabel.setText(tempVar2)
        detaValueLabel.setText(tempVar3)
        diagDateValueLabel.setText(tempVar4)

class medicationWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["mediID" + str(numberVal)]
        tempVar2 = myXmlDictionary["dosage" + str(numberVal)]
        tempVar3 = myXmlDictionary["prescriber" + str(numberVal)]
        tempVar4 = myXmlDictionary["mediDate" + str(numberVal)]

        mediValueLabel.setText(tempVar1)
        doseValueLabel.setText(tempVar2)
        preValueLabel.setText(tempVar3)
        datepValueLabel.setText(tempVar4)

class immunizationWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["immID" + str(numberVal)]
        tempVar2 = myXmlDictionary["immDate" + str(numberVal)]
        tempVar3 = myXmlDictionary["location" + str(numberVal)]

        immValueLabel.setText(tempVar1)
        dateiValueLabel.setText(tempVar2)
        lociValueLabel.setText(tempVar3)
        
class allergyWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["allID" + str(numberVal)]
        tempVar2 = myXmlDictionary["severity" + str(numberVal)]
        tempVar3 = myXmlDictionary["mitigation" + str(numberVal)]
        
        allValueLabel.setText(tempVar1)
        seveValueLabel.setText(tempVar2)
        mitiValueLabel.setText(tempVar3)
        
class TestResultsTab(QWidget):
    def __init__(self, parent=None):
        content = ET.parse(tripleVal[1])
        testResults=content.find("testResults")

        labCounter = 1
        radCounter = 1

        global doctValueLabel
        global radDateValueLabel
        global radNoteValueLabel
        global labLocaValueLabel
        global labDate2ValueLabel
        global note2ValueLabel
        global radiomap
        global radioImg
        global labmap
        global labImg

        QWidget.__init__(self, parent)
        widget = QWidget()
       
        #RADIOLOGY LIST
        RadiologyListBox = radWidget()
        RadiologyStatus = []
        
        while "radImg" + str(radCounter) in myXmlDictionary:
            tempVar = "Radiology Scan #" + str(radCounter)
            RadiologyStatus.append(tempVar)
            radCounter += 1
        RadiologyListBox.insertItems(0, RadiologyStatus)
        RadiologyListBox.connect(RadiologyListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               RadiologyListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #LAB TEST LIST
        LabStatusListBox = labWidget()
        LabStatus = []
 
        while "labImg" + str(labCounter) in myXmlDictionary:
            tempVar = "Lab Test #" + str(labCounter)
            LabStatus.append(tempVar)
            labCounter += 1
        LabStatusListBox.insertItems(0, LabStatus)
        LabStatusListBox.connect(LabStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               LabStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Sets up side boxes information
        #Radiology
        doctLabel = QLabel(self.tr("Location/Doctor:"))
        doctValueLabel = QLabel("")
        doctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        dateLabel = QLabel(self.tr("Date:"))
        radDateValueLabel = QLabel("")
        radDateValueLabel.setFixedWidth(250)
        radDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        noteLabel = QLabel(self.tr("Notes:"))
        radNoteValueLabel = QLabel("")
        radNoteValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Lab Tests
        locaLabel = QLabel(self.tr("Location/Doctor:"))
        labLocaValueLabel = QLabel("")
        labLocaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        date2Label = QLabel(self.tr("Date:"))
        labDate2ValueLabel = QLabel("")
        labDate2ValueLabel.setFixedWidth(250)
        labDate2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        note2Label = QLabel(self.tr("Notes:"))
        note2ValueLabel = QLabel("")
        note2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Test Images
        radiomap = QPixmap("")
        radioImg = QLabel()
        radioImg.setPixmap(radiomap)

        radioImg.setFixedWidth(225)
        radioImg.setFixedHeight(225)

        labmap = QPixmap("")
        labImg = QLabel()

        labImg.setPixmap(labmap)
        labImg.setFixedWidth(225)
        labImg.setFixedHeight(225)
        
        #Set up picture boxes
        RadiologyPicBox = QGroupBox()
        LabPicBox = QGroupBox()

        RadiologyPicBoxLayout = QVBoxLayout()
        LabPicBoxLayout = QVBoxLayout()

        RadiologyPicBoxLayout.addWidget(radioImg)
        LabPicBoxLayout.addWidget(labImg)

        RadiologyPicBox.setLayout(RadiologyPicBoxLayout)
        LabPicBox.setLayout(LabPicBoxLayout)

        #Sets up final boxes
        RadiologyVBox = QGroupBox()
        LabVBox = QGroupBox()

        RadiologyVBoxLayout = QVBoxLayout()
        LabVBoxLayout = QVBoxLayout()

        RadiologyVBoxLayout.addWidget(doctLabel)
        RadiologyVBoxLayout.addWidget(doctValueLabel)
        RadiologyVBoxLayout.addWidget(dateLabel)
        RadiologyVBoxLayout.addWidget(radDateValueLabel)
        RadiologyVBoxLayout.addWidget(noteLabel)
        RadiologyVBoxLayout.addWidget(radNoteValueLabel)

        LabVBoxLayout.addWidget(locaLabel)
        LabVBoxLayout.addWidget(labLocaValueLabel)
        LabVBoxLayout.addWidget(date2Label)
        LabVBoxLayout.addWidget(labDate2ValueLabel)
        LabVBoxLayout.addWidget(note2Label)
        LabVBoxLayout.addWidget(note2ValueLabel)
    
        RadiologyVBox.setLayout(RadiologyVBoxLayout)
        LabVBox.setLayout(LabVBoxLayout)
        
        #Displays each of list on page
        RadiologyGroup = QGroupBox(self.tr("Radiology Scans:"))
        LabGroup = QGroupBox(self.tr("Lab/Test Results:"))
        
        RadiologyGroupLayout = QHBoxLayout()
        RadiologyGroupLayout.addWidget(RadiologyListBox)
        RadiologyGroupLayout.addWidget(RadiologyPicBox)
        RadiologyGroupLayout.addWidget(RadiologyVBox)

        LabGroupLayout = QHBoxLayout()
        LabGroupLayout.addWidget(LabStatusListBox)
        LabGroupLayout.addWidget(LabPicBox)
        LabGroupLayout.addWidget(LabVBox)

        RadiologyGroup.setLayout(RadiologyGroupLayout)
        LabGroup.setLayout(LabGroupLayout)

        #Overall layout
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(RadiologyGroup)
        mainLayout.addWidget(LabGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)

class radWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["radImg" + str(numberVal)]
        tempVar2 = myXmlDictionary["radLocDr" + str(numberVal)]
        tempVar3 = myXmlDictionary["radDate" + str(numberVal)]
        tempVar4 = myXmlDictionary["radNotes" + str(numberVal)]

        doctValueLabel.setText(tempVar2)
        radDateValueLabel.setText(tempVar3)
        radNoteValueLabel.setText(tempVar4)
        
        try:
            picDir = theDirectory + "\\imgs\\" + tempVar1
        except TypeError:
            picDir = ""
        radiomap = QPixmap(picDir)
        radiomap = radiomap.scaled(250, 250, Qt.KeepAspectRatio)
        radioImg.setPixmap(radiomap)

class labWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["labImg" + str(numberVal)]
        tempVar2 = myXmlDictionary["labLocDr" + str(numberVal)]
        tempVar3 = myXmlDictionary["labDate" + str(numberVal)]
        tempVar4 = myXmlDictionary["labNotes" + str(numberVal)]

        labLocaValueLabel.setText(tempVar2)
        labDate2ValueLabel.setText(tempVar3)
        note2ValueLabel.setText(tempVar4)

        try:
            picDir = theDirectory + "\\imgs\\" + tempVar1
        except TypeError:
            picDir = ""
        labmap = QPixmap(picDir)
        labmap = labmap.scaled(250, 250, Qt.KeepAspectRatio)
        labImg.setPixmap(labmap)

class CurrentStatusTab(QWidget):
    def __init__(self, parent=None):
        content = ET.parse(tripleVal[1])
        currentStatus=content.find("currentStatus")
        futCounter = 1
        pastCounter = 1

        global futDoctValueLabel
        global vistValueLabel
        global addiValueLabel
        global futDateValueLabel
        global locaValueLabel
        global rechValueLabel
        global recwValueLabel
        global blopValueLabel
        global blplValueLabel
        global noteValueLabel
        global date2ValueLabel
        global heightmap
        global heightImg
        global weightmap
        global weightImg
        global pressmap
        global pressureImg

        QWidget.__init__(self, parent)
        widget = QWidget()

        #FUTURE LIST
        FutureListBox = futureWidget()
        FutureList = []
        FutureListBox.setFixedWidth(350)

        while "futLocDr" + str(futCounter) in myXmlDictionary:
            tempVar = "Future Appointment #" + str(futCounter)
            FutureList.append(tempVar)
            futCounter += 1
        FutureListBox.insertItems(0, FutureList)
        FutureListBox.connect(FutureListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               FutureListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #PAST LIST
        PastListBox = pastWidget()
        PastListBox.setFixedWidth(350)
        PastList = []
        
        while "locDr" + str(pastCounter) in myXmlDictionary:
            tempVar = "Past Appointment #" + str(pastCounter)
            PastList.append(tempVar)
            pastCounter += 1
        PastListBox.insertItems(0, PastList)
        PastListBox.connect(PastListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               PastListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Set up Future Information
        doctLabel = QLabel(self.tr("Location/Doctor:"))
        futDoctValueLabel = QLabel("")
        futDoctValueLabel.setFixedWidth(500)
        futDoctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        vistLabel = QLabel(self.tr("Visitation Purpose:"))
        vistValueLabel = QLabel("")
        vistValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        addiLabel = QLabel(self.tr("Additional Notes:"))
        addiValueLabel = QLabel("")
        addiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        dateLabel = QLabel(self.tr("Scheduled Date:"))
        futDateValueLabel = QLabel("")
        futDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Set up Past Information
        locaLabel = QLabel(self.tr("Location/Doctor:"))
        locaValueLabel = QLabel("")
        locaValueLabel.setFixedWidth(500)
        locaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        rechLabel = QLabel(self.tr("Recorded Height:"))
        rechValueLabel = QLabel("")
        rechValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        recwLabel = QLabel(self.tr("Recorded Weight:"))
        recwValueLabel = QLabel("")
        recwValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        blopLabel = QLabel(self.tr("Blood Pressure:"))
        blopValueLabel = QLabel("")
        blopValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        blplLabel = QLabel(self.tr("Blood Pulse:"))
        blplValueLabel = QLabel("")
        blplValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        noteLabel = QLabel(self.tr("Notes:"))
        noteValueLabel = QLabel("")
        noteValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        date2Label = QLabel(self.tr("Visitation Date:"))
        date2ValueLabel = QLabel("")
        date2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        height = QCheckBox(self.tr("Height"))
        weight = QCheckBox(self.tr("Weight"))
        pressure = QCheckBox(self.tr("Blood Pressure"))

        #Set up Graph Information
        heightmap = QPixmap("")
        heightImg = QLabel()

        heightImg.setPixmap(heightmap)
        heightImg.setFixedWidth(275)
        heightImg.setFixedHeight(275)

        weightmap = QPixmap("")
        weightImg = QLabel()

        weightImg.setPixmap(weightmap)
        weightImg.setFixedWidth(275)
        weightImg.setFixedHeight(275)

        pressmap = QPixmap("")
        pressureImg = QLabel()

        pressureImg.setPixmap(pressmap)
        pressureImg.setFixedWidth(275)
        pressureImg.setFixedHeight(275)

        #Set up picture boxes
        HeightPicBox = QGroupBox(self.tr("Height"))
        WeightPicBox = QGroupBox(self.tr("Weight"))
        PressPicBox = QGroupBox(self.tr("Pressure"))

        HeightPicBoxLayout = QVBoxLayout()
        WeightPicBoxLayout = QVBoxLayout()
        PressPicBoxLayout = QVBoxLayout()
        
        HeightPicBoxLayout.addWidget(heightImg)
        WeightPicBoxLayout.addWidget(weightImg)
        PressPicBoxLayout.addWidget(pressureImg)

        HeightPicBox.setLayout(HeightPicBoxLayout)
        WeightPicBox.setLayout(WeightPicBoxLayout)
        PressPicBox.setLayout(PressPicBoxLayout)

        #Set up Future Boxes
        futureInfo = QGroupBox()
        futureInfoLayout = QVBoxLayout()

        futureInfoLayout.addWidget(doctLabel)
        futureInfoLayout.addWidget(futDoctValueLabel)
        futureInfoLayout.addWidget(dateLabel)
        futureInfoLayout.addWidget(futDateValueLabel)
        futureInfoLayout.addWidget(vistLabel)
        futureInfoLayout.addWidget(vistValueLabel)
        futureInfoLayout.addWidget(addiLabel)
        futureInfoLayout.addWidget(addiValueLabel)
        
        futureInfo.setLayout(futureInfoLayout)

        #Set up Past Boxes
        graphCheck = QGroupBox()
        pastInfo = QGroupBox()

        graphCheckLayout = QVBoxLayout()
        pastInfoLayout = QVBoxLayout()

        graphCheckLayout.addWidget(height)
        graphCheckLayout.addWidget(weight)
        graphCheckLayout.addWidget(pressure)
        
        pastInfoLayout.addWidget(locaLabel)
        pastInfoLayout.addWidget(locaValueLabel)
        pastInfoLayout.addWidget(date2Label)
        pastInfoLayout.addWidget(date2ValueLabel)
        pastInfoLayout.addWidget(rechLabel)
        pastInfoLayout.addWidget(rechValueLabel)
        pastInfoLayout.addWidget(recwLabel)
        pastInfoLayout.addWidget(recwValueLabel)
        pastInfoLayout.addWidget(blopLabel)
        pastInfoLayout.addWidget(blopValueLabel)
        pastInfoLayout.addWidget(blplLabel)
        pastInfoLayout.addWidget(blplValueLabel)
        pastInfoLayout.addWidget(noteLabel)
        pastInfoLayout.addWidget(noteValueLabel)

        graphCheck.setLayout(graphCheckLayout)
        pastInfo.setLayout(pastInfoLayout)

        #Setting up Future Appointment data
        futureApp = QGroupBox(self.tr("Upcoming Appointments"))
        futureAppLayout = QHBoxLayout()

        futureAppLayout.addWidget(FutureListBox)
        futureAppLayout.addWidget(futureInfo)
        
        futureApp.setLayout(futureAppLayout)

        #Setting up Past Appointment data
        pastApp = QGroupBox(self.tr("Past Appointments"))
        pastAppLayout = QHBoxLayout()

        pastAppLayout.addWidget(PastListBox)
        pastAppLayout.addWidget(pastInfo)
        #pastAppLayout.addWidget(graphCheck)

        pastApp.setLayout(pastAppLayout)

        #Setting up Graphs of Checkups
        appGraph = QGroupBox(self.tr("Graphical Display"))
        appGraphLayout = QHBoxLayout()

        appGraphLayout.addWidget(HeightPicBox)
        appGraphLayout.addWidget(WeightPicBox)
        appGraphLayout.addWidget(PressPicBox)

        appGraph.setLayout(appGraphLayout)

        #Putting three layers on page
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(futureApp)
        mainLayout.addWidget(pastApp)
        mainLayout.addWidget(appGraph)
        mainLayout.addStretch(1)
        
        widget.setLayout(mainLayout)

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)

class futureWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["futLocDr" + str(numberVal)]
        tempVar2 = myXmlDictionary["futDate" + str(numberVal)]
        tempVar3 = myXmlDictionary["futPurpose" + str(numberVal)]
        tempVar4 = myXmlDictionary["futNotes" + str(numberVal)]

        futDoctValueLabel.setText(tempVar1)
        vistValueLabel.setText(tempVar2)
        addiValueLabel.setText(tempVar3)
        futDateValueLabel.setText(tempVar4)

class pastWidget(QListWidget):
    def doubleClickedSlot(self,item):
        numberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["locDr" + str(numberVal)]
        tempVar2 = myXmlDictionary["date" + str(numberVal)]
        tempVar3 = myXmlDictionary["height" + str(numberVal)]
        tempVar4 = myXmlDictionary["weight" + str(numberVal)]
        tempVar5 = myXmlDictionary["bloodPressure" + str(numberVal)]
        tempVar6 = myXmlDictionary["pulse" + str(numberVal)]
        tempVar7 = myXmlDictionary["notes" + str(numberVal)]
        tempVar8 = myXmlDictionary["heightImg" + str(numberVal)]
        tempVar9 = myXmlDictionary["weightImg" + str(numberVal)]
        tempVar10 = myXmlDictionary["pressureImg" + str(numberVal)]

        locaValueLabel.setText(tempVar1)
        rechValueLabel.setText(tempVar2)
        recwValueLabel.setText(tempVar3)
        blopValueLabel.setText(tempVar4)
        blplValueLabel.setText(tempVar5)
        noteValueLabel.setText(tempVar6)
        date2ValueLabel.setText(tempVar7)

        try:
            heightDir = theDirectory + "\\imgs\\" + tempVar8
        except TypeError:
            heightDir = ""
        try:
            weightDir = theDirectory + "\\imgs\\" + tempVar9
        except TypeError:
            weightDir = ""
        try:
            pressDir = theDirectory + "\\imgs\\" + tempVar10
        except TypeError:
            pressDir = ""

        heightmap = QPixmap(heightDir)
        weightmap = QPixmap(weightDir)
        pressmap = QPixmap(pressDir)

        heightmap = heightmap.scaled(300, 300, Qt.KeepAspectRatio)
        weightmap = weightmap.scaled(300, 300, Qt.KeepAspectRatio)
        pressmap = pressmap.scaled(300, 300, Qt.KeepAspectRatio)

        heightImg.setPixmap(heightmap)
        weightImg.setPixmap(weightmap)
        pressureImg.setPixmap(pressmap)

def myExitHandler():
    finalDirect = tripleVal[1][:len(str(tripleVal[1]))-13]
    closeOut(finalDirect)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.aboutToQuit.connect(myExitHandler)
    window = loginTab()
    window.setWindowTitle("FlashEHR")
    window.show()
    sys.exit(app.exec_())
    #WHEN FILE CLOSED: closeOut(directory,XML (index 1 for triple from first call), integer (index 2 of triple))