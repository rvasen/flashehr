import sys
from os import listdir
from os.path import join
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtHelp import *
from PySide.QtNetwork import *
from PySide import QtXml
from xml.etree import ElementTree as ET
from encryptDB.doctor.logInDoctor import login
from encryptDB.doctor.authentication import authenticate
from encryptDB.doctor.newUserDoctor import createUser
from encryptDB.doctor.synchronization import synchronize
from shutil import copy

myXmlDictionary = {}
myDuplicateXmlDictionary = {}
#patientDirectory = "C:\\FlashEHR\\patients\\Doe, John\\patient.xml"

diagNumberVal = 0
mediNumberVal = 0
immNumberVal = 0
alleNumberVal = 0
radNumberVal = 0
labNumberVal = 0
futNumberVal = 0
pastNumberVal = 0

#labDirectory[0] = ""
#heightDirectory[0] = ""
#weightDirectory[0] = ""
#pressDirectory[0] = ""

def writeOut(myXmlDictionary):
    if (xmlFile.open(QIODevice.WriteOnly) == False):
        print("Error opening file")    
    else:
        xmlWriter.writeStartDocument()
        xmlWriter.writeCharacters(makeTab(0))
        xmlWriter.writeStartElement("patient")

        #START MAIN INFO RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("mainInfo")

        #WHO
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("who")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("lastname")
        xmlWriter.writeCharacters(myXmlDictionary["lastname"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("firstname")
        xmlWriter.writeCharacters(myXmlDictionary["firstname"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("ssn")
        xmlWriter.writeCharacters(myXmlDictionary["ssn"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("marital")
        xmlWriter.writeCharacters(myXmlDictionary["marital"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("DOB")
        xmlWriter.writeCharacters(myXmlDictionary["DOB"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("sex")
        xmlWriter.writeCharacters(myXmlDictionary["sex"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("pic")
        xmlWriter.writeCharacters(myXmlDictionary["pic"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #CONTACT
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("contact")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("address")
        xmlWriter.writeCharacters(myXmlDictionary["address"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("state")
        xmlWriter.writeCharacters(myXmlDictionary["state"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("country")
        xmlWriter.writeCharacters(myXmlDictionary["country"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("email")
        xmlWriter.writeCharacters(myXmlDictionary["email"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("city")
        xmlWriter.writeCharacters(myXmlDictionary["city"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("postalCode")
        xmlWriter.writeCharacters(myXmlDictionary["postalCode"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("emergencyPhone")
        xmlWriter.writeCharacters(myXmlDictionary["emergencyPhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("emergencyContact")
        xmlWriter.writeCharacters(myXmlDictionary["emergencyContact"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("workPhone")
        xmlWriter.writeCharacters(myXmlDictionary["workPhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("homePhone")
        xmlWriter.writeCharacters(myXmlDictionary["homePhone"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("mobilePhone")
        xmlWriter.writeCharacters(myXmlDictionary["mobilePhone"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #EMPLOYER
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("employer")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("occupation")
        xmlWriter.writeCharacters(myXmlDictionary["occupation"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerName")
        xmlWriter.writeCharacters(myXmlDictionary["employerName"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerAddress")
        xmlWriter.writeCharacters(myXmlDictionary["employerAddress"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerState")
        xmlWriter.writeCharacters(myXmlDictionary["employerState"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerCountry")
        xmlWriter.writeCharacters(myXmlDictionary["employerCountry"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerCity")
        xmlWriter.writeCharacters(myXmlDictionary["employerCity"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("employerPost")
        xmlWriter.writeCharacters(myXmlDictionary["employerPost"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #STATS
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("stats")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("language")
        xmlWriter.writeCharacters(myXmlDictionary["language"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("race")
        xmlWriter.writeCharacters(myXmlDictionary["race"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("familySize")
        xmlWriter.writeCharacters(myXmlDictionary["familySize"])
        xmlWriter.writeEndElement()
         
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("homeless")
        xmlWriter.writeCharacters(myXmlDictionary["homeless"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("income")
        xmlWriter.writeCharacters(myXmlDictionary["income"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("interpreter")
        xmlWriter.writeCharacters(myXmlDictionary["interpreter"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        #INSURANCE
        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("insurance")
        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("provider")
        xmlWriter.writeCharacters(myXmlDictionary["provider"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("plan")
        xmlWriter.writeCharacters(myXmlDictionary["plan"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("date")
        xmlWriter.writeCharacters(myXmlDictionary["date"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("policyNumber")
        xmlWriter.writeCharacters(myXmlDictionary["policyNumber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("groupNumber")
        xmlWriter.writeCharacters(myXmlDictionary["groupNumber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subEmployer")
        xmlWriter.writeCharacters(myXmlDictionary["subEmployer"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subscriber")
        xmlWriter.writeCharacters(myXmlDictionary["subscriber"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("relationship")
        xmlWriter.writeCharacters(myXmlDictionary["relationship"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subDOB")
        xmlWriter.writeCharacters(myXmlDictionary["subDOB"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subSsn")
        xmlWriter.writeCharacters(myXmlDictionary["subSsn"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subSex")
        xmlWriter.writeCharacters(myXmlDictionary["subSex"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subAddress")
        xmlWriter.writeCharacters(myXmlDictionary["subAddress"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subCity")
        xmlWriter.writeCharacters(myXmlDictionary["subCity"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subState")
        xmlWriter.writeCharacters(myXmlDictionary["subState"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subZip")
        xmlWriter.writeCharacters(myXmlDictionary["subZip"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subCountry")
        xmlWriter.writeCharacters(myXmlDictionary["subCountry"])
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(3))
        xmlWriter.writeStartElement("subPhone")
        xmlWriter.writeCharacters(myXmlDictionary["subPhone"])
        xmlWriter.writeEndElement()
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START MEDICAL RECORD RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("medRecords")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("diagnosis")
        tempVar = 1
        while "diagID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("diagID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["diagID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "doctor" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("doctor" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["doctor" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "details" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("details" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["details" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "diagDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("diagDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["diagDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("medication")
        tempVar = 1
        while "mediID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mediID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mediID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "dosage" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("dosage" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["dosage" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "prescriber" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("prescriber" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["prescriber" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "mediDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mediDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mediDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("immunization")
        tempVar = 1
        while "immID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("immID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["immID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "immDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("immDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["immDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "location" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("location" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["location" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("allergy")
        tempVar = 1
        while "allID" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("allID" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["allID" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "severity" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("severity" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["severity" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "mitigation" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("mitigation" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["mitigation" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START TEST RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("testResults")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("radiology")
        tempVar = 1
        while "radImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "radNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("radNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["radNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("labtest")
        tempVar = 1
        while "labImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "labNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("labNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["labNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #START CURRENT STATUS RESULTS
        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeStartElement("currentStatus")

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("upcomingAppt")
        tempVar = 1
        while "futLocDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futLocDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futLocDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futDate" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futDate" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futDate" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futPurpose" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futPurpose" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futPurpose" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "futNotes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("futNotes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["futNotes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeStartElement("pastAppt")
        tempVar = 1
        while "locDr" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("locDr" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["locDr" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "date" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("date" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["date" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "height" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("height" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["height" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "weight" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("weight" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["weight" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "bloodPressure" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("bloodPressure" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["bloodPressure" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "pulse" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("pulse" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["pulse" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "notes" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("notes" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["notes" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()

        xmlWriter.writeCharacters(makeTab(2))
        xmlWriter.writeStartElement("graphicalDisplay")
        tempVar = 1
        while "heightImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("heightImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["heightImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "weightImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("weightImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["weightImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1

        tempVar = 1
        while "pressureImg" + str(tempVar) in myXmlDictionary:
            xmlWriter.writeCharacters(makeTab(3))
            xmlWriter.writeStartElement("pressureImg" + str(tempVar))
            xmlWriter.writeCharacters(myXmlDictionary["pressureImg" + str(tempVar)])
            xmlWriter.writeEndElement()
            tempVar += 1
        xmlWriter.writeCharacters(makeTab(2)) 
        xmlWriter.writeEndElement()  

        xmlWriter.writeCharacters(makeTab(1))
        xmlWriter.writeEndElement()

        #END DOCUMENT
        xmlWriter.writeCharacters(makeTab(0))
        xmlWriter.writeEndElement()
        xmlWriter.writeEndDocument()
        xmlFile.close()

def makeTab(indCount):
    space = ""
    while indCount > 0:
        indCount -= 1
        space = space + "    "
    newLine = "\n" + space
    return newLine

def parseXML():
    global content
    global mainInfo
    global medRecords
    global testResults
    global currentStatus

    content = ET.parse(xmlDir)
    mainInfo=content.find("mainInfo")
    medRecords=content.find("medRecords")
    testResults=content.find("testResults")
    currentStatus=content.find("currentStatus")

    #Set static values as global
    global firstNameVal
    global lastNameVal
    global ssnVal
    global marVal
    global dobVal
    global sexVal
    global picVal

    global addVal
    global statVal
    global countVal
    global emailVal
    global cityVal
    global postVal
    global emPhoneVal
    global emContVal
    global workPhoneVal
    global homePhoneVal
    global mobPhoneVal

    global occVal
    global empNameVal
    global empAddVal
    global empStatVal
    global empCountVal
    global empCityVal
    global empPostVal

    global langVal
    global raceVal
    global famSizeVal
    global homeVal
    global incVal
    global interVal

    global primVal
    global planVal
    global effDateVal
    global policyVal
    global groupVal
    global subsEmpVal
    global subsVal
    global relateVal
    global insDobVal
    global insSsnVal
    global insSexVal
    global insAddVal
    global insCityVal
    global insStateVal
    global insZipVal
    global insCountVal
    global insPhoneVal

    #mainInfo
    lastNameVal = mainInfo.find('who/lastname').text
    firstNameVal = mainInfo.find('who/firstname').text
    ssnVal = mainInfo.find('who/ssn').text
    marVal = mainInfo.find('who/marital').text
    dobVal = mainInfo.find('who/DOB').text
    sexVal = mainInfo.find('who/sex').text
    picVal = mainInfo.find('who/pic').text

    #contact
    addVal = mainInfo.find('contact/address').text
    statVal = mainInfo.find('contact/state').text
    countVal = mainInfo.find('contact/country').text
    emailVal = mainInfo.find('contact/email').text
    cityVal = mainInfo.find('contact/city').text
    postVal = mainInfo.find('contact/postalCode').text
    emPhoneVal = mainInfo.find('contact/emergencyPhone').text
    emContVal = mainInfo.find('contact/emergencyContact').text
    workPhoneVal = mainInfo.find('contact/workPhone').text
    homePhoneVal = mainInfo.find('contact/homePhone').text
    mobPhoneVal = mainInfo.find('contact/mobilePhone').text

    #employer
    occVal = mainInfo.find('employer/occupation').text
    empNameVal = mainInfo.find('employer/employerName').text
    empAddVal = mainInfo.find('employer/employerAddress').text
    empStatVal = mainInfo.find('employer/employerState').text
    empCountVal = mainInfo.find('employer/employerCountry').text
    empCityVal = mainInfo.find('employer/employerCity').text
    empPostVal = mainInfo.find('employer/employerPost').text

    #stats
    langVal = mainInfo.find('stats/language').text
    raceVal = mainInfo.find('stats/race').text
    famSizeVal = mainInfo.find('stats/familySize').text
    homeVal = mainInfo.find('stats/homeless').text
    incVal = mainInfo.find('stats/income').text
    interVal = mainInfo.find('stats/interpreter').text

    #insurance
    primVal = mainInfo.find('insurance/provider').text
    planVal = mainInfo.find('insurance/plan').text
    effDateVal = mainInfo.find('insurance/date').text
    policyVal = mainInfo.find('insurance/policyNumber').text
    groupVal = mainInfo.find('insurance/groupNumber').text
    subsEmpVal = mainInfo.find('insurance/subEmployer').text
    subsVal = mainInfo.find('insurance/subscriber').text
    relateVal = mainInfo.find('insurance/relationship').text
    insDobVal = mainInfo.find('insurance/subDOB').text
    insSsnVal = mainInfo.find('insurance/subSsn').text
    insSexVal = mainInfo.find('insurance/subSex').text
    insAddVal = mainInfo.find('insurance/subAddress').text
    insCityVal = mainInfo.find('insurance/subCity').text
    insStateVal = mainInfo.find('insurance/subState').text
    insZipVal = mainInfo.find('insurance/subZip').text
    insCountVal = mainInfo.find('insurance/subCountry').text
    insPhoneVal = mainInfo.find('insurance/subPhone').text

    #medRecords
    global diagCounterSet
    global mediCounterSet
    global immCounterSet 
    global allCounterSet 
    global radCounterSet
    global labCounterSet
    global futCounterSet
    global pastCounterSet
    global graphCounterSet

    diagCounterSet = 1
    mediCounterSet = 1
    immCounterSet = 1
    allCounterSet = 1
    radCounterSet = 1
    labCounterSet = 1
    futCounterSet = 1
    pastCounterSet = 1
    graphCounterSet = 1

    while str(type(medRecords.find("diagnosis/diagID" + str(diagCounterSet)))) != "<class 'NoneType'>": 
        global diagName
        global diagDocName
        global diagDetName
        global diagDateName        

        diagName = "diagnosis/diagID" + str(diagCounterSet)
        diagDocName = "diagnosis/doctor" + str(diagCounterSet)
        diagDetName = "diagnosis/details" + str(diagCounterSet)
        diagDateName = "diagnosis/diagDate" + str(diagCounterSet)

        myXmlDictionary["diagID" + str(diagCounterSet)] = medRecords.find(diagName).text
        myXmlDictionary["doctor" + str(diagCounterSet)] = medRecords.find(diagDocName).text
        myXmlDictionary["details" + str(diagCounterSet)] = medRecords.find(diagDetName).text
        myXmlDictionary["diagDate" + str(diagCounterSet)] = medRecords.find(diagDateName).text

        diagCounterSet += 1

    while str(type(medRecords.find("medication/mediID" + str(mediCounterSet)))) != "<class 'NoneType'>": 
        global mediIDValName
        global mediDosValName
        global mediPresValName
        global mediDateValName         

        mediIDValName = "medication/mediID" + str(mediCounterSet)
        mediDosValName = "medication/dosage" + str(mediCounterSet)
        mediPresValName = "medication/prescriber" + str(mediCounterSet)
        mediDateValName = "medication/mediDate" + str(mediCounterSet)       

        myXmlDictionary["mediID" + str(mediCounterSet)] = medRecords.find(mediIDValName).text
        myXmlDictionary["dosage" + str(mediCounterSet)] = medRecords.find(mediDosValName).text
        myXmlDictionary["prescriber" + str(mediCounterSet)] = medRecords.find(mediPresValName).text
        myXmlDictionary["mediDate" + str(mediCounterSet)] = medRecords.find(mediDateValName).text

        mediCounterSet += 1
    
    while str(type(medRecords.find("immunization/immID" + str(immCounterSet)))) != "<class 'NoneType'>": 
        global immIDValName
        global immDateValName
        global immLocValName        

        immIDValName = "immunization/immID" + str(immCounterSet)
        immDateValName = "immunization/immDate" + str(immCounterSet)
        immLocValName = "immunization/location" + str(immCounterSet)

        myXmlDictionary["immID" + str(immCounterSet)] = medRecords.find(immIDValName).text
        myXmlDictionary["immDate" + str(immCounterSet)] = medRecords.find(immDateValName).text
        myXmlDictionary["location" + str(immCounterSet)] = medRecords.find(immLocValName).text

        immCounterSet += 1

    while str(type(medRecords.find("allergy/allID" + str(allCounterSet)))) != "<class 'NoneType'>": 
        global allIDValName
        global allSevValName
        global allMitiValName        

        allIDValName = "allergy/allID" + str(allCounterSet)
        allSevValName = "allergy/severity" + str(allCounterSet)
        allMitiValName = "allergy/mitigation" + str(allCounterSet)
        
        myXmlDictionary["allID" + str(allCounterSet)] = medRecords.find(allIDValName).text
        myXmlDictionary["severity" + str(allCounterSet)] = medRecords.find(allSevValName).text
        myXmlDictionary["mitigation" + str(allCounterSet)] = medRecords.find(allMitiValName).text

        allCounterSet += 1

    #testResults
    radiology = testResults.find('radiology')
    
    while str(type(testResults.find("radiology/radLocDr" + str(radCounterSet)))) != "<class 'NoneType'>": 
        global radImgValName
        global radLocValName
        global radDateValName
        global radNoteValName        

        radImgValName = "radiology/radImg" + str(radCounterSet)
        radLocValName = "radiology/radLocDr" + str(radCounterSet)
        radDateValName = "radiology/radDate" + str(radCounterSet)
        radNoteValName = "radiology/radNotes" + str(radCounterSet)

        myXmlDictionary["radImg" + str(radCounterSet)] = testResults.find(radImgValName).text
        myXmlDictionary["radLocDr" + str(radCounterSet)] = testResults.find(radLocValName).text
        myXmlDictionary["radDate" + str(radCounterSet)] = testResults.find(radDateValName).text
        myXmlDictionary["radNotes" + str(radCounterSet)] = testResults.find(radNoteValName).text

        radCounterSet += 1
    labtest = testResults.find('labtest')

    while str(type(testResults.find("labtest/labLocDr" + str(labCounterSet)))) != "<class 'NoneType'>": 
        global labImgValName
        global labLocValName
        global labDateValName
        global labNoteValName        

        labImgValName = "labtest/labImg" + str(labCounterSet)
        labLocValName = "labtest/labLocDr" + str(labCounterSet)
        labDateValName = "labtest/labDate" + str(labCounterSet)
        labNoteValName = "labtest/labNotes" + str(labCounterSet)

        myXmlDictionary["labImg" + str(labCounterSet)] = testResults.find(labImgValName).text
        myXmlDictionary["labLocDr" + str(labCounterSet)] = testResults.find(labLocValName).text
        myXmlDictionary["labDate" + str(labCounterSet)] = testResults.find(labDateValName).text
        myXmlDictionary["labNotes" + str(labCounterSet)] = testResults.find(labNoteValName).text

        labCounterSet += 1

    #currentStatus
    while str(type(currentStatus.find("upcomingAppt/futLocDr" + str(futCounterSet)))) != "<class 'NoneType'>": 
        global futLocValName
        global futDateValName
        global futPurValName
        global futNoteValName        

        futLocValName = "upcomingAppt/futLocDr" + str(futCounterSet)
        futDateValName = "upcomingAppt/futDate" + str(futCounterSet)
        futPurValName = "upcomingAppt/futPurpose" + str(futCounterSet)
        futNoteValName = "upcomingAppt/futNotes" + str(futCounterSet)

        myXmlDictionary["futLocDr" + str(futCounterSet)] = currentStatus.find(futLocValName).text
        myXmlDictionary["futDate" + str(futCounterSet)] = currentStatus.find(futDateValName).text
        myXmlDictionary["futPurpose" + str(futCounterSet)] = currentStatus.find(futPurValName).text
        myXmlDictionary["futNotes" + str(futCounterSet)] = currentStatus.find(futNoteValName).text

        futCounterSet += 1

    while str(type(currentStatus.find("pastAppt/locDr" + str(pastCounterSet)))) != "<class 'NoneType'>": 
        global pastLocValName
        global pastDateValName
        global pastHeightValName
        global pastWeightValName
        global pastPressValName
        global pastPulseValName
        global pastNoteValName        

        pastLocValName = "pastAppt/locDr" + str(pastCounterSet)
        pastDateValName = "pastAppt/date" + str(pastCounterSet)
        pastHeightValName = "pastAppt/height" + str(pastCounterSet)
        pastWeightValName = "pastAppt/weight" + str(pastCounterSet)
        pastPressValName = "pastAppt/bloodPressure" + str(pastCounterSet)
        pastPulseValName = "pastAppt/pulse" + str(pastCounterSet)
        pastNoteValName = "pastAppt/notes" + str(pastCounterSet)

        myXmlDictionary["locDr" + str(pastCounterSet)] = currentStatus.find(pastLocValName).text
        myXmlDictionary["date" + str(pastCounterSet)] = currentStatus.find(pastDateValName).text
        myXmlDictionary["height" + str(pastCounterSet)] = currentStatus.find(pastHeightValName).text
        myXmlDictionary["weight" + str(pastCounterSet)] = currentStatus.find(pastWeightValName).text
        myXmlDictionary["bloodPressure" + str(pastCounterSet)] = currentStatus.find(pastPressValName).text
        myXmlDictionary["pulse" + str(pastCounterSet)] = currentStatus.find(pastPulseValName).text
        myXmlDictionary["notes" + str(pastCounterSet)] = currentStatus.find(pastNoteValName).text

        pastCounterSet += 1

    while str(type(currentStatus.find("graphicalDisplay/heightImg" + str(graphCounterSet)))) != "<class 'NoneType'>": 
        global heightImgValName
        global weightImgValName
        global pressureImgValName        

        heightImgValName = "graphicalDisplay/heightImg" + str(graphCounterSet)
        weightImgValName = "graphicalDisplay/weightImg" + str(graphCounterSet)
        pressureImgValName = "graphicalDisplay/pressureImg" + str(graphCounterSet)
        
        myXmlDictionary["heightImg" + str(graphCounterSet)] = currentStatus.find(heightImgValName).text
        myXmlDictionary["weightImg" + str(graphCounterSet)] = currentStatus.find(weightImgValName).text
        myXmlDictionary["pressureImg" + str(graphCounterSet)] = currentStatus.find(pressureImgValName).text

        graphCounterSet += 1

def writeXML(number, funcType, tagVal, parameters):
    global xmlFile
    global xmlWriter
    global myXmlDictionary

    xmlFile = QFile(xmlDir)
    xmlWriter = QXmlStreamWriter()
    xmlWriter.setDevice(xmlFile)

    myDuplicateXmlDictionary = {} #used to transpose values to the actual dictionary later
    
    writeMain(myDuplicateXmlDictionary)
    writeMed(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)
    writeTest(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)
    writeCurrent(number, funcType, tagVal, parameters, myDuplicateXmlDictionary)

    myXmlDictionary = myDuplicateXmlDictionary
    writeOut(myXmlDictionary)

def writeMain(myDuplicateXmlDictionary):
    #maininfo
    #Who
    myDuplicateXmlDictionary["lastname"] = lastNameEdit.text()
    myDuplicateXmlDictionary["firstname"] = firstNameEdit.text()
    myDuplicateXmlDictionary["ssn"] = ssnValueLabel.text()
    myDuplicateXmlDictionary["marital"] = marValueLabel.text()
    myDuplicateXmlDictionary["DOB"] = dobValueLabel.text()
    myDuplicateXmlDictionary["sex"] = sexValueLabel.text()
    myDuplicateXmlDictionary["pic"] = picVal

    #contact 
    myDuplicateXmlDictionary["address"] = addValueLabel.text()
    myDuplicateXmlDictionary["state"] = statValueLabel.text()
    myDuplicateXmlDictionary["country"] = counValueLabel.text()
    myDuplicateXmlDictionary["email"] = contValueLabel.text()
    myDuplicateXmlDictionary["city"] = cityValueLabel.text()
    myDuplicateXmlDictionary["postalCode"] = postValueLabel.text()
    myDuplicateXmlDictionary["emergencyPhone"] = emeValueLabel.text()
    myDuplicateXmlDictionary["emergencyContact"] = emconValueLabel.text()
    myDuplicateXmlDictionary["workPhone"] = workValueLabel.text()
    myDuplicateXmlDictionary["homePhone"] = homeValueLabel.text()
    myDuplicateXmlDictionary["mobilePhone"] = mobValueLabel.text()
    
    #employer
    myDuplicateXmlDictionary["occupation"] = occValueLabel.text()
    myDuplicateXmlDictionary["employerName"] = emNameValueLabel.text()
    myDuplicateXmlDictionary["employerAddress"] = emAddValueLabel.text()
    myDuplicateXmlDictionary["employerState"] = staValueLabel.text()
    myDuplicateXmlDictionary["employerCountry"] = countValueLabel.text()
    myDuplicateXmlDictionary["employerCity"] = citValueLabel.text()
    myDuplicateXmlDictionary["employerPost"] = postCodValueLabel.text()
    
    #stats
    myDuplicateXmlDictionary["language"] = langValueLabel.text()
    myDuplicateXmlDictionary["race"] = raceValueLabel.text()
    myDuplicateXmlDictionary["familySize"] = famiValueLabel.text()
    myDuplicateXmlDictionary["homeless"] = homlValueLabel.text()
    myDuplicateXmlDictionary["income"] = incoValueLabel.text()
    myDuplicateXmlDictionary["interpreter"] = inteValueLabel.text()

    #insurance
    myDuplicateXmlDictionary["provider"] = primValueLabel.text()
    myDuplicateXmlDictionary["plan"] = planValueLabel.text()
    myDuplicateXmlDictionary["date"] = effeValueLabel.text()
    myDuplicateXmlDictionary["policyNumber"] = plcyValueLabel.text()
    myDuplicateXmlDictionary["groupNumber"] = gropValueLabel.text()
    myDuplicateXmlDictionary["subEmployer"] = subsValueLabel.text()
    myDuplicateXmlDictionary["subscriber"] = scrbValueLabel.text()
    myDuplicateXmlDictionary["relationship"] = relaValueLabel.text()
    myDuplicateXmlDictionary["subDOB"] = dob2ValueLabel.text()
    myDuplicateXmlDictionary["subSsn"] = ssn2ValueLabel.text()
    myDuplicateXmlDictionary["subSex"] = sex2ValueLabel.text()
    myDuplicateXmlDictionary["subAddress"] = sub2ValueLabel.text()
    myDuplicateXmlDictionary["subCity"] = cit2ValueLabel.text()
    myDuplicateXmlDictionary["subState"] = sta2ValueLabel.text()
    myDuplicateXmlDictionary["subZip"] = zip2ValueLabel.text()
    myDuplicateXmlDictionary["subCountry"] = con2ValueLabel.text()
    myDuplicateXmlDictionary["subPhone"] = phonValueLabel.text()

def writeMed(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    #medRecords
    global diagCounterSet
    global mediCounterSet
    global immCounterSet
    global allCounterSet

    if funcType == "add":
        if tagVal == "diag":
            diagCounterSet += 1
        elif tagVal == "medi":
            mediCounterSet += 1
        elif tagVal == "immu":
            immCounterSet += 1
        elif tagVal == "alle":
            allCounterSet += 1
    if funcType == "remove":
        if tagVal == "diag":
            diagCounterSet -= 1
        elif tagVal == "medi":
            mediCounterSet -= 1
        elif tagVal == "immu":
            immCounterSet -= 1
        elif tagVal == "alle":
            allCounterSet -= 1
    #PUT DIAGNOSES LOOP HERE
    tempDiagCounter = 0
    
    if funcType != "remove" or tagVal != "diag":
        while tempDiagCounter < len(DiagnosesStatus):
            diagBool = False
            if funcType == "edit":
                if tempDiagCounter == int(diagNumberVal) - 1:
                    diagBool = True

            tempDiagCounter += 1
            if tagVal == "diag" and (diagBool or (tempDiagCounter == len(DiagnosesStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempDoctorVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["diagID" + str(tempDiagCounter)]
                tempDoctorVar = myXmlDictionary["doctor" + str(tempDiagCounter)] 
                tempDetVar = myXmlDictionary["details" + str(tempDiagCounter)]
                tempDateVar = myXmlDictionary["diagDate" + str(tempDiagCounter)]

            myDuplicateXmlDictionary["diagID" + str(tempDiagCounter)] = tempIDVar
            myDuplicateXmlDictionary["doctor" + str(tempDiagCounter)] = tempDoctorVar
            myDuplicateXmlDictionary["details" + str(tempDiagCounter)] = tempDetVar
            myDuplicateXmlDictionary["diagDate" + str(tempDiagCounter)] = tempDateVar
    else:
        tempDiagCounter = 1
        while tempDiagCounter < int(diagNumberVal):
            myDuplicateXmlDictionary["diagID" + str(tempDiagCounter)] = myXmlDictionary["diagID" + str(tempDiagCounter)]
            myDuplicateXmlDictionary["doctor" + str(tempDiagCounter)] = myXmlDictionary["doctor" + str(tempDiagCounter)]
            myDuplicateXmlDictionary["details" + str(tempDiagCounter)] = myXmlDictionary["details" + str(tempDiagCounter)]
            myDuplicateXmlDictionary["diagDate" + str(tempDiagCounter)] = myXmlDictionary["diagDate" + str(tempDiagCounter)]
            tempDiagCounter += 1
        while tempDiagCounter != diagCounterSet:
            myDuplicateXmlDictionary["diagID" + str(tempDiagCounter)] = myXmlDictionary["diagID" + str(tempDiagCounter + 1)]
            myDuplicateXmlDictionary["doctor" + str(tempDiagCounter)] = myXmlDictionary["doctor" + str(tempDiagCounter + 1)]
            myDuplicateXmlDictionary["details" + str(tempDiagCounter)] = myXmlDictionary["details" + str(tempDiagCounter + 1)]
            myDuplicateXmlDictionary["diagDate" + str(tempDiagCounter)] = myXmlDictionary["diagDate" + str(tempDiagCounter + 1)]
            tempDiagCounter += 1

    #PUT MEDICATION LOOP HERE
    tempMediCounter = 0
    if funcType != "remove" or tagVal != "medi":
        while tempMediCounter < len(MedicationStatus):
            mediBool = False
            if funcType == "edit":
                if tempMediCounter == int(mediNumberVal) - 1:
                    mediBool = True

            tempMediCounter += 1
            if tagVal == "medi" and (mediBool or (tempMediCounter == len(MedicationStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempdosageVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["mediID" + str(tempMediCounter)]
                tempdosageVar = myXmlDictionary["dosage" + str(tempMediCounter)] 
                tempDetVar = myXmlDictionary["prescriber" + str(tempMediCounter)]
                tempDateVar = myXmlDictionary["mediDate" + str(tempMediCounter)]

            myDuplicateXmlDictionary["mediID" + str(tempMediCounter)] = tempIDVar
            myDuplicateXmlDictionary["dosage" + str(tempMediCounter)] = tempdosageVar
            myDuplicateXmlDictionary["prescriber" + str(tempMediCounter)] = tempDetVar
            myDuplicateXmlDictionary["mediDate" + str(tempMediCounter)] = tempDateVar
    else:
        tempMediCounter = 1
        while tempMediCounter < int(mediNumberVal):
            myDuplicateXmlDictionary["mediID" + str(tempMediCounter)] = myXmlDictionary["mediID" + str(tempMediCounter)]
            myDuplicateXmlDictionary["dosage" + str(tempMediCounter)] = myXmlDictionary["dosage" + str(tempMediCounter)]
            myDuplicateXmlDictionary["prescriber" + str(tempMediCounter)] = myXmlDictionary["prescriber" + str(tempMediCounter)]
            myDuplicateXmlDictionary["mediDate" + str(tempMediCounter)] = myXmlDictionary["mediDate" + str(tempMediCounter)]
            tempMediCounter += 1
        while tempMediCounter != mediCounterSet:
            myDuplicateXmlDictionary["mediID" + str(tempMediCounter)] = myXmlDictionary["mediID" + str(tempMediCounter + 1)]
            myDuplicateXmlDictionary["dosage" + str(tempMediCounter)] = myXmlDictionary["dosage" + str(tempMediCounter + 1)]
            myDuplicateXmlDictionary["prescriber" + str(tempMediCounter)] = myXmlDictionary["prescriber" + str(tempMediCounter + 1)]
            myDuplicateXmlDictionary["mediDate" + str(tempMediCounter)] = myXmlDictionary["mediDate" + str(tempMediCounter + 1)]
            tempMediCounter += 1

    #PUT IMMUNIZATION LOOP HERE
    tempImmCounter = 0
    if funcType != "remove" or tagVal != "immu":
        while tempImmCounter < len(ImmunizationStatus):
            immBool = False
            if funcType == "edit":
                if tempImmCounter == int(immNumberVal) - 1:
                    immBool = True

            tempImmCounter += 1
            if tagVal == "immu" and (immBool or (tempImmCounter == len(ImmunizationStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempimmDateVar = parameters[1]
                tempDetVar = parameters[2]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["immID" + str(tempImmCounter)]
                tempimmDateVar = myXmlDictionary["immDate" + str(tempImmCounter)] 
                tempDetVar = myXmlDictionary["location" + str(tempImmCounter)]

            myDuplicateXmlDictionary["immID" + str(tempImmCounter)] = tempIDVar
            myDuplicateXmlDictionary["immDate" + str(tempImmCounter)] = tempimmDateVar
            myDuplicateXmlDictionary["location" + str(tempImmCounter)] = tempDetVar
    else:
        tempImmCounter = 1
        while tempImmCounter < int(immNumberVal):
            myDuplicateXmlDictionary["immID" + str(tempImmCounter)] = myXmlDictionary["immID" + str(tempImmCounter)]
            myDuplicateXmlDictionary["immDate" + str(tempImmCounter)] = myXmlDictionary["immDate" + str(tempImmCounter)]
            myDuplicateXmlDictionary["location" + str(tempImmCounter)] = myXmlDictionary["location" + str(tempImmCounter)]
            tempImmCounter += 1
        while tempImmCounter != immCounterSet:
            myDuplicateXmlDictionary["immID" + str(tempImmCounter)] = myXmlDictionary["immID" + str(tempImmCounter + 1)]
            myDuplicateXmlDictionary["immDate" + str(tempImmCounter)] = myXmlDictionary["immDate" + str(tempImmCounter + 1)]
            myDuplicateXmlDictionary["location" + str(tempImmCounter)] = myXmlDictionary["location" + str(tempImmCounter + 1)]
            tempImmCounter += 1

    #PUT ALLERGY LOOP HERE
    tempAllCounter = 0
    if funcType != "remove" or tagVal != "alle":
        while tempAllCounter < len(AllergiesStatus):
            alleBool = False
            if funcType == "edit":
                if tempAllCounter == int(alleNumberVal) - 1:
                    alleBool = True

            tempAllCounter += 1
            if tagVal == "alle" and (alleBool or (tempAllCounter == len(AllergiesStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempseverityVar = parameters[1]
                tempDetVar = parameters[2]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["allID" + str(tempAllCounter)]
                tempseverityVar = myXmlDictionary["severity" + str(tempAllCounter)] 
                tempDetVar = myXmlDictionary["mitigation" + str(tempAllCounter)]

            myDuplicateXmlDictionary["allID" + str(tempAllCounter)] = tempIDVar
            myDuplicateXmlDictionary["severity" + str(tempAllCounter)] = tempseverityVar
            myDuplicateXmlDictionary["mitigation" + str(tempAllCounter)] = tempDetVar
    else:
        tempAllCounter = 1
        while tempAllCounter < int(alleNumberVal):
            myDuplicateXmlDictionary["allID" + str(tempAllCounter)] = myXmlDictionary["allID" + str(tempAllCounter)]
            myDuplicateXmlDictionary["severity" + str(tempAllCounter)] = myXmlDictionary["severity" + str(tempAllCounter)]
            myDuplicateXmlDictionary["mitigation" + str(tempAllCounter)] = myXmlDictionary["mitigation" + str(tempAllCounter)]
            tempAllCounter += 1
        while tempAllCounter != allCounterSet:
            myDuplicateXmlDictionary["allID" + str(tempAllCounter)] = myXmlDictionary["allID" + str(tempAllCounter + 1)]
            myDuplicateXmlDictionary["severity" + str(tempAllCounter)] = myXmlDictionary["severity" + str(tempAllCounter + 1)]
            myDuplicateXmlDictionary["mitigation" + str(tempAllCounter)] = myXmlDictionary["mitigation" + str(tempAllCounter + 1)]
            tempAllCounter += 1

def writeTest(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    global radCounterSet
    global labCounterSet

    if funcType == "add":
        if tagVal == "rad":
            radCounterSet += 1
        elif tagVal == "lab":
            labCounterSet += 1
    if funcType == "remove":
        if tagVal == "rad":
            radCounterSet -= 1
        elif tagVal == "lab":
            labCounterSet -= 1

    #testResults
    tempRadCounter = 0
    if funcType != "remove" or tagVal != "rad":
        while tempRadCounter < len(RadiologyStatus):
            radBool = False
            if funcType == "edit":
                if tempRadCounter == int(radNumberVal) - 1:
                    radBool = True

            tempRadCounter += 1
            if tagVal == "rad" and (radBool or (tempRadCounter == len(RadiologyStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempradLocDrVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["radImg" + str(tempRadCounter)]
                tempradLocDrVar = myXmlDictionary["radLocDr" + str(tempRadCounter)] 
                tempDetVar = myXmlDictionary["radDate" + str(tempRadCounter)]
                tempDateVar = myXmlDictionary["radNotes" + str(tempRadCounter)]

            myDuplicateXmlDictionary["radImg" + str(tempRadCounter)] = tempIDVar
            myDuplicateXmlDictionary["radLocDr" + str(tempRadCounter)] = tempradLocDrVar
            myDuplicateXmlDictionary["radDate" + str(tempRadCounter)] = tempDetVar
            myDuplicateXmlDictionary["radNotes" + str(tempRadCounter)] = tempDateVar
    else:
        tempRadCounter = 1
        while tempRadCounter < int(radNumberVal):
            myDuplicateXmlDictionary["radImg" + str(tempRadCounter)] = myXmlDictionary["radImg" + str(tempRadCounter)]
            myDuplicateXmlDictionary["radLocDr" + str(tempRadCounter)] = myXmlDictionary["radLocDr" + str(tempRadCounter)]
            myDuplicateXmlDictionary["radDate" + str(tempRadCounter)] = myXmlDictionary["radDate" + str(tempRadCounter)]
            myDuplicateXmlDictionary["radNotes" + str(tempRadCounter)] = myXmlDictionary["radNotes" + str(tempRadCounter)]
            tempRadCounter += 1
        while tempRadCounter != radCounterSet:
            myDuplicateXmlDictionary["radImg" + str(tempRadCounter)] = myXmlDictionary["radImg" + str(tempRadCounter + 1)]
            myDuplicateXmlDictionary["radLocDr" + str(tempRadCounter)] = myXmlDictionary["radLocDr" + str(tempRadCounter + 1)]
            myDuplicateXmlDictionary["radDate" + str(tempRadCounter)] = myXmlDictionary["radDate" + str(tempRadCounter + 1)]
            myDuplicateXmlDictionary["radNotes" + str(tempRadCounter)] = myXmlDictionary["radNotes" + str(tempRadCounter + 1)]
            tempRadCounter += 1

    tempLabCounter = 0
    if funcType != "remove" or tagVal != "lab":
        while tempLabCounter < len(LabStatus):
            labBool = False
            if funcType == "edit":
                if tempLabCounter == int(labNumberVal) - 1:
                    labBool = True

            tempLabCounter += 1
            if tagVal == "lab" and (labBool or (tempLabCounter == len(LabStatus)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                templabLocDrVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["labImg" + str(tempLabCounter)]
                templabLocDrVar = myXmlDictionary["labLocDr" + str(tempLabCounter)] 
                tempDetVar = myXmlDictionary["labDate" + str(tempLabCounter)]
                tempDateVar = myXmlDictionary["labNotes" + str(tempLabCounter)]

            myDuplicateXmlDictionary["labImg" + str(tempLabCounter)] = tempIDVar
            myDuplicateXmlDictionary["labLocDr" + str(tempLabCounter)] = templabLocDrVar
            myDuplicateXmlDictionary["labDate" + str(tempLabCounter)] = tempDetVar
            myDuplicateXmlDictionary["labNotes" + str(tempLabCounter)] = tempDateVar
    else:
        tempLabCounter = 1
        while tempLabCounter < int(labNumberVal):
            myDuplicateXmlDictionary["labImg" + str(tempLabCounter)] = myXmlDictionary["labImg" + str(tempLabCounter)]
            myDuplicateXmlDictionary["labLocDr" + str(tempLabCounter)] = myXmlDictionary["labLocDr" + str(tempLabCounter)]
            myDuplicateXmlDictionary["labDate" + str(tempLabCounter)] = myXmlDictionary["labDate" + str(tempLabCounter)]
            myDuplicateXmlDictionary["labNotes" + str(tempLabCounter)] = myXmlDictionary["labNotes" + str(tempLabCounter)]
            tempLabCounter += 1
        while tempLabCounter != labCounterSet:
            myDuplicateXmlDictionary["labImg" + str(tempLabCounter)] = myXmlDictionary["labImg" + str(tempLabCounter + 1)]
            myDuplicateXmlDictionary["labLocDr" + str(tempLabCounter)] = myXmlDictionary["labLocDr" + str(tempLabCounter + 1)]
            myDuplicateXmlDictionary["labDate" + str(tempLabCounter)] = myXmlDictionary["labDate" + str(tempLabCounter + 1)]
            myDuplicateXmlDictionary["labNotes" + str(tempLabCounter)] = myXmlDictionary["labNotes" + str(tempLabCounter + 1)]
            tempLabCounter += 1

def writeCurrent(number, funcType, tagVal, parameters, myDuplicateXmlDictionary):
    global futCounterSet
    global pastCounterSet

    #currentstatus
    if funcType == "add":
        if tagVal == "fut":
            futCounterSet += 1
        elif tagVal == "past":
            pastCounterSet += 1
    if funcType == "remove":
        if tagVal == "fut":
            futCounterSet -= 1
        elif tagVal == "past":
            pastCounterSet -= 1
            
    tempFutCounter = 0
    if funcType != "remove" or tagVal != "fut":
        while tempFutCounter < len(FutureList):
            futBool = False
            if funcType == "edit":
                if tempFutCounter == int(futNumberVal) - 1:
                    futBool = True

            tempFutCounter += 1
            if tagVal == "fut" and (futBool or (tempFutCounter == len(FutureList)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempdateVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["futLocDr" + str(tempFutCounter)]
                tempdateVar = myXmlDictionary["futDate" + str(tempFutCounter)] 
                tempDetVar = myXmlDictionary["futPurpose" + str(tempFutCounter)]
                tempDateVar = myXmlDictionary["futNotes" + str(tempFutCounter)]

            myDuplicateXmlDictionary["futLocDr" + str(tempFutCounter)] = tempIDVar
            myDuplicateXmlDictionary["futDate" + str(tempFutCounter)] = tempdateVar
            myDuplicateXmlDictionary["futPurpose" + str(tempFutCounter)] = tempDetVar
            myDuplicateXmlDictionary["futNotes" + str(tempFutCounter)] = tempDateVar
    else:
        tempFutCounter = 1
        while tempFutCounter < int(futNumberVal):
            myDuplicateXmlDictionary["futLocDr" + str(tempFutCounter)] = myXmlDictionary["futLocDr" + str(tempFutCounter)]
            myDuplicateXmlDictionary["futDate" + str(tempFutCounter)] = myXmlDictionary["futDate" + str(tempFutCounter)]
            myDuplicateXmlDictionary["futPurpose" + str(tempFutCounter)] = myXmlDictionary["futPurpose" + str(tempFutCounter)]
            myDuplicateXmlDictionary["futNotes" + str(tempFutCounter)] = myXmlDictionary["futNotes" + str(tempFutCounter)]
            tempFutCounter += 1
        while tempFutCounter != futCounterSet:
            myDuplicateXmlDictionary["futLocDr" + str(tempFutCounter)] = myXmlDictionary["futLocDr" + str(tempFutCounter + 1)]
            myDuplicateXmlDictionary["futDate" + str(tempFutCounter)] = myXmlDictionary["futDate" + str(tempFutCounter + 1)]
            myDuplicateXmlDictionary["futPurpose" + str(tempFutCounter)] = myXmlDictionary["futPurpose" + str(tempFutCounter + 1)]
            myDuplicateXmlDictionary["futNotes" + str(tempFutCounter)] = myXmlDictionary["futNotes" + str(tempFutCounter + 1)]
            tempFutCounter += 1

    tempPastCounter = 0
    if funcType != "remove" or tagVal != "past":
        while tempPastCounter < len(PastList):
            pastBool = False
            if funcType == "edit":
                if tempPastCounter == int(pastNumberVal) - 1:
                    pastBool = True

            tempPastCounter += 1
            if tagVal == "past" and (pastBool or (tempPastCounter == len(PastList)  and funcType == "add")):
                #sets variables to parameters if editing/adding is going on
                tempIDVar = parameters[0]
                tempDoctorVar = parameters[1]
                tempDetVar = parameters[2]
                tempDateVar = parameters[3]
                tempIDVar2 = parameters[4]
                tempDoctorVar2 = parameters[5]
                tempDetVar2 = parameters[6]
                tempHimg = parameters[7]
                tempWimg = parameters[8]
                tempPimg = parameters[9]
            else:
                #sets variables to temporaries to be outputted
                tempIDVar = myXmlDictionary["locDr" + str(tempPastCounter)]
                tempDoctorVar = myXmlDictionary["date" + str(tempPastCounter)]
                tempDetVar = myXmlDictionary["height" + str(tempPastCounter)]
                tempDateVar = myXmlDictionary["weight" + str(tempPastCounter)]
                tempIDVar2 = myXmlDictionary["bloodPressure" + str(tempPastCounter)]
                tempDoctorVar2 = myXmlDictionary["pulse" + str(tempPastCounter)]
                tempDetVar2 = myXmlDictionary["notes" + str(tempPastCounter)]
                tempHimg = myXmlDictionary["heightImg" + str(tempPastCounter)]
                tempWimg = myXmlDictionary["weightImg" + str(tempPastCounter)]
                tempPimg = myXmlDictionary["pressureImg" + str(tempPastCounter)]

            myDuplicateXmlDictionary["locDr" + str(tempPastCounter)] = tempIDVar
            myDuplicateXmlDictionary["date" + str(tempPastCounter)] = tempDoctorVar
            myDuplicateXmlDictionary["height" + str(tempPastCounter)] = tempDetVar
            myDuplicateXmlDictionary["weight" + str(tempPastCounter)] = tempDateVar
            myDuplicateXmlDictionary["bloodPressure" + str(tempPastCounter)] = tempIDVar2
            myDuplicateXmlDictionary["pulse" + str(tempPastCounter)] = tempDoctorVar2
            myDuplicateXmlDictionary["notes" + str(tempPastCounter)] = tempDetVar2
            myDuplicateXmlDictionary["heightImg" + str(tempPastCounter)] = tempHimg
            myDuplicateXmlDictionary["weightImg" + str(tempPastCounter)] = tempWimg
            myDuplicateXmlDictionary["pressureImg" + str(tempPastCounter)] = tempPimg
    else:
        tempPastCounter = 1
        while tempPastCounter < int(pastNumberVal):
            myDuplicateXmlDictionary["locDr" + str(tempPastCounter)] = myXmlDictionary["locDr" + str(tempPastCounter)]
            myDuplicateXmlDictionary["date" + str(tempPastCounter)] = myXmlDictionary["date" + str(tempPastCounter)]
            myDuplicateXmlDictionary["height" + str(tempPastCounter)] = myXmlDictionary["height" + str(tempPastCounter)]
            myDuplicateXmlDictionary["weight" + str(tempPastCounter)] = myXmlDictionary["weight" + str(tempPastCounter)]
            myDuplicateXmlDictionary["bloodPressure" + str(tempPastCounter)] = myXmlDictionary["bloodPressure" + str(tempPastCounter)]
            myDuplicateXmlDictionary["pulse" + str(tempPastCounter)] = myXmlDictionary["pulse" + str(tempPastCounter)]
            myDuplicateXmlDictionary["notes" + str(tempPastCounter)] = myXmlDictionary["notes" + str(tempPastCounter)]
            myDuplicateXmlDictionary["heightImg" + str(tempPastCounter)] = myXmlDictionary["heightImg" + str(tempPastCounter)]
            myDuplicateXmlDictionary["weightImg" + str(tempPastCounter)] = myXmlDictionary["weightImg" + str(tempPastCounter)]
            myDuplicateXmlDictionary["pressureImg" + str(tempPastCounter)] = myXmlDictionary["pressureImg" + str(tempPastCounter)]
            tempPastCounter += 1
        while tempPastCounter != pastCounterSet:
            myDuplicateXmlDictionary["locDr" + str(tempPastCounter)] = myXmlDictionary["locDr" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["date" + str(tempPastCounter)] = myXmlDictionary["date" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["height" + str(tempPastCounter)] = myXmlDictionary["height" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["weight" + str(tempPastCounter)] = myXmlDictionary["weight" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["bloodPressure" + str(tempPastCounter)] = myXmlDictionary["bloodPressure" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["pulse" + str(tempPastCounter)] = myXmlDictionary["pulse" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["notes" + str(tempPastCounter)] = myXmlDictionary["notes" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["heightImg" + str(tempPastCounter)] = myXmlDictionary["heightImg" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["weightImg" + str(tempPastCounter)] = myXmlDictionary["weightImg" + str(tempPastCounter + 1)]
            myDuplicateXmlDictionary["pressureImg" + str(tempPastCounter)] = myXmlDictionary["pressureImg" + str(tempPastCounter + 1)]
            tempPastCounter += 1

class loginTab(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        temp = QWidget()

        mainLayout = QVBoxLayout()
        temp.setLayout(mainLayout)
        self.setGeometry(1,1,0,0)

        self.login1()

    def setExistingDirectory(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal.setText(str(directory))

    def setExistingDirectory2(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal2.setText(str(directory))

    def setExistingDirectory3(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal3.setText(str(directory))

    def setExistingDirectory4(self):
        directory = QFileDialog.getExistingDirectory(self,self.tr("Directory"),"",QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly)
        userLabelVal4.setText(str(directory))

    def checkLogin1(self):
        global boolLogin
        boolLogin = login(userLabelVal.text(), passBox1.text())
        
        if(boolLogin == True):
            self.choosePat()
            self.close()
        else:
            self.popUpLog1()
            #self.close()

    def checkLogin2(self):
        global boolLogin
        boolLogin = login(userLabelVal3.text(), passBox2.text())
        
        if(boolLogin == True):
            self.choosePat()
            self.close()
        else:
            self.popUpLog2()

    def createUserFinal(self):
        global boolCreate        
        boolCreate = createUser(userLabelVal2.text(), pass2Box.text())

        if(boolCreate == True):
            self.login2()
            self.window.close
        else:
            self.popUpNew()

    def popUpLog1(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.login1)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)
        
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def popUpLog2(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.login2)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)
        
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def popUpNew(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.newUser)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def popUpAuth(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(300)
        self.window.setFixedHeight(150)       
        self.window.show()

        infoPop = QLabel(self.tr("Incorrect login credentials"))
        retryEntry = QLabel(self.tr("Please retry login"))
        finishButton = QPushButton("Close Window")
        finishButton.clicked.connect(self.choosePat)
        finishButton.clicked.connect(self.window.close)
        finishButton.setFixedWidth(275)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(infoPop)
        mainLayout.addWidget(retryEntry)
        mainLayout.addWidget(finishButton)

        self.window.setLayout(mainLayout)

    def login1(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(450)
        self.window.setFixedHeight(250)          
        self.window.show()
        #login(userName, password) --> returns true/false (new page/dialogue popup)

        #Sets up boxes of information
        userLabel = QLabel(self.tr("User Name:"))
        #self.userLabel = QPushButton(self.tr("Select USB Directory"))
        #self.connect(self.userLabel, SIGNAL("clicked()"), self.setExistingDirectory)

        #directoryVal = QLabel(self.tr("Selected Directory:"))
        global userLabelVal
        userLabelVal = QLineEdit()
        
        global passBox1
        passLabel = QLabel(self.tr("Password:"))
        passBox1 = QLineEdit(self)

        submitButton = QPushButton(self.tr("Login"))
        userButton = QPushButton(self.tr("Create New User"))

        submitButton.clicked.connect(self.checkLogin1)

        userButton.clicked.connect(self.newUser)
        userButton.clicked.connect(self.close)

        #Sets up two boxes of information
        loginGroup = QGroupBox(self.tr("Login:"))
        submitGroup = QGroupBox(self.tr("Submit:"))

        loginGroup.setFixedWidth(425)
        submitGroup.setFixedWidth(425)

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QHBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        #loginGroupLayout.addWidget(self.userLabel)
        #loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal)
        loginGroupLayout.addWidget(passLabel)
        loginGroupLayout.addWidget(passBox1)

        submitGroupLayout.addWidget(submitButton)
        submitGroupLayout.addWidget(userButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Overall display
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def newUser(self):
        #createUser(userName, password) --> returns true/false (new page/dialogue popup

        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(475)
        self.window.setFixedHeight(275)          
        self.window.show()

        #Information setup
        userLabel = QLabel(self.tr("Username:"))
        #self.createUserLabel = QPushButton(self.tr("Select USB Directory"))
        #self.connect(self.createUserLabel, SIGNAL("clicked()"), self.setExistingDirectory2)

        #directoryVal2 = QLabel(self.tr("Selected Directory:"))
        global userLabelVal2        
        userLabelVal2 = QLineEdit()

        pass1Label = QLabel(self.tr("Enter Password:"))
        pass1Box = QLineEdit(self)

        global pass2Box
        pass2Label = QLabel(self.tr("Please Re-enter Password:"))
        pass2Box = QLineEdit(self)

        submitButton = QPushButton(self.tr("Submit"))

        submitButton.clicked.connect(self.createUserFinal)

        #Sets up each of the two boxes
        loginGroup = QGroupBox(self.tr("Create Account Information"))
        loginGroup.setFixedWidth(450)

        submitGroup = QGroupBox()
        submitGroup.setFixedWidth(450)

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QVBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        #loginGroupLayout.addWidget(self.createUserLabel)
        #loginGroupLayout.addWidget(directoryVal2)
        loginGroupLayout.addWidget(userLabelVal2)
        loginGroupLayout.addWidget(pass1Label)
        loginGroupLayout.addWidget(pass1Box)
        loginGroupLayout.addWidget(pass2Label)
        loginGroupLayout.addWidget(pass2Box)

        submitGroupLayout.addWidget(submitButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Final setup of page
        mainLayout = QVBoxLayout(self.window)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def login2(self):
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(450)
        self.window.setFixedHeight(250)          
        self.window.show()

        #Sets up boxes of information
        userLabel = QLabel(self.tr("User Name:"))
        #self.userLabel2 = QPushButton(self.tr("Select USB Directory"))
        #self.connect(self.userLabel2, SIGNAL("clicked()"), self.setExistingDirectory3)

        #directoryVal = QLabel(self.tr("Selected Directory:"))
        global userLabelVal3
        userLabelVal3 = QLineEdit()

        global passBox2
        passLabel = QLabel(self.tr("Password:"))
        passBox2 = QLineEdit(self)

        submitButton = QPushButton(self.tr("Login"))
        userButton = QPushButton(self.tr("Create New User"))

        submitButton.clicked.connect(self.checkLogin2)

        userButton.clicked.connect(self.newUser)
        userButton.clicked.connect(self.close)

        #Sets up two boxes of information
        loginGroup = QGroupBox(self.tr("Login:"))
        submitGroup = QGroupBox(self.tr("Submit:"))

        loginGroup.setFixedWidth(425)
        submitGroup.setFixedWidth(425)

        loginGroupLayout = QVBoxLayout()
        submitGroupLayout = QHBoxLayout()

        loginGroupLayout.addWidget(userLabel)
        #loginGroupLayout.addWidget(self.userLabel2)
        #loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal3)
        loginGroupLayout.addWidget(passLabel)
        loginGroupLayout.addWidget(passBox2)

        submitGroupLayout.addWidget(submitButton)
        submitGroupLayout.addWidget(userButton)

        loginGroup.setLayout(loginGroupLayout)
        submitGroup.setLayout(submitGroupLayout)

        #Overall display
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(loginGroup)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def authButton(self):
        #Determines whether authenticated for patient
        global boolAuth
        boolAuth = authenticate(userLabelVal4.text(), userBox.text())

        if(boolAuth == True):
            self.choosePat()
            userLabelVal4.setText("")
            userBox.setText("")
        else:
            self.popUpAuth()

    def synchronize(self):
        if(userLabelVal4.text() != ""):
            synchronize(userLabelVal4.text())
            userLabelVal4.setText("")
            userBox.setText("")

    def xmlDirAssign(self, item):
        joinedDir = join("C:\\FlashEHR\\patients", item.text())
        global xmlDir
        xmlDir = join(joinedDir, "patient.xml")

    def choosePat(self):
        #authenticate(directory, password) --> Returns true if can login (add name to list), false: Dialogue for failed
        #synchronize(directory) --> happens when "synchronize button" pressed: nothing returned
        self.window = QWidget()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedWidth(625)
        self.window.setFixedHeight(325) 
        self.window.show()

        #PATIENT LIST
        PatientListBox = QListWidget()
        PatientListBox.insertItems(0, listdir("C:\\FlashEHR\\patients"))
        if(len(listdir("C:\\FlashEHR\\patients"))>=1):
            PatientListBox.setCurrentRow(0)
            joinedDir = join("C:\\FlashEHR\\patients", PatientListBox.item(0).text()) 
            global xmlDir            
            xmlDir = join(joinedDir, "patient.xml")
        PatientListBox.itemClicked.connect(self.xmlDirAssign)

        #Information setup
        global userBox
        userLabel = QLabel(self.tr("Authentication Password:"))
        userBox = QLineEdit()

        pass1Label = QLabel(self.tr("Enter Patient Directory:"))
        self.authenticate = QPushButton(self.tr("Select USB Directory"))
        self.connect(self.authenticate, SIGNAL("clicked()"), self.setExistingDirectory4)

        submitButton = QPushButton(self.tr("Submit"))
        if(len(listdir("C:\\FlashEHR\\patients"))>=1):
            submitButton.clicked.connect(self.TabDialog)
            submitButton.clicked.connect(self.close)

        global userLabelVal4
        directoryVal = QLabel(self.tr("Selected Directory:"))
        userLabelVal4 = QLabel("")
        userLabelVal4.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        authButton = QPushButton(self.tr("Authenticate"))
        authButton.clicked.connect(self.authButton)

        syncButton = QPushButton(self.tr("Synchronize"))
        syncButton.clicked.connect(self.synchronize)

        buttonMap = QGroupBox()
        buttonLayout = QVBoxLayout()
        buttonLayout.addWidget(authButton)
        buttonLayout.addWidget(syncButton)
        buttonMap.setLayout(buttonLayout)
        buttonMap.setFixedHeight(75)

        #Sets up each of the two boxes
        loginGroup = QGroupBox(self.tr("Authenticate/Synchronize"))
        submitGroup = QGroupBox()

        loginGroupLayout = QVBoxLayout()
        loginGroupLayout.addWidget(userLabel)
        loginGroupLayout.addWidget(userBox)
        loginGroupLayout.addWidget(pass1Label)
        loginGroupLayout.addWidget(self.authenticate)
        loginGroupLayout.addWidget(directoryVal)
        loginGroupLayout.addWidget(userLabelVal4)

        loginGroup.setLayout(loginGroupLayout)
        
        submitGroupLayout = QVBoxLayout()
        submitGroupLayout.addWidget(loginGroup)
        submitGroupLayout.addWidget(buttonMap)

        submitGroup.setLayout(submitGroupLayout)
        submitGroup.setFixedWidth(300)
        submitGroup.setFixedHeight(300)

        PatientList = QGroupBox(self.tr("Patient List"))
        PatientListLayout = QVBoxLayout()
        PatientListLayout.addWidget(PatientListBox)
        PatientListLayout.addWidget(submitButton)
        
        PatientList.setLayout(PatientListLayout)
        PatientList.setFixedWidth(300)
        PatientList.setFixedHeight(300)

        #Final setup of page
        mainLayout = QHBoxLayout(self)
        mainLayout.addWidget(PatientList)
        mainLayout.addWidget(submitGroup)
        mainLayout.addStretch(1)

        self.window.setLayout(mainLayout)

    def TabDialog(self):
        self.window = QDialog()
        self.window.setWindowTitle(self.tr("FlashEHR"))
        self.window.setFixedHeight(600)
        self.window.setFixedWidth(1200)
        self.window.show()

        #Sets up the file bar
        exitAction = QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)   

        #Displays tab interface of the program
        tabWidget = QTabWidget()
        tabWidget.addTab(HomeTab(), self.tr("Home"))
        tabWidget.addTab(MedicalRecordsTab(), self.tr("Medical Records"))
        tabWidget.addTab(TestResultsTab(), self.tr("Test Results"))
        tabWidget.addTab(CurrentStatusTab(), self.tr("Current Status"))
        
        okButton = QPushButton(self.tr("Back"))
        okButton.setFixedWidth(1175)

        okButton.clicked.connect(self.choosePat)
        okButton.clicked.connect(self.close)

        buttonLayout = QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(okButton)
 
        #Sets up main layout of the program: file bar, tabs, button
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonLayout)
        self.window.setLayout(mainLayout)

class HomeTab(QWidget): 
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        widget = QWidget()
        
        parseXML()
        #Makes all of the information boxes for things to be displayed
        #SETS EVERYTHING AS GLOBAL TO BE ACCESSIBLE
        global firstNameEdit
        global lastNameEdit
        global dobValueLabel
        global ssnValueLabel
        global sexValueLabel
        global marValueLabel
        global patientPhotoID

        global addValueLabel
        global statValueLabel
        global counValueLabel
        global emeValueLabel
        global workValueLabel
        global contValueLabel
        global cityValueLabel
        global postValueLabel
        global emconValueLabel
        global homeValueLabel
        global mobValueLabel

        global occValueLabel
        global emAddValueLabel
        global staValueLabel
        global countValueLabel
        global emNameValueLabel
        global citValueLabel
        global postCodValueLabel

        global langValueLabel
        global raceValueLabel
        global famiValueLabel
        global homlValueLabel
        global incoValueLabel
        global inteValueLabel

        global primValueLabel
        global planValueLabel
        global effeValueLabel
        global plcyValueLabel
        global gropValueLabel
        global subsValueLabel
        global scrbValueLabel
        global relaValueLabel
        global dob2ValueLabel
        global ssn2ValueLabel
        global sex2ValueLabel
        global sub2ValueLabel
        global cit2ValueLabel
        global sta2ValueLabel
        global zip2ValueLabel
        global con2ValueLabel
        global phonValueLabel

        firstNameLabel = QLabel(self.tr("First Name:"))
        firstNameLabel.setFixedWidth(175)
        firstNameEdit = QLabel(firstNameVal)
        firstNameEdit.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        firstName = QGroupBox()
        firstNameLayout = QVBoxLayout()

        firstNameLayout.addWidget(firstNameLabel)
        firstNameLayout.addWidget(firstNameEdit)
        firstName.setLayout(firstNameLayout)

        lastNameLabel = QLabel(self.tr("Last Name:"))
        lastNameLabel.setFixedWidth(200)
        lastNameEdit = QLabel(lastNameVal)
        lastNameEdit.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        lastName = QGroupBox()
        lastNameLayout = QVBoxLayout()

        lastNameLayout.addWidget(lastNameLabel)
        lastNameLayout.addWidget(lastNameEdit)
        lastName.setLayout(lastNameLayout)

        patientName = QGroupBox()
        patientNameLayout = QHBoxLayout()

        patientNameLayout.addWidget(firstName)
        patientNameLayout.addWidget(lastName)

        patientName.setLayout(patientNameLayout)

        dobLabel = QLabel(self.tr("Date of Birth:"))
        dobValueLabel = QLabel(dobVal)
        dobValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
 
        ssnLabel = QLabel(self.tr("Social Security Number:"))
        ssnValueLabel = QLabel(ssnVal)
        ssnValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        sexLabel = QLabel(self.tr("Sex:"))
        sexValueLabel = QLabel(sexVal)
        sexValueLabel.setFixedWidth(400)
        sexValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        
        marLabel = QLabel(self.tr("Marital Status:"))
        marValueLabel = QLabel(marVal)
        marValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        picDir = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + picVal
        patientmap = QPixmap(picDir)
        patientPhotoID = QLabel("")
        patientmap = patientmap.scaled(200, 200, Qt.KeepAspectRatio)
        patientPhotoID.setPixmap(patientmap)
        patientPhotoID.setFixedWidth(200)
        patientPhotoID.setFixedHeight(150)

        '''CONTACT'''
        addLabel = QLabel(self.tr("Address:"))
        addValueLabel = QLabel(addVal)
        addValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        statLabel = QLabel(self.tr("State:"))
        statValueLabel = QLabel(statVal)
        statValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        counLabel = QLabel(self.tr("Country:"))
        counValueLabel = QLabel(countVal)
        counValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        emeLabel = QLabel(self.tr("Emergency Phone:"))
        emeValueLabel = QLabel(emPhoneVal)
        emeValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        workLabel = QLabel(self.tr("Work Phone:"))
        workValueLabel = QLabel(workPhoneVal)
        workValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        contLabel = QLabel(self.tr("Contact Email:"))
        contValueLabel = QLabel(emailVal)
        contValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        cityLabel = QLabel(self.tr("City:"))
        cityValueLabel = QLabel(cityVal)
        cityValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        postLabel = QLabel(self.tr("Postal Code:"))
        postValueLabel = QLabel(postVal)
        postValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        emconLabel = QLabel(self.tr("Emergency Contact:"))
        emconValueLabel = QLabel(emContVal)
        emconValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        homeLabel = QLabel(self.tr("Home Phone:"))
        homeValueLabel = QLabel(homePhoneVal)
        homeValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        mobLabel = QLabel(self.tr("Mobile Phone:"))
        mobValueLabel = QLabel(mobPhoneVal)
        mobValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        '''EMPLOYER'''
        occLabel = QLabel(self.tr("Occupation:"))
        occValueLabel = QLabel(occVal)
        occValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        emAddLabel = QLabel(self.tr("Employer Address:"))
        emAddValueLabel = QLabel(empAddVal)
        emAddValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        staLabel = QLabel(self.tr("State:"))
        staValueLabel = QLabel(empStatVal)
        staValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        countLabel = QLabel(self.tr("Country:"))
        countValueLabel = QLabel(empCountVal)
        countValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        emNameLabel = QLabel(self.tr("Employer Name:"))
        emNameValueLabel = QLabel(empNameVal)
        emNameValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        citLabel = QLabel(self.tr("City:"))
        citValueLabel = QLabel(empCityVal)
        citValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        postCodLabel = QLabel(self.tr("Postal Code:"))
        postCodValueLabel = QLabel(empPostVal)
        postCodValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        '''STATS'''
        langLabel = QLabel(self.tr("Language:"))
        langValueLabel = QLabel(langVal)
        langValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        raceLabel = QLabel(self.tr("Race/Ethnicity:"))
        raceValueLabel = QLabel(raceVal)
        raceValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        famiLabel = QLabel(self.tr("Family Size:"))
        famiValueLabel = QLabel(famSizeVal)
        famiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        homlLabel = QLabel(self.tr("Homeless, etc.:"))
        homlValueLabel = QLabel(homeVal)
        homlValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        incoLabel = QLabel(self.tr("Monthly Income:"))
        incoValueLabel = QLabel(incVal)
        incoValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        inteLabel = QLabel(self.tr("Interpreter:"))
        inteValueLabel = QLabel(interVal)
        inteValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        '''INSURANCE'''
        primLabel = QLabel(self.tr("Primary Insurance Provider:"))
        primValueLabel = QLabel(primVal)
        primValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        planLabel = QLabel(self.tr("Plan Name:"))
        planValueLabel = QLabel(planVal)
        planValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        effeLabel = QLabel(self.tr("Effective Date:"))
        effeValueLabel = QLabel(effDateVal)
        effeValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        plcyLabel = QLabel(self.tr("Policy Number:"))
        plcyValueLabel = QLabel(policyVal)
        plcyValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        gropLabel = QLabel(self.tr("Group Number:"))
        gropValueLabel = QLabel(groupVal)
        gropValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        subsLabel = QLabel(self.tr("Subscriber Employer (SE):"))
        subsValueLabel = QLabel(subsEmpVal)
        subsValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        scrbLabel = QLabel(self.tr("Subscriber:"))
        scrbValueLabel = QLabel(subsVal)
        scrbValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        relaLabel = QLabel(self.tr("Relationship:"))
        relaValueLabel = QLabel(relateVal)
        relaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        dob2Label = QLabel(self.tr("Date of Birth:"))
        dob2ValueLabel = QLabel(insDobVal)
        dob2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        ssn2Label = QLabel(self.tr("Social Security Number:"))
        ssn2ValueLabel = QLabel(insSsnVal)
        ssn2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        sex2Label = QLabel(self.tr("Sex:"))
        sex2ValueLabel = QLabel(insSexVal)
        sex2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        sub2Label = QLabel(self.tr("Subscriber Address:"))
        sub2ValueLabel = QLabel(insAddVal)
        sub2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        cit2Label = QLabel(self.tr("City:"))
        cit2ValueLabel = QLabel(insCityVal)
        cit2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        sta2Label = QLabel(self.tr("State:"))
        sta2ValueLabel = QLabel(insStateVal)
        sta2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        zip2Label = QLabel(self.tr("Zip Code:"))
        zip2ValueLabel = QLabel(insZipVal)
        zip2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        con2Label = QLabel(self.tr("Country:"))
        con2ValueLabel = QLabel(insCountVal)
        con2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        phonLabel = QLabel(self.tr("Subscriber Phone:"))
        phonValueLabel = QLabel(insPhoneVal)
        phonValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)


        #Box off each section of information displayed
        WhoRecordsGroup = QGroupBox(self.tr("Who"))
        ContactRecordsGroup = QGroupBox(self.tr("Contact"))
        EmployerRecordsGroup = QGroupBox(self.tr("Employer"))
        StatsRecordsGroup = QGroupBox(self.tr("Stats"))
        InsuranceRecordsGroup = QGroupBox(self.tr("Insurance"))

        #Assigns information to each box
        #WHO GROUP
        whoMiniGroup1 = QGroupBox()
        whoMiniGroup2 = QGroupBox()
        whoMiniGroup3 = QGroupBox()

        whoMiniGroup1Layout = QVBoxLayout()
        whoMiniGroup2Layout = QVBoxLayout()
        whoMiniGroup3Layout = QVBoxLayout()

        whoMiniGroup1Layout.addWidget(patientName)
        whoMiniGroup1Layout.addWidget(ssnLabel)
        whoMiniGroup1Layout.addWidget(ssnValueLabel)
        whoMiniGroup1Layout.addWidget(marLabel)
        whoMiniGroup1Layout.addWidget(marValueLabel)

        whoMiniGroup2Layout.addWidget(dobLabel)
        whoMiniGroup2Layout.addWidget(dobValueLabel)
        whoMiniGroup2Layout.addWidget(sexLabel)
        whoMiniGroup2Layout.addWidget(sexValueLabel)

        whoMiniGroup3Layout.addWidget(patientPhotoID)

        whoMiniGroup1.setLayout(whoMiniGroup1Layout)
        whoMiniGroup2.setLayout(whoMiniGroup2Layout)        
        whoMiniGroup3.setLayout(whoMiniGroup3Layout)

        WhoRecordsLayout = QHBoxLayout()
        WhoRecordsLayout.addWidget(whoMiniGroup1)
        WhoRecordsLayout.addWidget(whoMiniGroup2)
        WhoRecordsLayout.addWidget(whoMiniGroup3)
        WhoRecordsGroup.setLayout(WhoRecordsLayout)
        WhoRecordsGroup.setFixedWidth(1125)

        #CONTACT GROUP
        contactMiniGroup1 = QGroupBox()
        contactMiniGroup2 = QGroupBox()

        contactMiniGroup1Layout = QVBoxLayout()
        contactMiniGroup2Layout = QVBoxLayout()

        contactMiniGroup1Layout.addWidget(addLabel)
        contactMiniGroup1Layout.addWidget(addValueLabel)
        contactMiniGroup1Layout.addWidget(statLabel)
        contactMiniGroup1Layout.addWidget(statValueLabel)
        contactMiniGroup1Layout.addWidget(counLabel)
        contactMiniGroup1Layout.addWidget(counValueLabel)
        contactMiniGroup1Layout.addWidget(contLabel)
        contactMiniGroup1Layout.addWidget(contValueLabel)
        contactMiniGroup1Layout.addWidget(cityLabel)
        contactMiniGroup1Layout.addWidget(cityValueLabel)
        contactMiniGroup1Layout.addWidget(postLabel)
        contactMiniGroup1Layout.addWidget(postValueLabel)
        
        contactMiniGroup2Layout.addWidget(emeLabel)
        contactMiniGroup2Layout.addWidget(emeValueLabel)
        contactMiniGroup2Layout.addWidget(emconLabel)
        contactMiniGroup2Layout.addWidget(emconValueLabel)
        contactMiniGroup2Layout.addWidget(workLabel)
        contactMiniGroup2Layout.addWidget(workValueLabel)
        contactMiniGroup2Layout.addWidget(homeLabel)
        contactMiniGroup2Layout.addWidget(homeValueLabel)
        contactMiniGroup2Layout.addWidget(mobLabel)
        contactMiniGroup2Layout.addWidget(mobValueLabel)

        contactMiniGroup1.setLayout(contactMiniGroup1Layout)
        contactMiniGroup2.setLayout(contactMiniGroup2Layout)

        ContactRecordsLayout = QHBoxLayout()
        ContactRecordsLayout.addWidget(contactMiniGroup1)
        ContactRecordsLayout.addWidget(contactMiniGroup2)
        ContactRecordsGroup.setLayout(ContactRecordsLayout)
        ContactRecordsGroup.setFixedWidth(1125)

        #EMPLOYER GROUP
        employerMiniGroup1 = QGroupBox()
        employerMiniGroup2 = QGroupBox()

        employerMiniGroup1Layout = QVBoxLayout()
        employerMiniGroup2Layout = QVBoxLayout()

        employerMiniGroup1Layout.addWidget(occLabel)
        employerMiniGroup1Layout.addWidget(occValueLabel)
        employerMiniGroup1Layout.addWidget(emNameLabel)
        employerMiniGroup1Layout.addWidget(emNameValueLabel)
        employerMiniGroup1Layout.addWidget(emAddLabel)
        employerMiniGroup1Layout.addWidget(emAddValueLabel)

        employerMiniGroup2Layout.addWidget(staLabel)
        employerMiniGroup2Layout.addWidget(staValueLabel)
        employerMiniGroup2Layout.addWidget(countLabel)
        employerMiniGroup2Layout.addWidget(countValueLabel)
        employerMiniGroup2Layout.addWidget(citLabel)
        employerMiniGroup2Layout.addWidget(citValueLabel)
        employerMiniGroup2Layout.addWidget(postCodLabel)
        employerMiniGroup2Layout.addWidget(postCodValueLabel)

        employerMiniGroup1.setLayout(employerMiniGroup1Layout)
        employerMiniGroup2.setLayout(employerMiniGroup2Layout)

        EmployerRecordsLayout = QHBoxLayout()
        EmployerRecordsLayout.addWidget(employerMiniGroup1)
        EmployerRecordsLayout.addWidget(employerMiniGroup2)
        EmployerRecordsGroup.setLayout(EmployerRecordsLayout)
        EmployerRecordsGroup.setFixedWidth(1125)

        #STATS GROUP
        statsMiniGroup1 = QGroupBox()
        statsMiniGroup2 = QGroupBox()

        statsMiniGroup1Layout = QVBoxLayout()
        statsMiniGroup2Layout = QVBoxLayout()

        statsMiniGroup1Layout.addWidget(langLabel)
        statsMiniGroup1Layout.addWidget(langValueLabel)
        statsMiniGroup1Layout.addWidget(raceLabel)
        statsMiniGroup1Layout.addWidget(raceValueLabel)
        statsMiniGroup1Layout.addWidget(famiLabel)
        statsMiniGroup1Layout.addWidget(famiValueLabel)

        statsMiniGroup2Layout.addWidget(homlLabel)
        statsMiniGroup2Layout.addWidget(homlValueLabel)
        statsMiniGroup2Layout.addWidget(incoLabel)
        statsMiniGroup2Layout.addWidget(incoValueLabel)
        statsMiniGroup2Layout.addWidget(inteLabel)
        statsMiniGroup2Layout.addWidget(inteValueLabel)

        statsMiniGroup1.setLayout(statsMiniGroup1Layout)
        statsMiniGroup2.setLayout(statsMiniGroup2Layout)

        StatsRecordsLayout = QHBoxLayout()
        StatsRecordsLayout.addWidget(statsMiniGroup1)
        StatsRecordsLayout.addWidget(statsMiniGroup2)
        StatsRecordsGroup.setLayout(StatsRecordsLayout)
        StatsRecordsGroup.setFixedWidth(1125)

        #INSURANCE GROUP
        insuranceMiniGroup1 = QGroupBox()
        insuranceMiniGroup2 = QGroupBox()

        insuranceMiniGroup1Layout = QVBoxLayout()
        insuranceMiniGroup2Layout = QVBoxLayout()

        insuranceMiniGroup1Layout.addWidget(primLabel)
        insuranceMiniGroup1Layout.addWidget(primValueLabel)
        insuranceMiniGroup1Layout.addWidget(planLabel)
        insuranceMiniGroup1Layout.addWidget(planValueLabel)
        insuranceMiniGroup1Layout.addWidget(effeLabel)
        insuranceMiniGroup1Layout.addWidget(effeValueLabel)
        insuranceMiniGroup1Layout.addWidget(plcyLabel)
        insuranceMiniGroup1Layout.addWidget(plcyValueLabel)
        insuranceMiniGroup1Layout.addWidget(gropLabel)
        insuranceMiniGroup1Layout.addWidget(gropValueLabel)
        insuranceMiniGroup1Layout.addWidget(subsLabel)
        insuranceMiniGroup1Layout.addWidget(subsValueLabel)
        insuranceMiniGroup1Layout.addWidget(scrbLabel)
        insuranceMiniGroup1Layout.addWidget(scrbValueLabel)
        insuranceMiniGroup1Layout.addWidget(relaLabel)
        insuranceMiniGroup1Layout.addWidget(relaValueLabel)

        insuranceMiniGroup2Layout.addWidget(dob2Label)
        insuranceMiniGroup2Layout.addWidget(dob2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(ssn2Label)
        insuranceMiniGroup2Layout.addWidget(ssn2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sex2Label)
        insuranceMiniGroup2Layout.addWidget(sex2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sub2Label)
        insuranceMiniGroup2Layout.addWidget(sub2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(cit2Label)
        insuranceMiniGroup2Layout.addWidget(cit2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(sta2Label)
        insuranceMiniGroup2Layout.addWidget(sta2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(zip2Label)
        insuranceMiniGroup2Layout.addWidget(zip2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(con2Label)
        insuranceMiniGroup2Layout.addWidget(con2ValueLabel)
        insuranceMiniGroup2Layout.addWidget(phonLabel)
        insuranceMiniGroup2Layout.addWidget(phonValueLabel)

        insuranceMiniGroup1.setLayout(insuranceMiniGroup1Layout)
        insuranceMiniGroup2.setLayout(insuranceMiniGroup2Layout)

        InsuranceRecordsLayout = QHBoxLayout()
        InsuranceRecordsLayout.addWidget(insuranceMiniGroup1)
        InsuranceRecordsLayout.addWidget(insuranceMiniGroup2)
        InsuranceRecordsGroup.setLayout(InsuranceRecordsLayout)
        InsuranceRecordsGroup.setFixedWidth(1125)

        #Puts together each of the boxes
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(WhoRecordsGroup)
        mainLayout.addWidget(ContactRecordsGroup)
        mainLayout.addWidget(EmployerRecordsGroup)
        mainLayout.addWidget(StatsRecordsGroup)
        mainLayout.addWidget(InsuranceRecordsGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)

        #writeXML(0, "", "", "", "setup", "main")
        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)

class MedicalRecordsTab(QWidget):
    def addDiagButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Diagnosis #" + str(newDiagCounter)
        while testName in DiagnosesStatus:
            newDiagCounter += 1
            testName = "Diagnosis #" + str(newDiagCounter)

        tempVar = "Diagnosis #" + str(newDiagCounter)

        tempNewDiag = diagValueLabelEdit.text()
        tempNewDoct = diagDoctValueLabelEdit.text()
        tempNewDeta = detaValueLabelEdit.text()
        tempNewDate = diagDateValueLabelEdit.text()
        
        global tempNewDiagVars
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta, tempNewDate))

        newDiagnosesStatus = []
        newDiagnosesStatus.append(tempVar) 
        DiagnosesStatus.append(tempVar)           
        DiagnosesStatusListBox.insertItems(newDiagCounter - 1, newDiagnosesStatus)

        diagValueLabelEdit.setText("")
        diagDoctValueLabelEdit.setText("")
        detaValueLabelEdit.setText("")
        diagDateValueLabelEdit.setText("")

        diagValueLabel.setText("")
        diagDoctValueLabel.setText("")
        detaValueLabel.setText("")
        diagDateValueLabel.setText("")
        writeXML(newDiagCounter, "add", "diag", tempNewDiagVars)

    def diagEditButton(self,parent=None):
        if(editDiag.text() == "Edit"):
            editDiag.setText("Save")            
            diagValueLabelEdit.setText(diagValueLabel.text())
            diagDoctValueLabelEdit.setText(diagDoctValueLabel.text())
            detaValueLabelEdit.setText(detaValueLabel.text())
            diagDateValueLabelEdit.setText(diagDateValueLabel.text())

        else:
            editDiag.setText("Edit")

            tempDiag = diagValueLabelEdit.text()
            tempDoct = diagDoctValueLabelEdit.text()
            tempDeta = detaValueLabelEdit.text()
            tempDate = diagDateValueLabelEdit.text()
            
            global tempDiagVars
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta, tempDate))

            diagValueLabelEdit.setText("")
            diagDoctValueLabelEdit.setText("")
            detaValueLabelEdit.setText("")
            diagDateValueLabelEdit.setText("")

            diagValueLabel.setText("")
            diagDoctValueLabel.setText("")
            detaValueLabel.setText("")
            diagDateValueLabel.setText("")
            writeXML(1, "edit", "diag", tempDiagVars)

    def remDiagButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex
        remIndex = DiagnosesStatus.index("Diagnosis #" + str(len(DiagnosesStatus)))
        tempRemoval = DiagnosesStatus.pop(remIndex)

        while remIndex < len(DiagnosesStatus):
            diagListString = DiagnosesStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Diagnosis #" + str(diagListInt)
            DiagnosesStatus[remIndex] = newDiagListString
            remIndex += 1

        DiagnosesStatusListBox.clear()            
        DiagnosesStatusListBox.insertItems(0, DiagnosesStatus)

        diagValueLabel.setText("")
        diagDoctValueLabel.setText("")
        detaValueLabel.setText("")
        diagDateValueLabel.setText("")
        writeXML(remIndex, "remove", "diag", "")

    def addMediButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Medication #" + str(newDiagCounter)
        while testName in MedicationStatus:
            newDiagCounter += 1
            testName = "Medication #" + str(newDiagCounter)

        tempVar = "Medication #" + str(newDiagCounter)

        tempNewDiag = mediValueLabelEdit.text()
        tempNewDoct = doseValueLabelEdit.text()
        tempNewDeta = preValueLabelEdit.text()
        tempNewDate = datepValueLabelEdit.text()
        
        global tempNewDiagVars        
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta, tempNewDate))

        newMediStatus = []
        newMediStatus.append(tempVar) 
        MedicationStatus.append(tempVar)           
        MedicationStatusListBox.insertItems(newDiagCounter - 1, newMediStatus)

        mediValueLabelEdit.setText("")
        doseValueLabelEdit.setText("")
        preValueLabelEdit.setText("")
        datepValueLabelEdit.setText("")

        mediValueLabel.setText("")
        doseValueLabel.setText("")
        preValueLabel.setText("")
        datepValueLabel.setText("")
        writeXML(newDiagCounter, "add", "medi", tempNewDiagVars)

    def mediEditButton(self,parent=None):
        if editMedi.text() == "Edit":
            editMedi.setText("Save")
            
            mediValueLabelEdit.setText(mediValueLabel.text())
            doseValueLabelEdit.setText(doseValueLabel.text())
            preValueLabelEdit.setText(preValueLabel.text())
            datepValueLabelEdit.setText(datepValueLabel.text())
        else:
            editMedi.setText("Edit")

            tempDiag = mediValueLabelEdit.text()
            tempDoct = doseValueLabelEdit.text()
            tempDeta = preValueLabelEdit.text()
            tempDate = datepValueLabelEdit.text()
            
            global tempDiagVars            
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta, tempDate))

            mediValueLabelEdit.setText("")
            doseValueLabelEdit.setText("")
            preValueLabelEdit.setText("")
            datepValueLabelEdit.setText("")

            mediValueLabel.setText("")
            doseValueLabel.setText("")
            preValueLabel.setText("")
            datepValueLabel.setText("")
            writeXML(1, "edit", "medi", tempDiagVars)

    def remMediButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex
        remIndex = MedicationStatus.index("Medication #" + str(len(MedicationStatus)))
        tempRemoval = MedicationStatus.pop(remIndex)

        while remIndex < len(MedicationStatus):
            diagListString = MedicationStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Medication #" + str(diagListInt)
            MedicationStatus[remIndex] = newDiagListString
            remIndex += 1

        MedicationStatusListBox.clear()            
        MedicationStatusListBox.insertItems(0, MedicationStatus)

        mediValueLabel.setText("")
        doseValueLabel.setText("")
        preValueLabel.setText("")
        datepValueLabel.setText("")
        writeXML(remIndex, "remove", "medi", "")

    def addAlleButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Allergy #" + str(newDiagCounter)
        while testName in AllergiesStatus:
            newDiagCounter += 1
            testName = "Allergy #" + str(newDiagCounter)

        tempVar = "Allergy #" + str(newDiagCounter)

        tempNewDiag = allValueLabelEdit.text()
        tempNewDoct = seveValueLabelEdit.text()
        tempNewDeta = mitiValueLabelEdit.text()
        
        global tempNewDiagVars        
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta))

        newAllergiesStatus = []
        newAllergiesStatus.append(tempVar) 
        AllergiesStatus.append(tempVar)           
        AllergiesStatusListBox.insertItems(newDiagCounter - 1, newAllergiesStatus)

        allValueLabelEdit.setText("")
        seveValueLabelEdit.setText("")
        mitiValueLabelEdit.setText("")

        allValueLabel.setText("")
        seveValueLabel.setText("")
        mitiValueLabel.setText("")
        writeXML(newDiagCounter, "add", "alle", tempNewDiagVars)

    def alleEditButton(self,parent=None):
        if editAlle.text() == "Edit":
            editAlle.setText("Save")
            
            allValueLabelEdit.setText(allValueLabel.text())
            seveValueLabelEdit.setText(seveValueLabel.text())
            mitiValueLabelEdit.setText(mitiValueLabel.text())
        else:
            editAlle.setText("Edit")

            tempDiag = allValueLabelEdit.text()
            tempDoct = seveValueLabelEdit.text()
            tempDeta = mitiValueLabelEdit.text()
            
            global tempDiagVars            
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta))

            allValueLabelEdit.setText("")
            seveValueLabelEdit.setText("")
            mitiValueLabelEdit.setText("")

            allValueLabel.setText("")
            seveValueLabel.setText("")
            mitiValueLabel.setText("")
            writeXML(1, "edit", "alle", tempDiagVars)

    def remAlleButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex        
        remIndex = AllergiesStatus.index("Allergy #" + str(len(AllergiesStatus)))
        tempRemoval = AllergiesStatus.pop(remIndex)

        while remIndex < len(AllergiesStatus):
            diagListString = AllergiesStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Allergy #" + str(diagListInt)
            AllergiesStatus[remIndex] = newDiagListString
            remIndex += 1

        AllergiesStatusListBox.clear()            
        AllergiesStatusListBox.insertItems(0, AllergiesStatus)

        allValueLabel.setText("")
        seveValueLabel.setText("")
        mitiValueLabel.setText("")
        writeXML(remIndex, "remove", "alle", "")

    def addImmuButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Immunization #" + str(newDiagCounter)
        while testName in ImmunizationStatus:
            newDiagCounter += 1
            testName = "Immunization #" + str(newDiagCounter)

        tempVar = "Immunization #" + str(newDiagCounter)

        tempNewDiag = immValueLabelEdit.text()
        tempNewDoct = dateiValueLabelEdit.text()
        tempNewDeta = lociValueLabelEdit.text()
        
        global tempNewDiagVars        
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta))

        newImmunizationStatus = []
        newImmunizationStatus.append(tempVar) 
        ImmunizationStatus.append(tempVar)           
        ImmunizationStatusListBox.insertItems(newDiagCounter - 1, newImmunizationStatus)

        immValueLabelEdit.setText("")
        dateiValueLabelEdit.setText("")
        lociValueLabelEdit.setText("")

        immValueLabel.setText("")
        dateiValueLabel.setText("")
        lociValueLabel.setText("")
        writeXML(1, "add", "immu", tempNewDiagVars)

    def immuEditButton(self,parent=None):
        if editImmu.text() == "Edit":
            editImmu.setText("Save")
            
            immValueLabelEdit.setText(immValueLabel.text())
            dateiValueLabelEdit.setText(dateiValueLabel.text())
            lociValueLabelEdit.setText(lociValueLabel.text())
        else:
            editImmu.setText("Edit")

            tempDiag = immValueLabelEdit.text()
            tempDoct = dateiValueLabelEdit.text()
            tempDeta = lociValueLabelEdit.text()

            global tempDiagVars            
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta))

            immValueLabelEdit.setText("")
            dateiValueLabelEdit.setText("")
            lociValueLabelEdit.setText("")

            immValueLabel.setText("")
            dateiValueLabel.setText("")
            lociValueLabel.setText("")
            writeXML(1, "edit", "immu", tempDiagVars)

    def remImmuButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex        
        remIndex = ImmunizationStatus.index("Immunization #" + str(len(ImmunizationStatus)))
        tempRemoval = ImmunizationStatus.pop(remIndex)

        while remIndex < len(ImmunizationStatus):
            diagListString = ImmunizationStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Immunization #" + str(diagListInt)
            ImmunizationStatus[remIndex] = newDiagListString
            remIndex += 1

        ImmunizationStatusListBox.clear()            
        ImmunizationStatusListBox.insertItems(0, ImmunizationStatus)

        immValueLabel.setText("")
        dateiValueLabel.setText("")
        lociValueLabel.setText("")
        writeXML(remIndex, "remove", "immu", "")

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        widget = QWidget()
        content = ET.parse(xmlDir)
        medRecords=content.find("medRecords")

        diagCounter = 1
        mediCounter = 1
        immCounter = 1
        allCounter = 1

        #DIAGNOSES LIST
        global DiagnosesStatusListBox
        global DiagnosesStatus

        DiagnosesStatusListBox = diagnosisWidget()
        DiagnosesStatus = []
 
        while "diagID" + str(diagCounter) in myXmlDictionary: 
            tempVar = "Diagnosis #" + str(diagCounter)
            DiagnosesStatus.append(tempVar)
            diagCounter += 1
        DiagnosesStatusListBox.insertItems(0, DiagnosesStatus)
        DiagnosesStatusListBox.connect(DiagnosesStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               DiagnosesStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #MEDICATION LIST
        global MedicationStatusListBox
        global MedicationStatus

        MedicationStatusListBox = medicationWidget()
        MedicationStatus = []
 
        while "mediID" + str(mediCounter) in myXmlDictionary:
            tempVar = "Medication #" + str(mediCounter)
            MedicationStatus.append(tempVar)
            mediCounter += 1
        MedicationStatusListBox.insertItems(0, MedicationStatus)
        MedicationStatusListBox.connect(MedicationStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               MedicationStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #IMMUNIZATION LIST
        global ImmunizationStatusListBox
        global ImmunizationStatus

        ImmunizationStatusListBox = immunizationWidget()
        ImmunizationStatus = []
 
        while "immID" + str(immCounter) in myXmlDictionary: 
            tempVar = "Immunization #" + str(immCounter)
            ImmunizationStatus.append(tempVar)
            immCounter += 1
        ImmunizationStatusListBox.insertItems(0, ImmunizationStatus)
        ImmunizationStatusListBox.connect(ImmunizationStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               ImmunizationStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #ALLERGIES LIST
        global AllergiesStatusListBox
        global AllergiesStatus

        AllergiesStatusListBox = allergyWidget()
        AllergiesStatus = []

        while "allID" + str(allCounter) in myXmlDictionary:
            tempVar = "Allergy #" + str(allCounter)
            AllergiesStatus.append(tempVar)
            allCounter += 1
        AllergiesStatusListBox.insertItems(0, AllergiesStatus)
        AllergiesStatusListBox.connect(AllergiesStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               AllergiesStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Sets up side boxes information
        #Diagnoses
        global diagValueLabel
        diagLabel = QLabel(self.tr("Diagnosis:"))
        diagValueLabel = QLabel("")
        diagValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global diagDoctValueLabel
        doctLabel = QLabel(self.tr("Doctor:"))
        diagDoctValueLabel = QLabel("")
        diagDoctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global detaValueLabel
        detaLabel = QLabel(self.tr("Detailed Diagnosis:"))
        detaValueLabel = QLabel("")
        detaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global diagDateValueLabel
        dateLabel = QLabel(self.tr("Date:"))
        diagDateValueLabel = QLabel("")
        diagDateValueLabel.setFixedWidth(250)
        diagDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Medication
        global mediValueLabel
        mediLabel = QLabel(self.tr("Medication:"))
        mediValueLabel = QLabel("")
        mediValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global doseValueLabel
        doseLabel = QLabel(self.tr("Daily Dosage:"))
        doseValueLabel = QLabel("")
        doseValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global preValueLabel
        preLabel = QLabel(self.tr("Prescriber:"))
        preValueLabel = QLabel("")
        preValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global datepValueLabel
        datepLabel = QLabel(self.tr("Date Prescribed:"))
        datepValueLabel = QLabel("")
        datepValueLabel.setFixedWidth(250)
        datepValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Immunize
        global immValueLabel
        immLabel = QLabel(self.tr("Immunization:"))
        immValueLabel = QLabel("")
        immValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global dateiValueLabel
        dateiLabel = QLabel(self.tr("Date Immunized:"))
        dateiValueLabel = QLabel("")
        dateiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global lociValueLabel
        lociLabel = QLabel(self.tr("Location:"))
        lociValueLabel = QLabel("")
        lociValueLabel.setFixedWidth(250)
        lociValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Allergies
        global allValueLabel
        allLabel = QLabel(self.tr("Allergy:"))
        allValueLabel = QLabel("")
        allValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global seveValueLabel
        seveLabel = QLabel(self.tr("Severity:"))
        seveValueLabel = QLabel("")
        seveValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global mitiValueLabel
        mitiLabel = QLabel(self.tr("Mitigation Technique:"))
        mitiLabel.setFixedWidth(250)
        mitiValueLabel = QLabel("")
        mitiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Sets up side boxes information
        #Diagnoses
        global diagValueLabelEdit        
        diagIDLabelEdit = QLabel(self.tr("Diagnosis:"))
        diagValueLabelEdit = QLineEdit("")

        global diagDoctValueLabelEdit
        doctLabelEdit = QLabel(self.tr("Doctor:"))
        diagDoctValueLabelEdit = QLineEdit("")

        global detaValueLabelEdit
        detaLabelEdit = QLabel(self.tr("Detailed Diagnosis:"))
        detaValueLabelEdit = QLineEdit("")

        global diagDateValueLabelEdit
        dateLabelEdit = QLabel(self.tr("Date:"))
        diagDateValueLabelEdit = QLineEdit("")

        #Medication
        global mediValueLabelEdit        
        mediIDLabelEdit = QLabel(self.tr("Medication:"))
        mediValueLabelEdit = QLineEdit("")

        global doseValueLabelEdit
        doseLabelEdit = QLabel(self.tr("Daily Dosage:"))
        doseValueLabelEdit = QLineEdit("")

        global preValueLabelEdit
        preLabelEdit = QLabel(self.tr("Prescriber:"))
        preValueLabelEdit = QLineEdit("")

        global datepValueLabelEdit
        datepLabelEdit = QLabel(self.tr("Date Prescribed:"))
        datepValueLabelEdit = QLineEdit("")

        #Immunize
        global immValueLabelEdit        
        immIDLabelEdit = QLabel(self.tr("Immunization:"))
        immValueLabelEdit = QLineEdit("")

        global dateiValueLabelEdit
        dateiLabelEdit = QLabel(self.tr("Date Immunized:"))
        dateiValueLabelEdit = QLineEdit("")

        global lociValueLabelEdit
        lociLabelEdit = QLabel(self.tr("Location:"))
        lociValueLabelEdit = QLineEdit("")

        #Allergies
        global allValueLabelEdit        
        allIDLabelEdit = QLabel(self.tr("Allergy:"))
        allValueLabelEdit = QLineEdit("")

        global seveValueLabelEdit
        seveLabelEdit = QLabel(self.tr("Severity:"))
        seveValueLabelEdit = QLineEdit("")

        global mitiValueLabelEdit
        mitiLabelEdit = QLabel(self.tr("Mitigation Technique:"))
        mitiValueLabelEdit = QLineEdit("")

        #Sets up STAGNANT final boxes
        DiagnosisVBox = QGroupBox()
        MedicationVBox = QGroupBox()
        ImmunizationVBox = QGroupBox()
        AllergiesVBox = QGroupBox()

        DiagnosisVBoxLayout = QVBoxLayout()
        MedicationVBoxLayout = QVBoxLayout()
        ImmunizationVBoxLayout = QVBoxLayout()
        AllergiesVBoxLayout = QVBoxLayout()

        DiagnosisVBoxLayout.addWidget(diagLabel)
        DiagnosisVBoxLayout.addWidget(diagValueLabel)
        DiagnosisVBoxLayout.addWidget(doctLabel)
        DiagnosisVBoxLayout.addWidget(diagDoctValueLabel)
        DiagnosisVBoxLayout.addWidget(detaLabel)
        DiagnosisVBoxLayout.addWidget(detaValueLabel)
        DiagnosisVBoxLayout.addWidget(dateLabel)
        DiagnosisVBoxLayout.addWidget(diagDateValueLabel)

        MedicationVBoxLayout.addWidget(mediLabel)
        MedicationVBoxLayout.addWidget(mediValueLabel)
        MedicationVBoxLayout.addWidget(doseLabel)
        MedicationVBoxLayout.addWidget(doseValueLabel)
        MedicationVBoxLayout.addWidget(preLabel)
        MedicationVBoxLayout.addWidget(preValueLabel)
        MedicationVBoxLayout.addWidget(datepLabel)
        MedicationVBoxLayout.addWidget(datepValueLabel)

        ImmunizationVBoxLayout.addWidget(immLabel)
        ImmunizationVBoxLayout.addWidget(immValueLabel)
        ImmunizationVBoxLayout.addWidget(dateiLabel)
        ImmunizationVBoxLayout.addWidget(dateiValueLabel)
        ImmunizationVBoxLayout.addWidget(lociLabel)
        ImmunizationVBoxLayout.addWidget(lociValueLabel)

        AllergiesVBoxLayout.addWidget(allLabel)
        AllergiesVBoxLayout.addWidget(allValueLabel)
        AllergiesVBoxLayout.addWidget(seveLabel)
        AllergiesVBoxLayout.addWidget(seveValueLabel)
        AllergiesVBoxLayout.addWidget(mitiLabel)
        AllergiesVBoxLayout.addWidget(mitiValueLabel)

        DiagnosisVBox.setLayout(DiagnosisVBoxLayout)
        MedicationVBox.setLayout(MedicationVBoxLayout)
        ImmunizationVBox.setLayout(ImmunizationVBoxLayout)
        AllergiesVBox.setLayout(AllergiesVBoxLayout)

        #SET FINAL EDITABLE BOXES
        DiagnosisVBoxEdit = QGroupBox()
        MedicationVBoxEdit = QGroupBox()
        ImmunizationVBoxEdit = QGroupBox()
        AllergiesVBoxEdit = QGroupBox()

        DiagnosisVBoxLayoutEdit = QVBoxLayout()
        MedicationVBoxLayoutEdit = QVBoxLayout()
        ImmunizationVBoxLayoutEdit = QVBoxLayout()
        AllergiesVBoxLayoutEdit = QVBoxLayout()

        DiagnosisVBoxLayoutEdit.addWidget(diagIDLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(diagValueLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(doctLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(diagDoctValueLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(detaLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(detaValueLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(dateLabelEdit)
        DiagnosisVBoxLayoutEdit.addWidget(diagDateValueLabelEdit)

        MedicationVBoxLayoutEdit.addWidget(mediIDLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(mediValueLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(doseLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(doseValueLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(preLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(preValueLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(datepLabelEdit)
        MedicationVBoxLayoutEdit.addWidget(datepValueLabelEdit)

        ImmunizationVBoxLayoutEdit.addWidget(immIDLabelEdit)
        ImmunizationVBoxLayoutEdit.addWidget(immValueLabelEdit)
        ImmunizationVBoxLayoutEdit.addWidget(dateiLabelEdit)
        ImmunizationVBoxLayoutEdit.addWidget(dateiValueLabelEdit)
        ImmunizationVBoxLayoutEdit.addWidget(lociLabelEdit)
        ImmunizationVBoxLayoutEdit.addWidget(lociValueLabelEdit)

        AllergiesVBoxLayoutEdit.addWidget(allIDLabelEdit)
        AllergiesVBoxLayoutEdit.addWidget(allValueLabelEdit)
        AllergiesVBoxLayoutEdit.addWidget(seveLabelEdit)
        AllergiesVBoxLayoutEdit.addWidget(seveValueLabelEdit)
        AllergiesVBoxLayoutEdit.addWidget(mitiLabelEdit)
        AllergiesVBoxLayoutEdit.addWidget(mitiValueLabelEdit)

        DiagnosisVBoxEdit.setLayout(DiagnosisVBoxLayoutEdit)
        MedicationVBoxEdit.setLayout(MedicationVBoxLayoutEdit)
        ImmunizationVBoxEdit.setLayout(ImmunizationVBoxLayoutEdit)
        AllergiesVBoxEdit.setLayout(AllergiesVBoxLayoutEdit)

        #Creates buttons
        global editDiag

        addDiag = QPushButton(self.tr("Add"))
        editDiag = QPushButton(self.tr("Edit"))
        remDiag = QPushButton(self.tr("Remove"))

        addDiag.clicked.connect(self.addDiagButton)
        editDiag.clicked.connect(self.diagEditButton)
        remDiag.clicked.connect(self.remDiagButton)

        global editMedi

        addMedi = QPushButton(self.tr("Add"))
        editMedi = QPushButton(self.tr("Edit"))
        remMedi = QPushButton(self.tr("Remove"))

        addMedi.clicked.connect(self.addMediButton)
        editMedi.clicked.connect(self.mediEditButton)
        remMedi.clicked.connect(self.remMediButton)

        global editImmu

        addImmu = QPushButton(self.tr("Add"))
        editImmu = QPushButton(self.tr("Edit"))
        remImmu = QPushButton(self.tr("Remove"))

        addImmu.clicked.connect(self.addImmuButton)
        editImmu.clicked.connect(self.immuEditButton)
        remImmu.clicked.connect(self.remImmuButton)

        global editAlle

        addAlle = QPushButton(self.tr("Add"))
        editAlle = QPushButton(self.tr("Edit"))
        remAlle = QPushButton(self.tr("Remove"))

        addAlle.clicked.connect(self.addAlleButton)
        editAlle.clicked.connect(self.alleEditButton)
        remAlle.clicked.connect(self.remAlleButton)

        #Sets up each of the buttons
        DiagButton = QGroupBox()
        DiagButtonLayout = QHBoxLayout()

        MediButton = QGroupBox()
        MediButtonLayout = QHBoxLayout()

        ImmuButton = QGroupBox()
        ImmuButtonLayout = QHBoxLayout()

        AlleButton = QGroupBox()
        AlleButtonLayout = QHBoxLayout()

        DiagButtonLayout.addWidget(addDiag)
        DiagButtonLayout.addWidget(editDiag)
        DiagButtonLayout.addWidget(remDiag)

        MediButtonLayout.addWidget(addMedi)
        MediButtonLayout.addWidget(editMedi)
        MediButtonLayout.addWidget(remMedi)

        ImmuButtonLayout.addWidget(addImmu)
        ImmuButtonLayout.addWidget(editImmu)
        ImmuButtonLayout.addWidget(remImmu)

        AlleButtonLayout.addWidget(addAlle)
        AlleButtonLayout.addWidget(editAlle)
        AlleButtonLayout.addWidget(remAlle)

        DiagButton.setLayout(DiagButtonLayout)
        MediButton.setLayout(MediButtonLayout)
        ImmuButton.setLayout(ImmuButtonLayout)
        AlleButton.setLayout(AlleButtonLayout)

        #Final button display
        DiagButtonFin = QGroupBox()
        MediButtonFin = QGroupBox()
        ImmuButtonFin = QGroupBox()
        AlleButtonFin = QGroupBox()

        DiagButtonFinLayout = QVBoxLayout()
        MediButtonFinLayout = QVBoxLayout()
        ImmuButtonFinLayout = QVBoxLayout()
        AlleButtonFinLayout = QVBoxLayout()

        DiagButtonFinLayout.addWidget(DiagnosesStatusListBox)
        DiagButtonFinLayout.addWidget(DiagButton)

        MediButtonFinLayout.addWidget(MedicationStatusListBox)
        MediButtonFinLayout.addWidget(MediButton)
        
        ImmuButtonFinLayout.addWidget(ImmunizationStatusListBox)
        ImmuButtonFinLayout.addWidget(ImmuButton)
        
        AlleButtonFinLayout.addWidget(AllergiesStatusListBox)
        AlleButtonFinLayout.addWidget(AlleButton)        

        DiagButtonFin.setLayout(DiagButtonFinLayout)
        MediButtonFin.setLayout(MediButtonFinLayout)
        ImmuButtonFin.setLayout(ImmuButtonFinLayout)
        AlleButtonFin.setLayout(AlleButtonFinLayout)        

        #Displays each of list on page
        DiagnosesGroup = QGroupBox(self.tr("Diagnoses"))
        MedicationGroup = QGroupBox(self.tr("Medication"))
        ImmunizationGroup = QGroupBox(self.tr("Immunization"))
        AllergiesGroup = QGroupBox(self.tr("Allergies"))

        DiagnosesGroupLayout = QHBoxLayout()
        DiagnosesGroupLayout.addWidget(DiagButtonFin)
        DiagnosesGroupLayout.addWidget(DiagnosisVBox)
        DiagnosesGroupLayout.addWidget(DiagnosisVBoxEdit)

        MedicationGroupLayout = QHBoxLayout()
        MedicationGroupLayout.addWidget(MediButtonFin)
        MedicationGroupLayout.addWidget(MedicationVBox)
        MedicationGroupLayout.addWidget(MedicationVBoxEdit)

        ImmunizationGroupLayout = QHBoxLayout()
        ImmunizationGroupLayout.addWidget(ImmuButtonFin)
        ImmunizationGroupLayout.addWidget(ImmunizationVBox)
        ImmunizationGroupLayout.addWidget(ImmunizationVBoxEdit)

        AllergiesGroupLayout = QHBoxLayout()
        AllergiesGroupLayout.addWidget(AlleButtonFin)
        AllergiesGroupLayout.addWidget(AllergiesVBox)
        AllergiesGroupLayout.addWidget(AllergiesVBoxEdit)

        DiagnosesGroup.setLayout(DiagnosesGroupLayout)
        MedicationGroup.setLayout(MedicationGroupLayout)
        ImmunizationGroup.setLayout(ImmunizationGroupLayout)
        AllergiesGroup.setLayout(AllergiesGroupLayout)

        #Overall layout
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(DiagnosesGroup)
        mainLayout.addWidget(MedicationGroup)
        mainLayout.addWidget(ImmunizationGroup)
        mainLayout.addWidget(AllergiesGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)

        #writeXML(0, "", "", "", "setup", "medi")
        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)

class diagnosisWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global diagNumberVal

        diagNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["diagID" + str(diagNumberVal)]
        tempVar2 = myXmlDictionary["doctor" + str(diagNumberVal)]
        tempVar3 = myXmlDictionary["details" + str(diagNumberVal)]
        tempVar4 = myXmlDictionary["diagDate" + str(diagNumberVal)]

        diagValueLabel.setText(tempVar1)
        diagDoctValueLabel.setText(tempVar2)
        detaValueLabel.setText(tempVar3)
        diagDateValueLabel.setText(tempVar4)

class medicationWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global mediNumberVal

        mediNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["mediID" + str(mediNumberVal)]
        tempVar2 = myXmlDictionary["dosage" + str(mediNumberVal)]
        tempVar3 = myXmlDictionary["prescriber" + str(mediNumberVal)]
        tempVar4 = myXmlDictionary["mediDate" + str(mediNumberVal)]

        mediValueLabel.setText(tempVar1)
        doseValueLabel.setText(tempVar2)
        preValueLabel.setText(tempVar3)
        datepValueLabel.setText(tempVar4)

class immunizationWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global immNumberVal

        immNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["immID" + str(immNumberVal)]
        tempVar2 = myXmlDictionary["immDate" + str(immNumberVal)]
        tempVar3 = myXmlDictionary["location" + str(immNumberVal)]

        immValueLabel.setText(tempVar1)
        dateiValueLabel.setText(tempVar2)
        lociValueLabel.setText(tempVar3)
        
class allergyWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global alleNumberVal
        alleNumberVal = str(item.text())[-1]
        
        allValueLabel.setText(myXmlDictionary["allID" + alleNumberVal])
        seveValueLabel.setText(myXmlDictionary["severity" + alleNumberVal])
        mitiValueLabel.setText(myXmlDictionary["mitigation" + alleNumberVal])

class TestResultsTab(QWidget):
    def addRadButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Radiology Scan #" + str(newDiagCounter)
        while testName in RadiologyStatus:
            newDiagCounter += 1
            testName = "Radiology Scan #" + str(newDiagCounter)

        tempVar = "Radiology Scan #" + str(newDiagCounter)

        tempNewDiag = doctValueLabelEdit.text()
        tempNewDoct = radDateValueLabelEdit.text()
        tempNewDeta = radNoteValueLabelEdit.text()
        try:
            tempDate = radDirectory[0]
        except NameError:
            tempDate = ""
        
        global tempNewDiagVars
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempDate, tempNewDiag, tempNewDoct, tempNewDeta))

        newRadiologyStatus = []
        newRadiologyStatus.append(tempVar) 
        RadiologyStatus.append(tempVar)           
        RadiologyListBox.insertItems(newDiagCounter - 1, newRadiologyStatus)

        doctValueLabelEdit.setText("")
        radDateValueLabelEdit.setText("")
        radNoteValueLabelEdit.setText("")
        radFileName.setText("")

        doctValueLabel.setText("")
        radDateValueLabel.setText("")
        radNoteValueLabel.setText("")
        editRadImgVal.setText("")

        radioImg.clear()
        writeXML(newDiagCounter, "add", "rad", tempNewDiagVars)

    def radEditButton(self,parent=None):
        if editRad.text() == "Edit":
            editRad.setText("Save")
            
            doctValueLabelEdit.setText(doctValueLabel.text())
            radDateValueLabelEdit.setText(radDateValueLabel.text())
            radNoteValueLabelEdit.setText(radNoteValueLabel.text())
            editRadImgVal.setText(radFileName.text())
        else:
            editRad.setText("Edit")

            tempDiag = doctValueLabelEdit.text()
            tempDoct = radDateValueLabelEdit.text()
            tempDeta = radNoteValueLabelEdit.text()
            try:
                tempDate = radDirectory[0]
            except NameError:
                tempDate = ""
            
            global tempDiagVars            
            tempDiagVars = []
            tempDiagVars.extend((tempDate, tempDiag, tempDoct, tempDeta))

            doctValueLabelEdit.setText("")
            radDateValueLabelEdit.setText("")
            radNoteValueLabelEdit.setText("")
            radFileName.setText("")

            doctValueLabel.setText("")
            radDateValueLabel.setText("")
            radNoteValueLabel.setText("")
            editRadImgVal.setText("")
            radioImg.clear()
            writeXML(1, "edit", "rad", tempDiagVars)

    def remRadButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex        
        remIndex = RadiologyStatus.index("Radiology Scan #" + str(len(RadiologyStatus)))
        tempRemoval = RadiologyStatus.pop(remIndex)

        while remIndex < len(RadiologyStatus):
            diagListString = RadiologyStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Radiology Scan #" + str(diagListInt)
            RadiologyStatus[remIndex] = newDiagListString
            remIndex += 1

        RadiologyListBox.clear()            
        RadiologyListBox.insertItems(0, RadiologyStatus)

        doctValueLabel.setText("")
        radDateValueLabel.setText("")
        radNoteValueLabel.setText("")
        radFileName.setText("")
        radioImg.clear()

        writeXML(remIndex, "remove", "rad", "")

    def addLabButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Lab Test #" + str(newDiagCounter)
        while testName in LabStatus:
            newDiagCounter += 1
            testName = "Lab Test #" + str(newDiagCounter)

        tempVar = "Lab Test #" + str(newDiagCounter)

        tempNewDiag = labLocaValueLabelEdit.text()
        tempNewDoct = labDate2ValueLabelEdit.text()
        tempNewDeta = note2ValueLabelEdit.text()
        try:
            tempDate = labDirectory[0]
        except NameError:
            tempDate = ""
        
        global tempNewDiagVars
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempDate, tempNewDiag, tempNewDoct, tempNewDeta))

        newLabStatus = []
        newLabStatus.append(tempVar) 
        LabStatus.append(tempVar)           
        LabStatusListBox.insertItems(newDiagCounter - 1, newLabStatus)

        labLocaValueLabelEdit.setText("")
        labDate2ValueLabelEdit.setText("")
        note2ValueLabelEdit.setText("")
        labFileName.setText("")

        labLocaValueLabel.setText("")
        labDate2ValueLabel.setText("")
        note2ValueLabel.setText("")
        editLabImgVal.setText("")
        labImg.clear()
        writeXML(newDiagCounter, "add", "lab", tempNewDiagVars)

    def labEditButton(self,parent=None):
        if editLab.text() == "Edit":
            editLab.setText("Save")
            
            labLocaValueLabelEdit.setText(labLocaValueLabel.text())
            labDate2ValueLabelEdit.setText(labDate2ValueLabel.text())
            note2ValueLabelEdit.setText(note2ValueLabel.text())
            editLabImgVal.setText(labFileName.text())
        else:
            editLab.setText("Edit")

            tempDiag = labLocaValueLabelEdit.text()
            tempDoct = labDate2ValueLabelEdit.text()
            tempDeta = note2ValueLabelEdit.text()
            try:
                tempDate = labDirectory[0]
            except NameError:
                tempDate = ""
            
            global tempDiagVars
            tempDiagVars = []
            tempDiagVars.extend((tempDate, tempDiag, tempDoct, tempDeta))

            labLocaValueLabelEdit.setText("")
            labDate2ValueLabelEdit.setText("")
            note2ValueLabelEdit.setText("")
            labFileName.setText("")

            labLocaValueLabel.setText("")
            labDate2ValueLabel.setText("")
            note2ValueLabel.setText("")
            editLabImgVal.setText("")
            labImg.clear()
            writeXML(1, "edit", "lab", tempDiagVars)

    def remLabButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex
        remIndex = LabStatus.index("Lab Test #" + str(len(LabStatus)))
        tempRemoval = LabStatus.pop(remIndex)

        while remIndex < len(LabStatus):
            diagListString = LabStatus[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Lab Test #" + str(diagListInt)
            LabStatus[remIndex] = newDiagListString
            remIndex += 1

        LabStatusListBox.clear()            
        LabStatusListBox.insertItems(0, LabStatus)

        labLocaValueLabel.setText("")
        labDate2ValueLabel.setText("")
        note2ValueLabel.setText("")
        labFileName.setText("")
        labImg.clear()

        writeXML(remIndex, "remove", "lab", "")

    def radSelectFile(self):
        global radDirectory
        radDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(radDirectory).rfind('/')
        endPos = str(radDirectory).rfind("',")
        subString = str(radDirectory)[startPos+1:endPos]
        
        newLocation = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + subString
        radiomap = QPixmap(newLocation)
        radiomap = radiomap.scaled(250, 250, Qt.KeepAspectRatio)

        radioImg.setPixmap(radiomap)
        copy(radDirectory[0], newLocation)
        editRadImgVal.setText(subString)
        
        tempList = list(radDirectory)
        tempList[0] = subString
        radDirectory = tuple(tempList)

    def labSelectFile(self):
        global labDirectory
        labDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(labDirectory).rfind('/')
        endPos = str(labDirectory).rfind("',")
        subString = str(labDirectory)[startPos+1:endPos]

        newLocation = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + subString
        labmap = QPixmap(newLocation)
        labmap = labmap.scaled(250, 250, Qt.KeepAspectRatio)

        labImg.setPixmap(newLocation)
        copy(labDirectory[0], newLocation)
        editLabImgVal.setText(subString)

        tempList = list(labDirectory)
        tempList[0] = subString
        labDirectory = tuple(tempList)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        widget = QWidget()
       
        content = ET.parse(xmlDir)
        testResults=content.find("testResults")

        labCounter = 1
        radCounter = 1

        #DIAGNOSES LIST
        global RadiologyListBox
        global RadiologyStatus
        RadiologyListBox = radWidget()
        RadiologyStatus = []
        
        while "radImg" + str(radCounter) in myXmlDictionary:
            tempVar = "Radiology Scan #" + str(radCounter)
            RadiologyStatus.append(tempVar)
            radCounter += 1
        RadiologyListBox.insertItems(0, RadiologyStatus)
        RadiologyListBox.connect(RadiologyListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               RadiologyListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))
        
        #MEDICATION LIST
        global LabStatusListBox
        global LabStatus
        LabStatusListBox = labWidget()
        LabStatus = []
 
        while "labImg" + str(labCounter) in myXmlDictionary:
            tempVar = "Lab Test #" + str(labCounter)
            LabStatus.append(tempVar)
            labCounter += 1
        LabStatusListBox.insertItems(0, LabStatus)
        LabStatusListBox.connect(LabStatusListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               LabStatusListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Sets up side boxes information - STAGNANT
        #Radiology
        global doctValueLabel
        doctLabel = QLabel(self.tr("Location/Doctor:"))
        doctValueLabel = QLabel("")
        doctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        doctValueLabel.setFixedWidth(200)

        global radDateValueLabel
        dateLabel = QLabel(self.tr("Date:"))
        radDateValueLabel = QLabel("")
        radDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global radNoteValueLabel
        noteLabel = QLabel(self.tr("Notes:"))
        radNoteValueLabel = QLabel("")
        radNoteValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Lab Tests
        global labLocaValueLabel
        locaLabel = QLabel(self.tr("Location/Doctor:"))
        labLocaValueLabel = QLabel("")
        labLocaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        labLocaValueLabel.setFixedWidth(200)

        global labDate2ValueLabel
        date2Label = QLabel(self.tr("Date:"))
        labDate2ValueLabel = QLabel("")
        labDate2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global note2ValueLabel
        note2Label = QLabel(self.tr("Notes:"))
        note2ValueLabel = QLabel("")
        note2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #SET EDITABLE BOXES
        #Radiology
        global doctValueLabelEdit
        doctLabelEdit = QLabel(self.tr("Location/Doctor:"))
        doctValueLabelEdit = QLineEdit("")

        global radDateValueLabelEdit
        dateLabelEdit = QLabel(self.tr("Date:"))
        radDateValueLabelEdit = QLineEdit("")

        global radNoteValueLabelEdit
        noteLabelEdit = QLabel(self.tr("Notes:"))
        radNoteValueLabelEdit = QLineEdit("")

        global editRadImg
        editRadImgButt = QPushButton(self.tr("Select Radiology Image"))
        editRadImgButt.clicked.connect(self.radSelectFile)

        global editRadImgVal
        global editLabImgVal

        editRadImgVal = QLabel("")
        editLabImgVal = QLabel("")

        editRadImgVal.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        editLabImgVal.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global radFileName
        global labFileName

        radFileName = QLabel("")
        labFileName = QLabel("")

        radFileName.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        labFileName.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        radFileNameLabel = QLabel("Image File Name")
        labFileNameLabel = QLabel("Image File Name")

        #Lab Tests
        global labLocaValueLabelEdit
        locaLabelEdit = QLabel(self.tr("Location/Doctor:"))
        labLocaValueLabelEdit = QLineEdit("")

        global labDate2ValueLabelEdit
        date2LabelEdit = QLabel(self.tr("Date:"))
        labDate2ValueLabelEdit = QLineEdit("")

        global note2ValueLabelEdit
        note2LabelEdit = QLabel(self.tr("Notes:"))
        note2ValueLabelEdit = QLineEdit("")

        editLabImgButt = QPushButton(self.tr("Select Lab Image"))
        editLabImgButt.clicked.connect(self.labSelectFile)
        global editLabImg

        #FINAL BUTTON SET
        editRadImg = QGroupBox()
        editLabImg = QGroupBox()

        editRadImgLayout = QHBoxLayout()
        editLabImgLayout = QHBoxLayout()

        editRadImgLayout.addWidget(editRadImgButt)
        editLabImgLayout.addWidget(editLabImgButt)

        editRadImgLayout.addWidget(editRadImgVal)
        editLabImgLayout.addWidget(editLabImgVal)

        editRadImg.setLayout(editRadImgLayout)
        editLabImg.setLayout(editLabImgLayout)

        #Button setup
        global addRad
        global editRad
        global remRad

        addRad = QPushButton(self.tr("Add"))
        editRad = QPushButton(self.tr("Edit"))
        remRad = QPushButton(self.tr("Remove"))

        addRad.clicked.connect(self.addRadButton)
        editRad.clicked.connect(self.radEditButton)
        remRad.clicked.connect(self.remRadButton)

        RadButton = QGroupBox()
        RadButtonLayout = QHBoxLayout()

        RadButtonLayout.addWidget(addRad)
        RadButtonLayout.addWidget(editRad)
        RadButtonLayout.addWidget(remRad)
        RadButton.setLayout(RadButtonLayout)

        global addLab
        global editLab
        global remLab
        addLab = QPushButton(self.tr("Add"))
        editLab = QPushButton(self.tr("Edit"))
        remLab = QPushButton(self.tr("Remove"))

        addLab.clicked.connect(self.addLabButton)
        editLab.clicked.connect(self.labEditButton)
        remLab.clicked.connect(self.remLabButton)

        LabButton = QGroupBox()
        LabButtonLayout = QHBoxLayout()

        LabButtonLayout.addWidget(addLab)
        LabButtonLayout.addWidget(editLab)
        LabButtonLayout.addWidget(remLab)
        LabButton.setLayout(LabButtonLayout)

        #Set up Buttons
        rbuttonList = QGroupBox()
        rbuttonListLayout = QVBoxLayout()

        rbuttonListLayout.addWidget(RadiologyListBox)
        rbuttonListLayout.addWidget(RadButton)
        
        rbuttonList.setLayout(rbuttonListLayout)

        lbuttonList = QGroupBox()
        lbuttonListLayout = QVBoxLayout()

        lbuttonListLayout.addWidget(LabStatusListBox)
        lbuttonListLayout.addWidget(LabButton)
        
        lbuttonList.setLayout(lbuttonListLayout)
        
        #Test Images
        global radiomap
        global radioImg
        radiomap = QPixmap("")
        radioImg = QLabel()
        radiomap = radiomap.scaled(250, 250, Qt.KeepAspectRatio)

        radioImg.setPixmap(radiomap)
        radioImg.setFixedWidth(200)
        radioImg.setFixedHeight(200)

        global labmap
        global labImg
        labmap = QPixmap("")
        labImg = QLabel()
        labmap = labmap.scaled(250, 250, Qt.KeepAspectRatio)

        labImg.setPixmap(labmap)
        labImg.setFixedWidth(200)
        labImg.setFixedHeight(200)
        
        #Set up picture boxes
        RadiologyPicBox = QGroupBox()
        LabPicBox = QGroupBox()

        RadiologyPicBoxLayout = QVBoxLayout()
        LabPicBoxLayout = QVBoxLayout()

        RadiologyPicBoxLayout.addWidget(radioImg)
        LabPicBoxLayout.addWidget(labImg)

        RadiologyPicBox.setLayout(RadiologyPicBoxLayout)
        LabPicBox.setLayout(LabPicBoxLayout)

        #Sets up final boxes
        RadiologyVBox = QGroupBox()
        LabVBox = QGroupBox()

        RadiologyVBoxLayout = QVBoxLayout()
        LabVBoxLayout = QVBoxLayout()

        RadiologyVBoxLayout.addWidget(doctLabel)
        RadiologyVBoxLayout.addWidget(doctValueLabel)
        RadiologyVBoxLayout.addWidget(dateLabel)
        RadiologyVBoxLayout.addWidget(radDateValueLabel)
        RadiologyVBoxLayout.addWidget(noteLabel)
        RadiologyVBoxLayout.addWidget(radNoteValueLabel)
        RadiologyVBoxLayout.addWidget(radFileNameLabel)
        RadiologyVBoxLayout.addWidget(radFileName)

        LabVBoxLayout.addWidget(locaLabel)
        LabVBoxLayout.addWidget(labLocaValueLabel)
        LabVBoxLayout.addWidget(date2Label)
        LabVBoxLayout.addWidget(labDate2ValueLabel)
        LabVBoxLayout.addWidget(note2Label)
        LabVBoxLayout.addWidget(note2ValueLabel)
        LabVBoxLayout.addWidget(labFileNameLabel)
        LabVBoxLayout.addWidget(labFileName)
    
        RadiologyVBox.setLayout(RadiologyVBoxLayout)
        LabVBox.setLayout(LabVBoxLayout)

        #Sets up final boxes
        RadiologyVBoxEdit = QGroupBox()
        LabVBoxEdit = QGroupBox()

        RadiologyVBoxLayoutEdit = QVBoxLayout()
        LabVBoxLayoutEdit = QVBoxLayout()

        RadiologyVBoxLayoutEdit.addWidget(doctLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(doctValueLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(dateLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(radDateValueLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(noteLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(radNoteValueLabelEdit)
        RadiologyVBoxLayoutEdit.addWidget(editRadImg)

        LabVBoxLayoutEdit.addWidget(locaLabelEdit)
        LabVBoxLayoutEdit.addWidget(labLocaValueLabelEdit)
        LabVBoxLayoutEdit.addWidget(date2LabelEdit)
        LabVBoxLayoutEdit.addWidget(labDate2ValueLabelEdit)
        LabVBoxLayoutEdit.addWidget(note2LabelEdit)
        LabVBoxLayoutEdit.addWidget(note2ValueLabelEdit)
        LabVBoxLayoutEdit.addWidget(editLabImg)
    
        RadiologyVBoxEdit.setLayout(RadiologyVBoxLayoutEdit)
        LabVBoxEdit.setLayout(LabVBoxLayoutEdit)
        
        #Displays each of list on page
        RadiologyGroup = QGroupBox(self.tr("Radiology Scans:"))
        LabGroup = QGroupBox(self.tr("Lab/Test Results:"))
        
        RadiologyGroupLayout = QHBoxLayout()
        RadiologyGroupLayout.addWidget(rbuttonList)
        RadiologyGroupLayout.addWidget(RadiologyPicBox)
        RadiologyGroupLayout.addWidget(RadiologyVBox)
        RadiologyGroupLayout.addWidget(RadiologyVBoxEdit)

        LabGroupLayout = QHBoxLayout()
        LabGroupLayout.addWidget(lbuttonList)
        LabGroupLayout.addWidget(LabPicBox)
        LabGroupLayout.addWidget(LabVBox)
        LabGroupLayout.addWidget(LabVBoxEdit)

        RadiologyGroup.setLayout(RadiologyGroupLayout)
        LabGroup.setLayout(LabGroupLayout)

        #Overall layout
        mainLayout = QVBoxLayout(self)
        mainLayout.addWidget(RadiologyGroup)
        mainLayout.addWidget(LabGroup)
        mainLayout.addStretch(1)

        widget.setLayout(mainLayout)
        #writeXML(0, "", "", "", "setup", "test")

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)

class radWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global radNumberVal

        radNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["radImg" + str(radNumberVal)]
        tempVar2 = myXmlDictionary["radLocDr" + str(radNumberVal)]
        tempVar3 = myXmlDictionary["radDate" + str(radNumberVal)]
        tempVar4 = myXmlDictionary["radNotes" + str(radNumberVal)]

        doctValueLabel.setText(tempVar2)
        radDateValueLabel.setText(tempVar3)
        radNoteValueLabel.setText(tempVar4)
        radFileName.setText(tempVar1)
        
        try:
            radiomap = QPixmap("C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + tempVar1)
            radiomap = radiomap.scaled(250, 250, Qt.KeepAspectRatio)
        except TypeError:
            radiomap = ""

        radioImg.setPixmap(radiomap)

class labWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global labNumberVal

        labNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["labImg" + str(labNumberVal)]
        tempVar2 = myXmlDictionary["labLocDr" + str(labNumberVal)]
        tempVar3 = myXmlDictionary["labDate" + str(labNumberVal)]
        tempVar4 = myXmlDictionary["labNotes" + str(labNumberVal)]

        labLocaValueLabel.setText(tempVar2)
        labDate2ValueLabel.setText(tempVar3)
        note2ValueLabel.setText(tempVar4)
        labFileName.setText(tempVar1)

        labmap = QPixmap("C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + tempVar1)
        labmap = labmap.scaled(250, 250, Qt.KeepAspectRatio)
        labImg.setPixmap(labmap)

class CurrentStatusTab(QWidget):
    def addFutButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Future Appointment #" + str(newDiagCounter)
        while testName in FutureList:
            newDiagCounter += 1
            testName = "Future Appointment #" + str(newDiagCounter)

        tempVar = "Future Appointment #" + str(newDiagCounter)

        tempNewDiag = futDoctValueLabelEdit.text()
        tempNewDoct = vistValueLabelEdit.text()
        tempNewDeta = addiValueLabelEdit.text()
        tempNewDate = futDateValueLabelEdit.text()

        global tempNewDiagVars        
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta, tempNewDate))

        newFutureList = []
        newFutureList.append(tempVar) 
        FutureList.append(tempVar)           
        FutureListBox.insertItems(newDiagCounter - 1, newFutureList)

        futDoctValueLabelEdit.setText("")
        vistValueLabelEdit.setText("")
        addiValueLabelEdit.setText("")
        futDateValueLabelEdit.setText("")

        futDoctValueLabel.setText("")
        vistValueLabel.setText("")
        addiValueLabel.setText("")
        futDateValueLabel.setText("")
        writeXML(newDiagCounter, "add", "fut", tempNewDiagVars)

    def futEditButton(self,parent=None):
        if editFuture.text() == "Edit":
            editFuture.setText("Save")
            
            futDoctValueLabelEdit.setText(futDoctValueLabel.text())
            vistValueLabelEdit.setText(vistValueLabel.text())
            addiValueLabelEdit.setText(addiValueLabel.text())
            futDateValueLabelEdit.setText(futDateValueLabel.text())
        else:
            editFuture.setText("Edit")

            tempDiag = futDoctValueLabelEdit.text()
            tempDoct = vistValueLabelEdit.text()
            tempDeta = addiValueLabelEdit.text()
            tempDate = futDateValueLabelEdit.text()
            
            global tempDiagVars            
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta, tempDate))

            futDoctValueLabelEdit.setText("")
            vistValueLabelEdit.setText("")
            addiValueLabelEdit.setText("")
            futDateValueLabelEdit.setText("")

            futDoctValueLabel.setText("")
            vistValueLabel.setText("")
            addiValueLabel.setText("")
            futDateValueLabel.setText("")
            writeXML(1, "edit", "fut", tempDiagVars)

    def remFutButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex
        remIndex = FutureList.index("Future Appointment #" + str(len(FutureList)))
        tempRemoval = FutureList.pop(remIndex)

        while remIndex < len(FutureList):
            diagListString = FutureList[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Future Appointment #" + str(diagListInt)
            FutureList[remIndex] = newDiagListString
            remIndex += 1

        FutureListBox.clear()            
        FutureListBox.insertItems(0, FutureList)

        futDoctValueLabel.setText("")
        vistValueLabel.setText("")
        addiValueLabel.setText("")
        futDateValueLabel.setText("")
        writeXML(remIndex, "remove", "fut", "")

    def addPastButton(self, parent=None):
        #Figures out next thing to be named
        newDiagCounter = 1
        testName = "Past Appointment #" + str(newDiagCounter)
        while testName in PastList:
            newDiagCounter += 1
            testName = "Past Appointment #" + str(newDiagCounter)

        tempVar = "Past Appointment #" + str(newDiagCounter)

        tempNewDiag = locaValueLabelEdit.text()
        tempNewDoct = rechValueLabelEdit.text()
        tempNewDeta = recwValueLabelEdit.text()
        tempNewDate = blopValueLabelEdit.text()
        tempNewDiag2 = blplValueLabelEdit.text()
        tempNewDoct2 = noteValueLabelEdit.text()
        tempNewDeta2 = date2ValueLabelEdit.text()

        try:
            tempHimg = heightDirectory[0]
        except NameError:
            tempHimg = ""
        try:
            tempWimg = weightDirectory[0]
        except NameError:
            tempWimg = ""
        try:
            tempPimg = pressDirectory[0]
        except NameError:
            tempPimg = ""

        global tempNewDiagVars
        tempNewDiagVars = []
        tempNewDiagVars.extend((tempNewDiag, tempNewDoct, tempNewDeta, tempNewDate, tempNewDiag2, tempNewDoct2, tempNewDeta2, tempHimg, tempWimg, tempPimg))

        newPastList = []
        newPastList.append(tempVar) 
        PastList.append(tempVar)           
        PastListBox.insertItems(newDiagCounter - 1, newPastList)

        locaValueLabelEdit.setText("")
        rechValueLabelEdit.setText("")
        recwValueLabelEdit.setText("")
        blopValueLabelEdit.setText("")
        blplValueLabelEdit.setText("")
        noteValueLabelEdit.setText("")
        date2ValueLabelEdit.setText("")
        editHeightImgValLabel.setText("")
        editWeightImgValLabel.setText("")
        editPressImgValLabel.setText("")

        locaValueLabel.setText("")
        rechValueLabel.setText("")
        recwValueLabel.setText("")
        blopValueLabel.setText("")
        blplValueLabel.setText("")
        noteValueLabel.setText("")
        date2ValueLabel.setText("")
        heightFileName.setText("")
        weightFileName.setText("")
        pressFileName.setText("")
        heightFileName.setText("")
        weightFileName.setText("")
        pressFileName.setText("")

        heightImg.clear()
        weightImg.clear()
        pressureImg.clear()

        writeXML(newDiagCounter, "add", "past", tempNewDiagVars)

    def pastEditButton(self,parent=None):
        if editPast.text() == "Edit":
            editPast.setText("Save")
            
            locaValueLabelEdit.setText(locaValueLabel.text())
            rechValueLabelEdit.setText(rechValueLabel.text())
            recwValueLabelEdit.setText(recwValueLabel.text())
            blopValueLabelEdit.setText(blopValueLabel.text())
            blplValueLabelEdit.setText(blplValueLabel.text())
            noteValueLabelEdit.setText(noteValueLabel.text())
            date2ValueLabelEdit.setText(date2ValueLabel.text())
            editHeightImgValLabel.setText(heightFileName.text())
            editWeightImgValLabel.setText(weightFileName.text())
            editPressImgValLabel.setText(pressFileName.text())
        else:
            editPast.setText("Edit")

            tempDiag = locaValueLabelEdit.text()
            tempDoct = rechValueLabelEdit.text()
            tempDeta = recwValueLabelEdit.text()
            tempDate = blopValueLabelEdit.text()
            tempDiag2 = blplValueLabelEdit.text()
            tempDoct2 = noteValueLabelEdit.text()
            tempDeta2 = date2ValueLabelEdit.text()

            try:
                tempHimg = heightDirectory[0]
            except NameError:
                tempHimg = ""
            try:
                tempWimg = weightDirectory[0]
            except NameError:
                tempWimg = ""
            try:
                tempPimg = pressDirectory[0]
            except NameError:
                tempPimg = ""
            
            global tempDiagVars
            tempDiagVars = []
            tempDiagVars.extend((tempDiag, tempDoct, tempDeta, tempDate, tempDiag2, tempDoct2, tempDeta2, tempHimg, tempWimg, tempPimg))

            locaValueLabelEdit.setText("")
            rechValueLabelEdit.setText("")
            recwValueLabelEdit.setText("")
            blopValueLabelEdit.setText("")
            blplValueLabelEdit.setText("")
            noteValueLabelEdit.setText("")
            date2ValueLabelEdit.setText("")
            editHeightImgValLabel.setText("")
            editWeightImgValLabel.setText("")
            editPressImgValLabel.setText("")

            locaValueLabel.setText("")
            rechValueLabel.setText("")
            recwValueLabel.setText("")
            blopValueLabel.setText("")
            blplValueLabel.setText("")
            noteValueLabel.setText("")
            date2ValueLabel.setText("")
            heightFileName.setText("")
            weightFileName.setText("")
            pressFileName.setText("")
            heightFileName.setText("")
            weightFileName.setText("")
            pressFileName.setText("")

            heightImg.clear()
            weightImg.clear()
            pressureImg.clear()

            writeXML(1, "edit", "past", tempDiagVars)

    def remPastButton(self, parent=None):
        #Figures out next thing to be named
        global remIndex
        remIndex = PastList.index("Past Appointment #" + str(len(PastList)))
        tempRemoval = PastList.pop(remIndex)

        while remIndex < len(PastList):
            diagListString = PastList[remIndex][-1]
            diagListInt = int(diagListString) - 1
            newDiagListString = "Past Appointment #" + str(diagListInt)
            PastList[remIndex] = newDiagListString
            remIndex += 1

        PastListBox.clear()            
        PastListBox.insertItems(0, PastList)

        locaValueLabel.setText("")
        rechValueLabel.setText("")
        recwValueLabel.setText("")
        blopValueLabel.setText("")
        blplValueLabel.setText("")
        noteValueLabel.setText("")
        date2ValueLabel.setText("")
        heightFileName.setText("")
        weightFileName.setText("")
        pressFileName.setText("")

        heightImg.clear()
        weightImg.clear()
        pressureImg.clear()

        writeXML(remIndex, "remove", "past", "")

    def heightSelectFile(self):
        global heightDirectory
        heightDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(heightDirectory).rfind('/')
        endPos = str(heightDirectory).rfind("',")
        subString = str(heightDirectory)[startPos+1:endPos]

        newLocation = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + subString
        
        heightmap = QPixmap(newLocation)
        heightmap = heightmap.scaled(300, 300, Qt.KeepAspectRatio)
        heightImg.setPixmap(heightmap)
        copy(heightDirectory[0], newLocation)
        editHeightImgValLabel.setText(subString)

        tempList = list(heightDirectory)
        tempList[0] = subString
        heightDirectory = tuple(tempList)

    def weightSelectFile(self):
        global weightDirectory
        weightDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(weightDirectory).rfind('/')
        endPos = str(weightDirectory).rfind("',")
        subString = str(weightDirectory)[startPos+1:endPos]

        newLocation = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + subString
        weightmap = QPixmap(newLocation)
        weightmap = weightmap.scaled(300, 300, Qt.KeepAspectRatio)

        weightImg.setPixmap(weightmap)
        copy(weightDirectory[0], newLocation)
        editWeightImgValLabel.setText(subString)        

        tempList = list(weightDirectory)
        tempList[0] = subString
        weightDirectory = tuple(tempList)

    def pressSelectFile(self):
        global pressDirectory
        pressDirectory = QFileDialog.getOpenFileName(self,self.tr("Directory"),"","","",QFileDialog.DontResolveSymlinks)
        
        startPos = str(pressDirectory).rfind('/')
        endPos = str(pressDirectory).rfind("',")
        subString = str(pressDirectory)[startPos+1:endPos]

        newLocation = "C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + subString
        pressmap = QPixmap(newLocation)
        pressmap = pressmap.scaled(300, 300, Qt.KeepAspectRatio)

        pressureImg.setPixmap(pressmap)
        copy(pressDirectory[0], newLocation)
        editPressImgValLabel.setText(subString)        

        tempList = list(pressDirectory)
        tempList[0] = subString
        pressDirectory = tuple(tempList)


    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        widget = QWidget()

        content = ET.parse(xmlDir)
        currentStatus=content.find("currentStatus")
        futCounter = 1
        pastCounter = 1

        #FUTURE LIST
        global FutureListBox
        global FutureList

        FutureListBox = futureWidget()
        FutureList = []
        
        while "futLocDr" + str(futCounter) in myXmlDictionary:
            tempVar = "Future Appointment #" + str(futCounter)
            FutureList.append(tempVar)
            futCounter += 1
        FutureListBox.insertItems(0, FutureList)
        FutureListBox.connect(FutureListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               FutureListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        #Set up Button events 
        global addFuture
        global editFuture
        global remFuture

        addFuture = QPushButton(self.tr("Add"))
        editFuture = QPushButton(self.tr("Edit"))
        remFuture = QPushButton(self.tr("Remove"))

        addFuture.clicked.connect(self.addFutButton)
        editFuture.clicked.connect(self.futEditButton)
        remFuture.clicked.connect(self.remFutButton)

        futureButton = QGroupBox()
        futureButtonLayout = QHBoxLayout()

        futureButtonLayout.addWidget(addFuture)
        futureButtonLayout.addWidget(editFuture)
        futureButtonLayout.addWidget(remFuture)
        futureButton.setLayout(futureButtonLayout)

        #PAST LIST
        global PastListBox
        global PastList

        PastListBox = pastWidget()
        PastList = []
        
        while "locDr" + str(pastCounter) in myXmlDictionary:
            tempVar = "Past Appointment #" + str(pastCounter)
            PastList.append(tempVar)
            pastCounter += 1
        PastListBox.insertItems(0, PastList)
        PastListBox.connect(PastListBox,SIGNAL("itemClicked(QListWidgetItem*)"),
               PastListBox,SLOT("doubleClickedSlot(QListWidgetItem*)"))

        global addPast
        global editPast
        global remPast

        addPast = QPushButton(self.tr("Add"))
        editPast = QPushButton(self.tr("Edit"))
        remPast = QPushButton(self.tr("Remove"))

        addPast.clicked.connect(self.addPastButton)
        editPast.clicked.connect(self.pastEditButton)
        remPast.clicked.connect(self.remPastButton)

        pastButton = QGroupBox()
        pastButtonLayout = QHBoxLayout()

        pastButtonLayout.addWidget(addPast)
        pastButtonLayout.addWidget(editPast)
        pastButtonLayout.addWidget(remPast)
        pastButton.setLayout(pastButtonLayout)

        #Set up Buttons
        fbuttonList = QGroupBox()
        fbuttonListLayout = QVBoxLayout()

        fbuttonListLayout.addWidget(FutureListBox)
        fbuttonListLayout.addWidget(futureButton)
        
        fbuttonList.setLayout(fbuttonListLayout)

        pbuttonList = QGroupBox()
        pbuttonListLayout = QVBoxLayout()

        pbuttonListLayout.addWidget(PastListBox)
        pbuttonListLayout.addWidget(pastButton)
        
        pbuttonList.setLayout(pbuttonListLayout)

        #SET UP STAGNANT FUTURE
        #Set up Future Information
        global futDoctValueLabel
        doctLabel = QLabel(self.tr("Location/Doctor:"))
        futDoctValueLabel = QLabel("")
        futDoctValueLabel.setFixedWidth(300)
        futDoctValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global vistValueLabel
        vistLabel = QLabel(self.tr("Visitation Purpose:"))
        vistValueLabel = QLabel("")
        vistValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global addiValueLabel
        addiLabel = QLabel(self.tr("Additional Notes:"))
        addiValueLabel = QLabel("")
        addiValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global futDateValueLabel
        dateLabel = QLabel(self.tr("Scheduled Date:"))
        futDateValueLabel = QLabel("")
        futDateValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #Set up Past Information
        global locaValueLabel
        locaLabel = QLabel(self.tr("Location/Doctor:"))
        locaValueLabel = QLabel("")
        locaValueLabel.setFixedWidth(300)
        locaValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global rechValueLabel
        rechLabel = QLabel(self.tr("Recorded Height:"))
        rechValueLabel = QLabel("")
        rechValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global recwValueLabel
        recwLabel = QLabel(self.tr("Recorded Weight:"))
        recwValueLabel = QLabel("")
        recwValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global blopValueLabel
        blopLabel = QLabel(self.tr("Blood Pressure:"))
        blopValueLabel = QLabel("")
        blopValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global blplValueLabel
        blplLabel = QLabel(self.tr("Blood Pulse:"))
        blplValueLabel = QLabel("")
        blplValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global noteValueLabel
        noteLabel = QLabel(self.tr("Notes:"))
        noteValueLabel = QLabel("")
        noteValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global date2ValueLabel
        date2Label = QLabel(self.tr("Visitation Date:"))
        date2ValueLabel = QLabel("")
        date2ValueLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global heightFileName
        heightFileNameLabel = QLabel(self.tr("Height File Name:"))
        heightFileName = QLabel("")
        heightFileName.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global weightFileName
        weightFileNameLabel = QLabel(self.tr("Weight File Name:"))
        weightFileName = QLabel("")
        weightFileName.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        global pressFileName
        pressFileNameLabel = QLabel(self.tr("Pressure File Name:"))
        pressFileName = QLabel("")
        pressFileName.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        #SET UP EDITING FUTURE
        global futDoctValueLabelEdit
        doctLabelEdit = QLabel(self.tr("Location/Doctor:"))
        futDoctValueLabelEdit = QLineEdit("")

        global vistValueLabelEdit
        vistLabelEdit = QLabel(self.tr("Visitation Purpose: "))
        vistValueLabelEdit = QLineEdit("")

        global addiValueLabelEdit
        addiLabelEdit = QLabel(self.tr("Additional Notes: "))
        addiValueLabelEdit = QLineEdit("")

        global futDateValueLabelEdit
        dateLabelEdit = QLabel(self.tr("Scheduled Date:"))
        futDateValueLabelEdit = QLineEdit("")

        #Set up Past Information
        global locaValueLabelEdit        
        locaLabelEdit = QLabel(self.tr("Location/Doctor:"))
        locaValueLabelEdit = QLineEdit("")

        global rechValueLabelEdit
        rechLabelEdit = QLabel(self.tr("Recorded Height:"))
        rechValueLabelEdit = QLineEdit("")

        global recwValueLabelEdit
        recwLabelEdit = QLabel(self.tr("Recorded Weight:"))
        recwValueLabelEdit = QLineEdit("")

        global blopValueLabelEdit
        blopLabelEdit = QLabel(self.tr("Blood Pressure:"))
        blopValueLabelEdit = QLineEdit("")

        global blplValueLabelEdit
        blplLabelEdit = QLabel(self.tr("Blood Pulse:"))
        blplValueLabelEdit = QLineEdit("")

        global noteValueLabelEdit
        noteLabelEdit = QLabel(self.tr("Notes:"))
        noteValueLabelEdit = QLineEdit("")

        global date2ValueLabelEdit
        date2LabelEdit = QLabel(self.tr("Visitation Date:"))
        date2ValueLabelEdit = QLineEdit("")

        height = QCheckBox(self.tr("Height"))
        weight = QCheckBox(self.tr("Weight"))
        pressure = QCheckBox(self.tr("Blood Pressure"))

        editHeightImgButt = QPushButton(self.tr("Select Height Image"))
        editWeightImgButt = QPushButton(self.tr("Select Weight Image"))
        editPressureImgButt = QPushButton(self.tr("Select Pressure Image"))

        editHeightImgButt.clicked.connect(self.heightSelectFile)
        editWeightImgButt.clicked.connect(self.weightSelectFile)
        editPressureImgButt.clicked.connect(self.pressSelectFile)

        global editHeightImgValLabel
        global editWeightImgValLabel
        global editPressImgValLabel

        editHeightImgValLabel = QLabel("")
        editWeightImgValLabel = QLabel("")
        editPressImgValLabel = QLabel("")

        editHeightImgValLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        editWeightImgValLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        editPressImgValLabel.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        editHeightImg = QGroupBox()
        editWeightImg = QGroupBox()
        editPressureImg = QGroupBox()

        editHeightImgLayout = QHBoxLayout()
        editWeightImgLayout = QHBoxLayout()
        editPressureImgLayout = QHBoxLayout()

        editHeightImgLayout.addWidget(editHeightImgButt)
        editHeightImgLayout.addWidget(editHeightImgValLabel)

        editWeightImgLayout.addWidget(editWeightImgButt)
        editWeightImgLayout.addWidget(editWeightImgValLabel)
        
        editPressureImgLayout.addWidget(editPressureImgButt)
        editPressureImgLayout.addWidget(editPressImgValLabel)

        editHeightImg.setLayout(editHeightImgLayout)
        editWeightImg.setLayout(editWeightImgLayout)
        editPressureImg.setLayout(editPressureImgLayout)

        #Set up Graph Information
        global heightmap
        global heightImg
        heightmap = QPixmap("")
        heightImg = QLabel()
        heightmap = heightmap.scaled(250, 250, Qt.KeepAspectRatio)

        heightImg.setPixmap(heightmap)
        heightImg.setFixedWidth(275)
        heightImg.setFixedHeight(275)

        global weightmap
        global weightImg
        weightmap = QPixmap("")
        weightImg = QLabel()
        weightmap = weightmap.scaled(250, 250, Qt.KeepAspectRatio)

        weightImg.setPixmap(weightmap)
        weightImg.setFixedWidth(275)
        weightImg.setFixedHeight(275)

        global pressmap
        global pressureImg
        pressmap = QPixmap("")
        pressureImg = QLabel()
        pressmap = pressmap.scaled(250, 250, Qt.KeepAspectRatio)

        pressureImg.setPixmap(pressmap)
        pressureImg.setFixedWidth(275)
        pressureImg.setFixedHeight(275)

        #Set up picture boxes
        HeightPicBox = QGroupBox(self.tr("Height"))
        WeightPicBox = QGroupBox(self.tr("Weight"))
        PressPicBox = QGroupBox(self.tr("Pressure"))

        HeightPicBoxLayout = QVBoxLayout()
        WeightPicBoxLayout = QVBoxLayout()
        PressPicBoxLayout = QVBoxLayout()
        
        HeightPicBoxLayout.addWidget(heightImg)
        WeightPicBoxLayout.addWidget(weightImg)
        PressPicBoxLayout.addWidget(pressureImg)

        HeightPicBox.setLayout(HeightPicBoxLayout)
        WeightPicBox.setLayout(WeightPicBoxLayout)
        PressPicBox.setLayout(PressPicBoxLayout)

        #Set up Future Boxes for Stagnant
        futureInfo = QGroupBox()
        futureInfoLayout = QVBoxLayout()

        futureInfoLayout.addWidget(doctLabel)
        futureInfoLayout.addWidget(futDoctValueLabel)
        futureInfoLayout.addWidget(dateLabel)
        futureInfoLayout.addWidget(futDateValueLabel)
        futureInfoLayout.addWidget(vistLabel)
        futureInfoLayout.addWidget(vistValueLabel)
        futureInfoLayout.addWidget(addiLabel)
        futureInfoLayout.addWidget(addiValueLabel)
        
        futureInfo.setLayout(futureInfoLayout)

        #Set up Future Boxes for Editing
        futureInfoEdit = QGroupBox()
        futureInfoLayoutEdit = QVBoxLayout()

        futureInfoLayoutEdit.addWidget(doctLabelEdit)
        futureInfoLayoutEdit.addWidget(futDoctValueLabelEdit)
        futureInfoLayoutEdit.addWidget(dateLabelEdit)
        futureInfoLayoutEdit.addWidget(futDateValueLabelEdit)
        futureInfoLayoutEdit.addWidget(vistLabelEdit)
        futureInfoLayoutEdit.addWidget(vistValueLabelEdit)
        futureInfoLayoutEdit.addWidget(addiLabelEdit)
        futureInfoLayoutEdit.addWidget(addiValueLabelEdit)
        
        futureInfoEdit.setLayout(futureInfoLayoutEdit)

        #Set up Past Boxes for Stagnant
        pastInfo = QGroupBox()
        pastInfoLayout = QVBoxLayout()
        
        pastInfoLayout.addWidget(locaLabel)
        pastInfoLayout.addWidget(locaValueLabel)
        pastInfoLayout.addWidget(date2Label)
        pastInfoLayout.addWidget(date2ValueLabel)
        pastInfoLayout.addWidget(rechLabel)
        pastInfoLayout.addWidget(rechValueLabel)
        pastInfoLayout.addWidget(recwLabel)
        pastInfoLayout.addWidget(recwValueLabel)
        pastInfoLayout.addWidget(blopLabel)
        pastInfoLayout.addWidget(blopValueLabel)
        pastInfoLayout.addWidget(blplLabel)
        pastInfoLayout.addWidget(blplValueLabel)
        pastInfoLayout.addWidget(noteLabel)
        pastInfoLayout.addWidget(noteValueLabel)
        pastInfoLayout.addWidget(heightFileNameLabel)
        pastInfoLayout.addWidget(heightFileName)
        pastInfoLayout.addWidget(weightFileNameLabel)
        pastInfoLayout.addWidget(weightFileName)
        pastInfoLayout.addWidget(pressFileNameLabel)
        pastInfoLayout.addWidget(pressFileName)

        pastInfo.setLayout(pastInfoLayout)

        #Set up Past Boxes for Editing
        pastInfoEdit = QGroupBox()
        pastInfoLayoutEdit = QVBoxLayout()
        
        pastInfoLayoutEdit.addWidget(locaLabelEdit)
        pastInfoLayoutEdit.addWidget(locaValueLabelEdit)
        pastInfoLayoutEdit.addWidget(date2LabelEdit)
        pastInfoLayoutEdit.addWidget(date2ValueLabelEdit)
        pastInfoLayoutEdit.addWidget(rechLabelEdit)
        pastInfoLayoutEdit.addWidget(rechValueLabelEdit)
        pastInfoLayoutEdit.addWidget(recwLabelEdit)
        pastInfoLayoutEdit.addWidget(recwValueLabelEdit)
        pastInfoLayoutEdit.addWidget(blopLabelEdit)
        pastInfoLayoutEdit.addWidget(blopValueLabelEdit)
        pastInfoLayoutEdit.addWidget(blplLabelEdit)
        pastInfoLayoutEdit.addWidget(blplValueLabelEdit)
        pastInfoLayoutEdit.addWidget(noteLabelEdit)
        pastInfoLayoutEdit.addWidget(noteValueLabelEdit)
        pastInfoLayoutEdit.addWidget(editHeightImg)
        pastInfoLayoutEdit.addWidget(editWeightImg)
        pastInfoLayoutEdit.addWidget(editPressureImg)

        pastInfoEdit.setLayout(pastInfoLayoutEdit)

        #Setting up Future Appointment data
        futureApp = QGroupBox(self.tr("Upcoming Appointments"))
        futureAppLayout = QHBoxLayout()

        futureAppLayout.addWidget(fbuttonList)
        futureAppLayout.addWidget(futureInfo)
        futureAppLayout.addWidget(futureInfoEdit)
        
        futureApp.setLayout(futureAppLayout)

        #Setting up Past Appointment data
        pastApp = QGroupBox(self.tr("Past Appointments"))
        pastAppLayout = QHBoxLayout()

        pastAppLayout.addWidget(pbuttonList)
        pastAppLayout.addWidget(pastInfo)
        pastAppLayout.addWidget(pastInfoEdit)        
        
        pastApp.setLayout(pastAppLayout)

        #Setting up Graphs of Checkups
        appGraph = QGroupBox(self.tr("Graphical Display"))
        appGraphLayout = QHBoxLayout()

        appGraphLayout.addWidget(HeightPicBox)
        appGraphLayout.addWidget(WeightPicBox)
        appGraphLayout.addWidget(PressPicBox)

        appGraph.setLayout(appGraphLayout)

        #Putting three layers on page
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(futureApp)
        mainLayout.addWidget(pastApp)
        mainLayout.addWidget(appGraph)
        mainLayout.addStretch(1)
        
        widget.setLayout(mainLayout)
        #writeXML(0, "", "", "", "setup", "current")

        #Scroll settings
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        
        #Scroll Area Layer add 
        vLayout = QVBoxLayout(self)
        vLayout.addWidget(scroll)
        self.setLayout(vLayout)
 
class futureWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global futNumberVal

        futNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["futLocDr" + str(futNumberVal)]
        tempVar2 = myXmlDictionary["futDate" + str(futNumberVal)]
        tempVar3 = myXmlDictionary["futPurpose" + str(futNumberVal)]
        tempVar4 = myXmlDictionary["futNotes" + str(futNumberVal)]

        futDoctValueLabel.setText(tempVar1)
        vistValueLabel.setText(tempVar2)
        addiValueLabel.setText(tempVar3)
        futDateValueLabel.setText(tempVar4)

class pastWidget(QListWidget):
    def doubleClickedSlot(self,item):
        global pastNumberVal

        pastNumberVal = str(item.text())[-1]
        tempVar1 = myXmlDictionary["locDr" + str(pastNumberVal)]
        tempVar2 = myXmlDictionary["date" + str(pastNumberVal)]
        tempVar3 = myXmlDictionary["height" + str(pastNumberVal)]
        tempVar4 = myXmlDictionary["weight" + str(pastNumberVal)]
        tempVar5 = myXmlDictionary["bloodPressure" + str(pastNumberVal)]
        tempVar6 = myXmlDictionary["pulse" + str(pastNumberVal)]
        tempVar7 = myXmlDictionary["notes" + str(pastNumberVal)]
        tempVar8 = myXmlDictionary["heightImg" + str(pastNumberVal)]
        tempVar9 = myXmlDictionary["weightImg" + str(pastNumberVal)]
        tempVar10 = myXmlDictionary["pressureImg" + str(pastNumberVal)]

        locaValueLabel.setText(tempVar1)
        rechValueLabel.setText(tempVar2)
        recwValueLabel.setText(tempVar3)
        blopValueLabel.setText(tempVar4)
        blplValueLabel.setText(tempVar5)
        noteValueLabel.setText(tempVar6)
        date2ValueLabel.setText(tempVar7)

        heightFileName.setText(tempVar8)
        weightFileName.setText(tempVar9)
        pressFileName.setText(tempVar10)

        heightmap = QPixmap("C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + heightFileName.text())
        weightmap = QPixmap("C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + weightFileName.text())
        pressmap = QPixmap("C:\\FlashEHR\\patients\\" + lastNameVal + ", " + firstNameVal + "\\imgs\\" + pressFileName.text())

        heightmap = heightmap.scaled(300, 300, Qt.KeepAspectRatio)
        weightmap = weightmap.scaled(300, 300, Qt.KeepAspectRatio)
        pressmap = pressmap.scaled(300, 300, Qt.KeepAspectRatio)

        heightImg.setPixmap(heightmap)
        weightImg.setPixmap(weightmap)
        pressureImg.setPixmap(pressmap)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = loginTab()
    window.setWindowTitle("FlashEHR")
    window.show()
    sys.exit(app.exec_())