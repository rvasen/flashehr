import pymysql, os
from Crypto.Hash import SHA512
from Crypto.Cipher import AES
from hashlib import md5
from xml.etree import ElementTree as ET
from shutil import move, copytree, rmtree

def synchronize(usbDir):
	f = open(os.path.join(usbDir, 'ID.txt'),'r')
	usb = f.read()
	f.close()
	conn = pymysql.connect(host='sql.mit.edu', port=3306, user='rvasen', passwd='zoj77mel', db='rvasen+FlashEHR')
	cur = conn.cursor()
	cur.execute("SELECT encryptKey FROM patients WHERE usbID='%s'" % usb)
	for row in cur:
		encryptKey=row[0]
	cur.close
	conn.close
	inFile = open(os.path.join(usbDir, "encrypted.xml"), "rb")
	outFile = open(os.path.join("C:\\FlashEHR\\patients", "temp.xml"), "wb")
	decrypt(inFile, outFile, encryptKey)
	inFile.close()
	outFile.close()
	content = ET.parse("C:\\FlashEHR\\patients\\temp.xml")
	mainInfo=content.find("mainInfo")
	lastNameVal = mainInfo.find('who/lastname').text
	firstNameVal = mainInfo.find('who/firstname').text
	fullname = lastNameVal + ", " + firstNameVal
	os.remove("C:\\FlashEHR\\patients\\temp.xml")
	inFile = open(os.path.join("C:\\FlashEHR\\patients", fullname + "\\patient.xml"), "rb")
	outFile = open(os.path.join(usbDir, "encrypted.xml"), "wb")
	encrypt(inFile, outFile, encryptKey)
	inFile.close()
	outFile.close()
	newDir = os.path.join("C:\\FlashEHR\\patients", fullname)
	rmtree(os.path.join(usbDir, "imgs"))
	copytree(os.path.join(newDir, "imgs"), os.path.join(usbDir, "imgs"))


def derive_key_and_iv(password, salt, key_length, iv_length):
    d = d_i = b''
    while len(d) < key_length + iv_length:
            d_i = md5(d_i + str.encode(password) + salt).digest()
            d += d_i
    return d[:key_length], d[key_length:key_length+iv_length]

def encrypt(in_file, out_file, password, salt_header='', key_length=32):
    bs = AES.block_size
    salt = os.urandom(bs - len(salt_header))
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    out_file.write(str.encode(salt_header) + salt) 
    finished = False
    while not finished:
            chunk = in_file.read(1024 * bs)
            if len(chunk) == 0 or len(chunk) % bs != 0:
                    padding_length = (bs - len(chunk) % bs) or bs
                    chunk += str.encode(padding_length * chr(padding_length)) 
                    finished = True
            out_file.write(cipher.encrypt(chunk))

def decrypt(in_file, out_file, password, salt_header='', key_length=32): 

    bs = AES.block_size
    salt = in_file.read(bs)[len(salt_header):] 
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    next_chunk = ''
    finished = False
    while not finished:
            chunk = next_chunk
            next_chunk = cipher.decrypt(in_file.read(1024 * bs))
            if len(next_chunk) == 0:
                    padding_length = chunk[-1] 
                    chunk = chunk[:-padding_length]
                    finished = True
            out_file.write(bytes(x for x in chunk)) 