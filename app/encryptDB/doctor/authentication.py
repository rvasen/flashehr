import pymysql, os
from Crypto.Hash import SHA512
from Crypto.Cipher import AES
from hashlib import md5
from xml.etree import ElementTree as ET
from shutil import move, copytree

def authenticate(usbDir, enteredPassword):
	f = open(os.path.join(usbDir, 'ID.txt'),'r')
	usb = f.read()
	f.close()
	conn = pymysql.connect(host='sql.mit.edu', port=3306, user='rvasen', passwd='zoj77mel', db='rvasen+FlashEHR')
	cur = conn.cursor()
	cur.execute("SELECT password FROM patients WHERE usbID='%s'" % usb)
	user=False
	password = ""
	for row in cur:
		password=row[0]
	if(password != ""):
		user = True
	if(user == False):
		cur.close
		conn.close
		return False
	cur.close
	hash = SHA512.new()
	enteredPassword=enteredPassword.encode('utf-8')
	hash.update(enteredPassword)
	enteredPassword=hash.hexdigest()
	auth = False
	if(password == enteredPassword):
		auth = True
	else:
		conn.close
		return False
	if(auth):
		cur = conn.cursor()
		cur.execute("SELECT encryptKey FROM patients WHERE usbID='%s'" % usb)
		for row in cur:
			encryptKey=row[0]
		inFile = open(os.path.join(usbDir, "encrypted.xml"), "rb")
		outFile = open(os.path.join("C:\\FlashEHR\\patients", "patient.xml"), "wb")
		decrypt(inFile, outFile, encryptKey)
		inFile.close()
		outFile.close()
		content = ET.parse("C:\\FlashEHR\\patients\\patient.xml")
		mainInfo=content.find("mainInfo")
		lastNameVal = mainInfo.find('who/lastname').text
		firstNameVal = mainInfo.find('who/firstname').text
		fullname = lastNameVal + ", " + firstNameVal
		newDir = os.path.join("C:\\FlashEHR\\patients", fullname)
		copytree(os.path.join(usbDir, "imgs"), os.path.join(newDir, "imgs"))
		move("C:\\FlashEHR\\patients\\patient.xml", newDir)
		return True


def derive_key_and_iv(password, salt, key_length, iv_length):
    d = d_i = b''
    while len(d) < key_length + iv_length:
            d_i = md5(d_i + str.encode(password) + salt).digest()
            d += d_i
    return d[:key_length], d[key_length:key_length+iv_length]

def decrypt(in_file, out_file, password, salt_header='', key_length=32): 

    bs = AES.block_size
    salt = in_file.read(bs)[len(salt_header):] 
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    next_chunk = ''
    finished = False
    while not finished:
            chunk = next_chunk
            next_chunk = cipher.decrypt(in_file.read(1024 * bs))
            if len(next_chunk) == 0:
                    padding_length = chunk[-1] 
                    chunk = chunk[:-padding_length]
                    finished = True
            out_file.write(bytes(x for x in chunk)) 