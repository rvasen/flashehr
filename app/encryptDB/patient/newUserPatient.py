import pymysql, os, random
from Crypto.Hash import SHA512
from Crypto.Cipher import AES
from hashlib import md5

def createUser(usbDir, enteredPassword):
    #Create USB ID
    f = open(os.path.join(usbDir, "ID.txt"),'w')
    iD = '%064x' % random.randrange(16**64)
    f.write(iD)
    f.close
    #Generate password hash
    hash = SHA512.new()
    enteredPassword=enteredPassword.encode('utf-8')
    hash.update(enteredPassword)
    enteredPassword=hash.hexdigest()
    #Encrypt USB and generate key
    encryptKey = '%064x' % random.randrange(16**64)
    inFile = open(os.path.join(usbDir, "patient.xml"), "rb")
    outFile = open(os.path.join(usbDir, "encrypted.xml"), "wb")
    encrypt(inFile, outFile, encryptKey)
    inFile.close()
    outFile.close()
    os.remove(os.path.join(usbDir, "patient.xml"))
    conn = pymysql.connect(host='sql.mit.edu', port=3306, user='rvasen', passwd='zoj77mel', db='rvasen+FlashEHR')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO `rvasen+FlashEHR`.`patients` (`id`, `usbID`, `password`, `encryptKey`) VALUES (NULL, '%s', '%s', '%s')" % (iD, enteredPassword, encryptKey))
        conn.commit()
        return True
    except pymysql.IntegrityError:
        return False

def derive_key_and_iv(password, salt, key_length, iv_length):
    d = d_i = b''
    while len(d) < key_length + iv_length:
            d_i = md5(d_i + str.encode(password) + salt).digest()
            d += d_i
    return d[:key_length], d[key_length:key_length+iv_length]

def encrypt(in_file, out_file, password, salt_header='', key_length=32):
    bs = AES.block_size
    salt = os.urandom(bs - len(salt_header))
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    out_file.write(str.encode(salt_header) + salt) 
    finished = False
    while not finished:
            chunk = in_file.read(1024 * bs)
            if len(chunk) == 0 or len(chunk) % bs != 0:
                    padding_length = (bs - len(chunk) % bs) or bs
                    chunk += str.encode(padding_length * chr(padding_length)) 
                    finished = True
            out_file.write(cipher.encrypt(chunk))
