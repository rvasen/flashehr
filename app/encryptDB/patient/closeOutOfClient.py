import pymysql, os
from Crypto.Cipher import AES
from hashlib import md5

def closeOut(usbDir):
    inFile = open(os.path.join(usbDir, "temporary.xml"),"rb")
    outFile = open(os.path.join(usbDir, "encrypted.xml"), "wb")
    iD = open(os.path.join(usbDir, "ID.txt"), "r")
    usb = iD.read()
    conn = pymysql.connect(host='sql.mit.edu', port=3306, user='rvasen', passwd='zoj77mel', db='rvasen+FlashEHR')
    cur = conn.cursor()
    cur.execute("SELECT encryptKey FROM patients WHERE usbID='%s'" % usb)
    for row in cur:
       theKey=row[0]
    cur.close
    conn.close
    encrypt(inFile, outFile, theKey)
    inFile.close()
    outFile.close()
    os.remove(os.path.join(usbDir, "temporary.xml"))

def derive_key_and_iv(password, salt, key_length, iv_length):
    d = d_i = b''
    while len(d) < key_length + iv_length:
            d_i = md5(d_i + str.encode(password) + salt).digest()
            d += d_i
    return d[:key_length], d[key_length:key_length+iv_length]

def encrypt(in_file, out_file, password, salt_header='', key_length=32):
    bs = AES.block_size
    salt = os.urandom(bs - len(salt_header))
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    out_file.write(str.encode(salt_header) + salt) 
    finished = False
    while not finished:
            chunk = in_file.read(1024 * bs)
            if len(chunk) == 0 or len(chunk) % bs != 0:
                    padding_length = (bs - len(chunk) % bs) or bs
                    chunk += str.encode(padding_length * chr(padding_length)) 
                    finished = True
            out_file.write(cipher.encrypt(chunk))
